import * as debug from "../debug.js"
import * as utilitiesManager from "../utilities.js";


/**
 * Token is a PlaceableObject and we need to manipulate certain views of things for it.
 */
export class OSRICToken extends Token {

    /** @override because we need to point to this.document.name not this.data.name for actor ID name views */
    _drawNameplate() {
        const style = this._getTextStyle();
        const name = new PreciseText(this.document.name, style);
        name.anchor.set(0.5, 0);
        name.position.set(this.w / 2, this.h + 2);
        return name;
    }
}// end Token

/**
 * 
 * Extending tokenDocument so that we can filter the TokenConfig
 * dropdown list and block recursion issues
 * 
 * and also override name to hide npc names everywhere
 * 
 */
export class OSRICTokenDocument extends TokenDocument {
    get nameRaw() {
        return this.name;
    }

    /**
     * 
     * @returns name processed with some tests
     */
    getName() {
        // console.log("getName() OSRICTokenDocument", this)
        if (['npc', 'lootable'].includes(this?.actor?.type)) {
            if (!game.user.isGM) {
                // actor already does all the revelvant checks/points to alias
                return this.actor.getName();
            }
        }
        return this.name;
    }

    prepareDerivedData() {
        //     // for v10 requirements
        //     // console.log("token.js prepareDerivedData", this)
        super.prepareDerivedData();
        this.name = this.getName();
    }

    /**@override */
    static getTrackedAttributes(data, _path = []) {
        // console.log("getTrackedAttributes START", { data, _path });

        if (!data) {
            data = {};
            for (let model of Object.values(game.system.model.Actor)) {
                foundry.utils.mergeObject(data, model);
            }
        }

        /**
         * we need to filter the data of getTrackedAttributes()
         * otherwise it gets in a recursion loop and have stack error
         * 
         * we NEED to ignore "contains" and "containedIn" but the others are simply
         * to reduce the spam in the resources drop down for Token config
         * 
         */
        // const checkThese = ['attributes', 'abilities', 'details', 'power'];
        const ignoreThese = ['contains', 'containedIn', 'config', 'matrix', 'classes', 'inventory', 'memorizations', 'spellInfo', 'actionInventory', 'gear', 'containers', 'skills', 'weapons', 'spells', , 'races', 'rank', 'proficiencies', 'activeClasses', 'armorClass', 'abilityList', 'itemActionCount', 'mods', 'actionCount'];

        let reducedData = duplicate(data);
        // delete entries we ignore
        ignoreThese.forEach(entry => { delete reducedData[entry] })
        // console.log("getTrackedAttributes", { reducedData });        
        // cleaned up data, now send to the super.
        return super.getTrackedAttributes(reducedData, _path);
    }

    /**
     * //BUG:
     * This doesn't work perfectly with tokens that are not scale/size 1:1. Anything above/below and it skews
     * some distances but I'm not sure why.
     * 
     * Get the distance to a token
     * 
     * @param {TokenDocument} tokenDocument 
     * @returns {Number} The distance between the tokens
     * 
     */
    getDistance(tokenDocument = undefined) {
        const sourceDocument = this;
        console.log("token.js getDistance", { sourceDocument, tokenDocument })
        if (!tokenDocument) {
            ui.notifications.error(`token.js getDistance() requires tokenDocument target [${tokenDocument}]`)
            return {}
        }

        const sourceToken = sourceDocument.object;
        const targetToken = tokenDocument.object;

        // gridsize (5/10/etc)
        const gridDistance = canvas.scene.grid.distance;
        // pixels per grid (50/100/200/etc)
        const gridSize = canvas.scene.grid.size;
        // types of grid unit, (ft/mile/yard/etc)
        const gridUnits = canvas.scene.grid.units;

        // // figure out the size of the tokens so we can account for it's space
        const sourceDisplacement = (Math.max(sourceDocument.height, sourceDocument.width) / 2);
        const targetDisplacement = (Math.max(tokenDocument.height, tokenDocument.width) / 2);

        const tokenDisplacement = sourceDisplacement + targetDisplacement;
        let tokenSpace = tokenDisplacement * gridSize;

        const ray = new Ray(sourceToken.center, targetToken.center);
        // const ray = new Ray(sourceCenter, targetCenter);

        let nx = Math.abs(ray.dx);
        let ny = Math.abs(ray.dy);

        // Determine the number of straight and diagonal moves
        let nd = Math.min(nx, ny);
        let ns = Math.abs(ny - nx);
        // let lineDistance = (nd + ns);
        let rayDistance = Math.abs(ray.dx);
        // let rayDistance = (nd + ns);
        let range = rayDistance > 0 ? rayDistance - tokenSpace : 0;
        let tokenReduction = tokenSpace;
        // angles are slightly different, so if nx is < than ny, we adjust
        if (nd < ny) {
            ns = Math.abs(ny - nd);
            tokenReduction = tokenSpace - nx;
            range = ns - tokenReduction;
        }

        // const rayDistance = Math.abs(ray.dx);
        const between = range > 0 ? Math.floor((range / gridSize) * gridDistance) : 0;
        console.log(`Distance between ${sourceToken.name} and ${targetToken.name} is ${between} ${gridUnits}`);
        return between;
    }

}// end TokenDocument

export class OSRICTokenLayer extends TokenLayer {
    /**
     * 
     * This override is to push pack versions be dropped into folder called "Map Drops"
     * and to prevent it from making multiple copies there of the same creature (use local if id exists)
     * -cel
     * 
     * The bulk of this is copy/paste from foundry.js otherwise
     * 
     * @param {*} event 
     * @param {*} data 
     * @returns 
     */
    async _onDropActorData(event, data) {
        // console.log("overrides.js _onDropActorData", { event, data });

        // Ensure the user has permission to drop the actor and create a Token
        if (!game.user.can("TOKEN_CREATE")) {
            return ui.notifications.warn(`You do not have permission to create new Tokens!`);
        }

        //** new code -cel
        let actor = await game.actors.get(data.id);
        //** end new code -cel

        // Acquire dropped data and import the actor
        if (!actor) actor = await Actor.implementation.fromDropData(data);
        if (!actor.isOwner) {
            return ui.notifications.warn(`You do not have permission to create a new Token for the ${actor.name} Actor.`);
        }
        if (actor.compendium) {
            const actorData = game.actors.fromCompendium(actor);
            // actor = await Actor.implementation.create(actorData);
            //** new piece -cel
            actor = await Actor.implementation.create({ ...actorData, id: data.id, _id: data.id }, { keepId: true });
            const dumpfolder = await utilitiesManager.getFolder("Map Drops", "Actor");
            // console.log("overrides.js _onDropActorData", { dumpfolder });
            // actor.update({ 'folder': dumpfolder.id });
            // without the delay see .hud errors
            const _timeout = setTimeout(() => actor.update({ 'folder': dumpfolder.id }), 300);
            //** end new piece -cel
        }

        // Prepare the Token data
        const td = await actor.getTokenDocument({ x: data.x, y: data.y, hidden: event.altKey });

        // Bypass snapping
        if (event.shiftKey) td.updateSource({
            x: td.x - (td.width * canvas.grid.w / 2),
            y: td.y - (td.height * canvas.grid.h / 2)
        });

        // Otherwise, snap to the nearest vertex, adjusting for large tokens
        else {
            const hw = canvas.grid.w / 2;
            const hh = canvas.grid.h / 2;
            td.updateSource(canvas.grid.getSnappedPosition(td.x - (td.width * hw), td.y - (td.height * hh)));
        }
        // Validate the final position
        if (!canvas.dimensions.rect.contains(td.x, td.y)) return false;

        // Submit the Token creation request and activate the Tokens layer (if not already active)
        this.activate();
        // const cls = getDocumentClass("Token");
        // return cls.create(td, { parent: canvas.scene });
        return td.constructor.create(td, { parent: canvas.scene });
    }
}

