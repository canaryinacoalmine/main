import * as debug from "../debug.js";

/**
 * 
 * On select bring up action hud
 * 
 * @param {Token} token 
 * @param {Boolean} selected 
 * @returns 
 */
export async function actionsHUD(token, selected) {
    // console.log("hooks.js actionsHUD START", { token, selected },)

    // location where we place action buttons
    const footer = document.querySelector('#ui-bottom');

    // clean up any/all action buttons
    const _cleanPrevious = () => {
        const allActionHuds = footer.querySelectorAll(".action-buttons-menu");
        allActionHuds.forEach(actionHud => {
            actionHud.remove();
        });
    };
    const previousActions = footer.querySelector('.action-buttons-menu');
    // console.log("hooks.js actionsHUD ===>", { document, footer, previousActions })

    // not selected token, see if the action hud is for them and if so remove it
    if (!selected) {
        const ele = footer.querySelector('.action-buttons-menu');
        const id = ele?.dataset?.id || null;
        if (token.id == id) {
            // if (previousActions) previousActions.remove();
            _cleanPrevious();
        }
        if (!canvas.tokens.controlled.length) {
            _cleanPrevious();
        }
        return;
    }

    const actor = token?.actor;

    if (!actor || (!actor.isOwner && !game.user.isGM)) return;


    // see if previous actions exist and if so remove them
    _cleanPrevious();

    // add weapon & skill section
    const weaponsDisplay = await renderTemplate('systems/osric/templates/parts/actor/weaponlist-block-mini.hbs',
        {
            actor: actor,
            system: actor.system,
        })
    const skillsDisplay = await renderTemplate('systems/osric/templates/parts/actor/skills-block-mini.hbs',
        {
            actor: actor,
            system: actor.system,
        })

    // add spell memorization section
    const spellsDisplay = await renderTemplate('systems/osric/templates/parts/actor/memorization-block-mini.hbs',
        {
            actor: actor,
            system: actor.system,
        })

    // add a action based selection to TokenHUD when clicked for quick access
    const actionsDisplay = await renderTemplate('systems/osric/templates/parts/actor/actions-block-list-mini.hbs',
        {
            actor: actor,
            system: actor.system,
            actions: actor.system.actions,
            actionList: actor.system.actionList,
            actionCount: Object.keys(actor.system.actionList).length
        })

    // add a actorID tag so we can compare. If they click from one token to another display doesnt change
    let actionButtonText = `<div class="action-buttons-menu" data-id="${token.id}" >`;
    const actionHTMLText = actionButtonText.concat(
        `<div>`, skillsDisplay, `</div>`,
        `<div>`, weaponsDisplay, `</div>`,
        `<div>`, spellsDisplay, `</div>`,
        `<div>`, actionsDisplay, `</div>`,
        `</div>`)
    const allActionsHTML = footer.insertAdjacentHTML('afterbegin', actionHTMLText);

    // weapon/skills
    const charCardRoll = footer.querySelectorAll(".chatCard-roll");
    charCardRoll.forEach(chatCard => {
        chatCard.addEventListener("click", event => {
            actor.sheet._itemChatRoll.bind(actor.sheet)(event);
        });
    });

    // memslots
    const spellCardRoll = footer.querySelectorAll(".spellCard-roll");
    spellCardRoll.forEach(spellCard => {
        spellCard.addEventListener("click", event => {
            actor.sheet._itemChatRoll.bind(actor.sheet)(event)
        });
    });

    // actions
    const actionCardRoll = footer.querySelectorAll(".actionCard-roll");
    actionCardRoll.forEach(actionCard => {
        actionCard.addEventListener("click", event => {
            actor.sheet._actionChatRoll.bind(actor.sheet)(event)
        });
    });

    // console.log("hooks.js actionsHUD START", { allActionsHTML, charCardRoll, spellCardRoll, actionCardRoll })

}