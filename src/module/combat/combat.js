import * as utilitiesManager from "../utilities.js";
import * as dialogManager from "../dialog.js";
import * as debug from "../debug.js";


export class CombatManager {
    /**
     * 
     * Process damage adjustments for resists, absorb/saves/etc.
     * 
     * @param {*} sourceActor 
     * @param {*} targetToken 
     * @param {*} sourceItem 
     * @param {*} sourceAction 
     * @param {*} bDamage 
     * @param {*} dmgDone 
     * @param {*} ammo
     * @returns { total: damageTotalDone, types: damageTypes, absorbed: absorbedTotal }
     */
    static async applyDamageAdjustments(sourceActor, targetToken, sourceItem, sourceAction, bDamage, dmgDone, ammo = null) {
        console.log("combat.js processDamageModifications ", { sourceActor, targetToken, sourceItem, sourceAction, bDamage, dmgDone });

        let targetActor = targetToken ? targetToken.actor : undefined;
        let damageTotalDone = 0;
        let absorbedTotal = 0;
        let damageTypes = [];
        let clearSaveCache = false;


        /**
         * 
         * Return resisted total, only match a single damage type
         * 
         * @param {String} resistanceType (immune,resist,vuln) (-100%/-50%/+200%)
         * @param {Actor} targetActor 
         * @param {Integer} rollTotal 
         * @returns 
         */
        const _resistHelper = (targetActor, rollTotal, resistanceType = 'resist', dmgType) => {
            console.log("combat.js processDamageModifications _resistHelper", { targetActor, rollTotal, resistanceType, dmgType });
            let newTotal = rollTotal;
            findOneMatch:
            for (const effect of targetActor.getActiveEffects()) {
                for (const change of effect.changes) {
                    if (change.key === `system.mods.${resistanceType}`) {
                        const resists = change.value.toLowerCase().split(",").map(text => text.trim());
                        if (resists.includes(dmgType.toLowerCase())) {
                            switch (resistanceType) {
                                case 'immune':
                                    newTotal = 0;
                                    break;

                                case 'resist':
                                    newTotal = (Math.round(rollTotal * 0.5));
                                    break;

                                case 'vuln':
                                    newTotal = (Math.round(rollTotal * 2));
                                    break;

                            }
                            // we only match one
                            break findOneMatch;
                        }
                    }
                }
            }
            return newTotal;
        };

        const saveCache = targetToken ? await targetToken.document.getFlag("osric", "saveCache") : undefined;
        const halfDamageFromSave = saveCache ? (saveCache.save === 'halve' && saveCache.sourceId === String(sourceActor.id)) : false;
        // let checkedTargeted = false;
        // loop through each damage formula rolled
        for (let entry of dmgDone) {
            let rollTotal = entry.dmg;

            // console.log("combat.js processDamageModifications ", { entry, rollTotal });
            const damageType = entry.type;
            if (!damageTypes.includes(damageType)) damageTypes.push(damageType);


            // if this is damage, calculate resists
            if (bDamage && targetActor) {

                // Targeted damage is applied to the first damage done using it's damage type/etc
                //TODO figure out a way to add flavor that includes targeted damage applications
                // if (!checkedTargeted) {
                //     checkedTargeted = true;
                //     const targetedFormulas = await getTargedEffectModifiers(sourceActor, targetActor, sourceItem);
                //     const targeted = targetedFormulas.filter(c => c.applyToField === 'damage').reduce((partialSum, a) => partialSum + a.modifier, 0) ?? 0;
                //     if (targeted) rollTotal += targeted;
                // }

                // did they previously save and now damage is being sent
                // half damage if so.
                if (halfDamageFromSave) rollTotal = Math.round(rollTotal * 0.5);

                // check weapon for magic to hit potency and metal type that works
                if ((sourceItem && sourceItem.type === 'weapon')) {
                    let specialMetal = false; // Need to know if we found special metal in item

                    // look for magic potency (require +1/+2/+3... magic items to hit), check if mods is better
                    const magicPotencyMod = targetActor && targetActor.system?.resistances?.magicpotency ?
                        parseInt(utilitiesManager.evaluateFormulaValue(targetActor.system.resistances?.magicpotency, targetActor.getRollData())) : 0;
                    let targetMagicPotency = targetActor && targetActor.system?.resistances ?
                        (targetActor.system.resistances ? targetActor.system.resistances.weapon.magicpotency : 0) : 0;
                    if (targetMagicPotency < magicPotencyMod) targetMagicPotency = magicPotencyMod;
                    //

                    let itemPotency = 0;
                    if (sourceItem && sourceItem.type === 'weapon') {
                        itemPotency = sourceItem.system.attack.magicPotency;
                    }
                    // include ammo if it exists
                    if (ammo && ammo.system?.attack?.magicPotency) {
                        itemPotency += ammo.system?.attack?.magicPotency;
                    }

                    // we also include the action entry from the weapon, incase special settings are used for this attack
                    const actionPotency = sourceAction ? sourceAction.magicpotency : 0;
                    const actionProperties =
                        sourceAction ? Object.values(sourceAction.properties) : [];
                    const itemProperties =
                        sourceItem ? Object.values(sourceItem.system.attributes.properties || []) : [];

                    let magicPotency = sourceActor.effectiveMagicPotency;
                    if (actionPotency > magicPotency) magicPotency = actionPotency;
                    if (itemPotency > magicPotency) magicPotency = itemPotency;

                    const propertiesList =
                        itemProperties.concat(actionProperties).map(entry => { return entry.trim().toLowerCase() });

                    const metals = targetActor.type === 'npc' ? Object.values(targetActor.system.resistances?.weapon?.metals) : undefined;
                    if (metals && metals.length && propertiesList && propertiesList.length) {
                        for (const metal of metals) {
                            if (propertiesList.includes(metal.type.toLowerCase().trim())) {
                                switch (metal.protection) {
                                    case 'halve':
                                        rollTotal = Math.round(rollTotal * 0.5);
                                        break;
                                    case 'full':
                                        break;
                                }
                                if (!specialMetal) specialMetal = true;
                                break;
                            }
                        }
                    }
                    // if no special metals, check magicpotency last
                    if (!specialMetal && (magicPotency < targetMagicPotency)) { // check this last
                        rollTotal = 0;
                    }
                } // end of weapon magic/metal resists

                // TODO add flavor for resists and absorb

                //TODO resist static amount of damage type like resist.fire 2 and resist 2 fire damage
                // const resistStatic = targetActor.data.resists.filter(function (eff) { return eff.hasAmount });

                // calculate the total "perdiceValue"
                let resistPerdiceValue;
                if (targetActor.system.mods.resists?.perdice?.[damageType]) {
                    resistPerdiceValue = targetActor.system.mods.resists?.perdice?.[damageType]
                } else if (targetActor.system.mods.resists?.perdice?.all) {
                    resistPerdiceValue = targetActor.system.mods.resists.perdice.all
                }
                let resistsPerdice = parseInt(resistPerdiceValue || 0);
                // process perdice modifications first!
                if (resistsPerdice && Number.isNumeric(resistsPerdice)) {
                    rollTotal = 0;
                    // best effort to not lose modifiers outside of die rolls
                    const perRollTotal = entry.roll.total;
                    const diceRollResults = entry.roll.dice[0].total ? entry.roll.dice[0].total : 0;
                    const otherTerms = diceRollResults ? (perRollTotal - diceRollResults) : 0;
                    for (let dieRoll of entry.roll.dice) {
                        for (let perRolled of dieRoll.results) {
                            rollTotal += Math.max((perRolled.result + (parseInt(resistsPerdice))), 1);
                            // we set the adjusted value, it changes the chat card but the roll.total does not change because of this
                            // rolled.result = Math.max((rolled.result + (perdiceValue)), 1);
                        }
                    }
                    rollTotal += otherTerms;
                }

                // resists by amount 50%/100%/-100%/whatever entered reduced damage
                let resist;
                try {
                    resist = targetActor.system.mods.resists[damageType];
                } catch { }
                if (resist && Number.isNumeric(resist)) {
                    const amount = parseInt(resist);
                    if (amount && amount != 0) {
                        const resistAmount = (amount * 0.01); // get percent multiplier
                        rollTotal -= Math.round(rollTotal * resistAmount); // get Adjusted damage amount.
                    }
                }

                //// data.mods.{resist|immune|vuln} value = type like fire,cold,slashing
                rollTotal = _resistHelper(targetToken.actor, rollTotal, 'immune', damageType);
                rollTotal = _resistHelper(targetToken.actor, rollTotal, 'resist', damageType);
                rollTotal = _resistHelper(targetToken.actor, rollTotal, 'vuln', damageType);
                ////

                // Abbsorb damage
                // let absorbEffects = effectManager.getEffectsByChangesKey('ABSORB', targetToken.actor.data.effects);
                console.log("combat.js processDamageModifications ", { targetToken });
                let effectsBundle = duplicate(targetToken.actor.effects);
                let absorbed = 0;
                let depletedEffect = []
                let updatedEffects = false;
                effectsBundle.forEach((effect, efindex) => {
                    if (!effect.disabled && !effect.isSuppressed) {
                        effect.changes.forEach((change, index) => {
                            if (rollTotal > 0 && change.key === 'ABSORB') {
                                const rollTotalOrig = rollTotal;
                                let [match, amount, dmgtype] = change.value.match(/^(\d+) (\w+)$/);
                                if (amount && dmgtype) {
                                    updatedEffects = true;
                                    amount = parseInt(amount);
                                    if (dmgtype === damageType || dmgtype === 'none' || dmgtype === 'all') {
                                        let amountleft = amount;
                                        const absorbDiff = rollTotal - amount;
                                        if (absorbDiff < 0) { // absorbed it all, save the rest
                                            amountleft = Math.abs(absorbDiff);
                                            absorbed += rollTotal;
                                            rollTotal = 0;
                                        } else {
                                            rollTotal = absorbDiff;
                                            amountleft = 0;
                                            depletedEffect.push(effect._id);
                                            absorbed += amount;
                                        }
                                        change.value = `${amountleft} ${dmgtype}`;
                                        if (dmgtype === 'none') {
                                            // a none absorb effect only tracks absorbtion but doesn't absorb the damage.
                                            rollTotal = rollTotalOrig;
                                            absorbed = 0;
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
                if (updatedEffects) {
                    await utilitiesManager.runAsGM({
                        sourceFunction: 'applyDamageAdjustments',
                        operation: 'actorUpdate',
                        user: game.user.id,
                        // targetActorId: targetToken.actor.id,
                        targetTokenId: targetToken.id,
                        // sourceActorId: sourceActor.id,
                        update: { 'effects': effectsBundle },
                    });
                }
                if (depletedEffect.length)
                    await utilitiesManager.runAsGM({
                        operation: 'deleteActiveEffect',
                        user: game.user.id,
                        targetActorId: targetToken.actor.id,
                        targetTokenId: targetToken.id,
                        sourceActorId: sourceActor.id,
                        effectIds: depletedEffect,
                    });

                absorbedTotal += absorbed;
                console.log("combat.js absorbed total =", absorbed);
                // end absorb section
            }

            if (clearSaveCache) { // we used up the save for 1/2, clear flag
                utilitiesManager.deleteSaveCache(targetToken);
                clearSaveCache = false;
            }
            console.log("combat.js  applyDamageAndChat Adjusted total = ", { rollTotal }, Math.round(rollTotal));
            //TODO: apply Math.round(rollTotal) here?
            damageTotalDone += Math.round(rollTotal);
            // damageTotalDone += rollTotal;
            if (!clearSaveCache) clearSaveCache = halfDamageFromSave;
        } // end for dmg
        // targetToken, damageTotalDone, bDamage, damageTypes

        return ({ total: damageTotalDone, types: damageTypes, absorbed: absorbedTotal })
    }
    /**
     * 
     * Make a chat card for incoming damage and send it.
     * 
     * @param {*} sourceActor 
     * @param {*} targetToken 
     * @param {*} bDamage 
     * @param {*} rolled 
     * @param {*} damageTotalDone 
     * @param {*} damageTypes 
     * @param {*} damageFlavor 
     */
    static async sendHealthAdjustChatCard(sourceActor, targetToken, bDamage, rolled, damageTotalDone, damageTypes, damageFlavor = '', sourceName = '', rollMode = game.settings.get("core", "rollMode")) {
        console.log("combat.js sendHealthAdjustChatCard", { sourceActor, targetToken, bDamage, rolled, damageTotalDone, damageTypes, damageFlavor })
        let damageTypesLocalized = []
        damageTypes.forEach((dmgType) => {
            damageTypesLocalized.push(bDamage ? game.i18n.localize("OSRIC.damageTypes." + dmgType) : '');
        });

        const damageTypeString = damageTypesLocalized.join();
        const formulasString = rolled.formulas.join(" + ");
        const rawFormulasString = rolled.rawformulas.join(" + ");
        const resultsString = rolled.results.join(" + ");
        const totalsString = rolled.totals.join(" + ");
        const diceHTMLToolTips = await Promise.all(rolled.rolls.map(async (roll) => {
            return await roll.getTooltip()
        }));
        // console.log("healthAdjust diceHTMLToolTips ", { diceHTMLToolTips })

        let sFlavor = undefined;
        let nHPAdjustment = Math.max(damageTotalDone, 0);
        let nHPDiff = nHPAdjustment;
        if (targetToken) {
            const sNameTarget = targetToken.name;
            let hpPath = targetToken.actor.system.attributes.hp;
            const nTargetHPOriginalValue = hpPath.value;
            let nTargetHPResult = nTargetHPOriginalValue;

            console.log("combat.js applyDamageAndChat applyDamageAndChat", { targetToken, sNameTarget, hpPath, nHPAdjustment, nTargetHPOriginalValue, nTargetHPResult });

            if (bDamage) {
                nTargetHPResult = Math.clamped((hpPath.value - nHPAdjustment), hpPath.min, hpPath.max);
            } else {
                nTargetHPResult = Math.clamped((hpPath.value + nHPAdjustment), hpPath.min, hpPath.max);
            }

            nHPDiff = Math.abs(nTargetHPOriginalValue - nTargetHPResult);

            console.log("combat.js applyDamageAndChat nTargetHPOriginalValue ", { nTargetHPOriginalValue, nTargetHPResult, nHPDiff });
            utilitiesManager.runAsGM({
                sourceFunction: 'sendHealthAdjustChatCard',
                operation: 'adjustTargetHealth',
                user: game.user.id,
                targetTokenId: targetToken.id,
                targetActorId: targetToken.actor.id,
                targetHPresult: nTargetHPResult
            });

            //TODO this needs to be made more understandable !!
            sFlavor =
                `<div data-id="${targetToken?.id}" class="secure-name">${sNameTarget}</div>` +
                `<a data-value="${rolled.totalValues}" class="secure-value">${damageTotalDone}</a> ` +
                `${damageTypeString}${(bDamage ? " damage" : " healing")}${(damageFlavor ? ` ${damageFlavor}` : "")}`;
        } else {
            // no target
            // dont think I need secure-value here cause it should always show flat value w/o target
            sFlavor =
                `<div data-id="${targetToken?.id}" class="secure-name">${sourceName}</div>` +
                `<a data-value="${rolled.totalValues}" class="secure-value">${damageTotalDone}</a> ${damageTypeString}${(bDamage ? " damage" : " healing")}${(damageFlavor ? ` ${damageFlavor}` : "")}`;
        }

        let speaker = ChatMessage.getSpeaker({ actor: sourceActor });
        const roll = {
            formula: formulasString,
            rawformula: rawFormulasString,
            result: resultsString,
            diceToolTip: diceHTMLToolTips.join(''),
            total: rolled.totalValues,
        }
        let cardData = {
            speaker: speaker,
            flavor: sFlavor,
            tooltip: resultsString,
            isDamage: bDamage,
            adjustment: nHPDiff,
            "roll": roll,
            "sourceActorId": sourceActor.id,
            "targetActorId": targetToken?.actor.id,
            "targetToken": targetToken,
            "owner": sourceActor.id,
            "game": game,
        };

        const content = await renderTemplate("systems/osric/templates/parts/chat/chatCard-healthAdjust.hbs", cardData);

        let chatData = {
            content: content,
            user: game.user.id,
            speaker: ChatMessage.getSpeaker(),
            rollMode: rollMode,
            // roll: roll,
        };
        ChatMessage.applyRollMode(chatData, rollMode);
        ChatMessage.create(chatData);

        if (bDamage && targetToken?.combatant && targetToken.combatant.getFlag("osric", "initCasting")) {
            utilitiesManager.chatMessage(ChatMessage.getSpeaker({ actor: targetToken.actor }), 'Casting Interrupted', `${targetToken.name} has casting interrupted`, targetToken.img, rollMode);
        }
    } // end makeHealthAdjustChatCard

    /**
     * 
     * Return targeted effects formula that are triggered with this action
     * 
     * Trigger on alignement
     * target.alignment.{attack|damage}.{alignment} value is formula
     * 
     * Trigger on creature type
     * target.type.{attack|damage}.{creatureType} value is formula
     * 
     * @param {*} sourceActor Source actor
     * @param {*} targetActor Target actor
     * @param {*} sourceItem  Item used while making this attack (optional)
     * @returns [{
                    formula: formula,
                    modifier: addMod,
                    applyToField: field,
                    appliedFor: `target alignement ${field}`,
                }]
    */
    // async getTargedEffectModifiers(sourceActor, targetActor, sourceItem = undefined) {
    //     let targeted = [];
    //     const modes = game.osric.const.ACTIVE_EFFECT_MODES;
    //     for (const effect of sourceActor.effects) {
    //         // effect enabled and is the source of the effect from the item used or directly on the actor taking the action?
    //         if (!effect.disabled && !effect.isSuppressed &&
    //             ((sourceItem && effect.origin === sourceItem.uuid) ||
    //                 effect.origin === sourceActor.uuid)) {
    //             for (const change of effect.changes) {
    //                 // find alignments
    //                 if (change.mode == modes.CUSTOM && (change.key.startsWith('target.alignment.'))) {
    //                     const targetSplit = change.key.split('.');
    //                     if (targetSplit.length == 4) {
    //                         const sourceAlignment = targetActor.system.details.alignment;
    //                         const targetAlignment = targetSplit[3];
    //                         // match ne or neutralevil, lg or lawfulgood, n or neutral...etc
    //                         if (sourceAlignment === targetAlignment || game.osric.config.alignmentLongNameMap[targetAlignment] === targetAlignment) {
    //                             const field = targetSplit[2];
    //                             const formula = change.value.trim();
    //                             const addMod = await utilitiesManager.evaluateFormulaValue(formula, sourceActor.getRollData());
    //                             targeted.push({
    //                                 formula: formula,
    //                                 modifier: addMod,
    //                                 applyToField: field,
    //                                 appliedFor: `target alignement ${field}`,
    //                             })
    //                         }
    //                     }
    //                 } // end alignment

    //                 // find creature types
    //                 if (change.mode == modes.CUSTOM && (change.key.startsWith('target.type.'))) {
    //                     const targetSplit = change.key.split('.');
    //                     if (typeSplit.length == 4) {
    //                         const targetType = targetActor.system.details.type;
    //                         const findTypes = targetSplit[3].trim().toLowerCase();
    //                         if (findTypes.includes(targetType)) {
    //                             if (targetType === findType.trim().toLowerCase()) {
    //                                 const field = targetSplit[2];
    //                                 const formula = change.value.trim();
    //                                 const addMod = await utilitiesManager.evaluateFormulaValue(formula, sourceActor.getRollData());
    //                                 targeted.push({
    //                                     formula: formula,
    //                                     modifier: addMod,
    //                                     applyToField: field,
    //                                     appliedFor: `target type ${field}`,
    //                                 });
    //                             }
    //                         }
    //                     }
    //                 }// end target type

    //             }
    //         }
    //     }
    //     return targeted;
    // }

    /**
     * 
     * Gets the damaged rolled
     * 
     * @param {*} dd 
     */
    static async getRolledDamage(dd) {
        const _roll = async (localFormula, additionalRollData = undefined) => {
            let roll;
            let rollOptions = { maximize: dd.dmgAdj === 'max', async: true };
            const rollData = additionalRollData ? mergeObject(dd.source.getRollData(), additionalRollData) : dd.source.getRollData();
            // console.log("combat.js getRolledDamage _roll", { localFormula, rollData });
            try {
                roll = new Roll(localFormula, rollData, { rollMode: dd.rollMode });
                // console.log("getRolledDamage==>1", duplicate(roll))
                // console.log("getRolledDamage 1", { roll })
                if (dd.dmgAdj === 'double') roll.alter(2, 0);
                // if (dd.dmgAdj.match(/^x(\d+)/i)) { // look for x2, x3, x4/etc
                //     let match = dd.dmgAdj.match(/^x(\d+)/i);
                //     if (match && match[1]) {
                //         const multiplier = parseInt(match[1]);
                //         rolledTotal *= multiplier;
                //     }
                // }            
                await roll.roll(rollOptions);
                if (game.dice3d) await game.dice3d.showForRoll(roll, game.user, true);
            } catch (err) {
                console.error(err);
                ui.notifications.error(`Dice roll evaluation failed: ${err.message}`);
                return null;
            }

            // console.log("getRolledDamage==>1", duplicate(roll))
            return roll;
        };

        // calculate damage and result damage resists
        const rolled = {
            armorDamage: [],
            rawformulas: [],
            formulas: [],
            results: [],
            totals: [],
            rolls: [],
            totalValues: 0,
        };

        let dmgDone = [];
        for (let dmg of dd.dmgFormulas) {
            const diceRoll = dmg.formula;
            const damageType = dmg.type;
            const roll = await _roll(diceRoll, dmg.rollData);
            console.log("combat.js getRolledDamage", { roll })
            //TODO variant 2, 1 armor damage per die rolled
            const armorDamage = dd.isDamage ? roll.dice[0]?.results?.length || 0 : 0;
            // console.log("combat.js getRolledDamage totals", roll.dice[0].total, roll.total, armorDamage)

            rolled.armorDamage.push(armorDamage);
            rolled.formulas.push(roll.formula);
            rolled.rawformulas.push(dmg.rawformula);
            rolled.results.push(roll.result);
            rolled.totals.push(roll.total);
            rolled.totalValues += parseInt(roll.total);
            rolled.rolls.push(roll);

            let rolledTotal = roll.total;
            if (dd.dmgAdj === 'half') {
                rolledTotal = Math.max(Math.round(rolledTotal / 2), 1);
            } else if (dd.dmgAdj.match(/^x(\d+)/i)) { // look for x2, x3, x4/etc
                let match = dd.dmgAdj.match(/^x(\d+)/i);
                if (match && match[1]) {
                    const multiplier = parseFloat(match[1]);
                    rolledTotal += rolledTotal * multiplier;
                    if (rolledTotal < 0) rolledTotal = 1;
                }
            }
            // damage multipliers from mods.*'
            //TODO add flavor for this
            // multiplier general/all
            if (dd.source.system.mods?.damage?.multiplier?.value) {
                const multiplier = parseFloat(dd.source.system.mods.damage.multiplier.value);
                rolledTotal += rolledTotal * multiplier;
                if (rolledTotal < 0) rolledTotal = 1;
            }

            // multiplier by ranged/melee/thrown
            const attackType = (dd.options.isWeapon ? dd.item.system.attack.type : dd.action.type);
            if (dd.source.system.mods?.damage?.multiplier?.[attackType]) {
                const multiplier = parseFloat(dd.source.system.mods.damage.multiplier[attackType]);
                rolledTotal += rolledTotal * multiplier;
                if (rolledTotal < 0) rolledTotal = 1;
            }

            dmgDone.push({
                dmg: rolledTotal,
                type: damageType,
                // roll: roll, sendHealthAdjustChatCard
                roll: roll
            });
        } // end for damageFormulas

        dd.data.dmgDone = dmgDone;
        dd.data.rolled = rolled;
    }

    /**
     * Populate damage formulas
     * 
     * @param {*} targetToken 
     * @param {*} dd 
     * @returns 
     */
    static async getDamageFormulas(targetToken, dd) {
        const sourceActor = dd.source;
        const sourceItem = dd.item;
        const sourceAction = dd.action;
        const damageStyle = dd.dmgAdj;
        let bonusFormula = [];

        console.log("combat.js getDamageFormulas", { targetToken, dd });

        //TODO: effects/status dont work on actions for damage, because they do aoe/etc we dont have a target, revisit this someday
        function _bonusFormulaHelper(profDamage = 0) {
            // melee/thrown/ranged
            const attackType = (dd.options.isWeapon ? dd.item.system.attack.type : dd.action.type);

            // insert @mods
            if (dd.source.system?.mods?.damage?.value)
                bonusFormula.push("@mods.damage.value");
            // actions dont have a type of damage, just "damage"
            // if (dd.item && dd.options.isWeapon && dd.source.system?.mods?.damage?.[attackType])
            if (dd.source.system?.mods?.damage?.[attackType])
                bonusFormula.push(`@mods.damage.${attackType}`);

            // insert attack mods for effects
            const selfEffects = attackMods.effects.self.value + (dd.item ? attackMods.effects.self?.[attackType] : 0);
            // when using action (not weapon) to damage we dont check target.*.damage style effects because they do aoe damage
            const targetEffects = dd.item ? (targetToken ? attackMods.effects.target.value + attackMods.effects.target?.[attackType] : 0) : 0;

            if (selfEffects) bonusFormula.push("@selfEffects");
            if (targetEffects) bonusFormula.push("@targetEffects");

            // insert attack mods for status effects
            const selfStatus = attackMods.status.self.value + (dd.item ? attackMods.status.self?.[attackType] : 0);
            // when using action (not weapon) to damage we dont check target.*.damage style effects because they do aoe damage
            const targetStatus = dd.item ? (targetToken ? attackMods.status.target.value + attackMods.status.target?.[attackType] : 0) : 0;
            if (selfStatus) bonusFormula.push("@selfStatus");
            if (targetStatus) bonusFormula.push("@targetStatus");

            const rollData = {
                profs: profDamage,
                situational: dd?.situational?.mod,
                selfEffects: selfEffects,
                targetEffects: targetEffects,
                selfStatus: selfStatus,
                targetStatus: targetStatus,
            }

            return rollData;
        }

        let damageFormulas, largeDamageFormulas;
        let damageFlavor = "";
        if (damageStyle !== 'normal') {
            // damageFlavor = damageFlavor ? `${damageFlavor} <b>${damageStyle}</b>` : `<b>${damageStyle}</b>`;
            damageFlavor = `<b>${damageStyle}</b>`;
        }

        const statusEffects = {
            self: dd.source.statusModifiers(),
            target: targetToken?.actor.statusModifiers()
        };
        const attackMods = {
            effects: {
                self: await dd.source.getTargetCombatMods(targetToken?.actor, 'damage', dd.item),
                target: await targetToken?.actor.getAttackerCombatMods(dd.source, 'damage', dd.item),
            },
            status: {
                self: statusEffects.self.damage,
                target: statusEffects.target?.damage,
            }
        }

        let profDamage = 0;
        if (sourceItem?.type === 'weapon') {
            // get the modifiers for damage from proficiencies
            for (const profItem of sourceActor.system.proficiencies) {
                for (const weapon of Object.values(profItem.system.appliedto)) {
                    if (weapon.id === sourceItem.id) {
                        const weaponDamageMod = await utilitiesManager.evaluateFormulaValue(profItem.system.damage, sourceActor.getRollData());
                        profDamage += weaponDamageMod;
                    }
                }
            } // end profs check
        }


        if (dd.options.isWeapon && sourceItem && sourceItem.type === 'weapon') {
            // add in magic bonus damage
            const weaponFormulaDamage = sourceItem.system.damage.normal;
            const weaponFormulaBonusDamage = sourceItem.system.damage.magicBonus;

            const largeWeaponFormulaDamage = sourceItem.system.damage.large || weaponFormulaDamage;

            // addin bonus magic damage if the weapon is identified
            const formulaDamage = weaponFormulaDamage +
                ((weaponFormulaBonusDamage) ? ` + ${weaponFormulaBonusDamage}` : "");

            const largeFormulaDamage = largeWeaponFormulaDamage +
                ((weaponFormulaBonusDamage) ? ` + ${weaponFormulaBonusDamage}` : "");

            // include ammo
            const ammo = await dd.item.getAmmo(true);
            let ammoFormulaDmg = ammo ? ammo.system.damage.normal +
                ((ammo.system.damage.magicBonus) ? `+ ${ammo.system.damage.magicBonus}` : "") : '';

            // if item is ranged weapon also apply it's bonus
            if (weaponFormulaBonusDamage && ammoFormulaDmg && dd.item.system.attack.type === 'ranged') ammoFormulaDmg = `${ammoFormulaDmg} + ${weaponFormulaBonusDamage}`;
            let ammoLargeFormulaDmg = ammo ? ammo.system.damage.large +
                ((ammo.system.damage.magicBonus) ? `+ ${ammo.system.damage.magicBonus}` : "") : '';
            // if item is ranged weapon also apply it's bonus
            if (weaponFormulaBonusDamage && ammoLargeFormulaDmg && dd.item.system.attack.type === 'ranged') ammoLargeFormulaDmg = `${ammoLargeFormulaDmg} + ${weaponFormulaBonusDamage}`;

            // console.log("combat.js rollDamage", { sourceActor, targetToken, targetActor, sourceItem, bDamage, weaponFormulaDamage, weaponFormulaBonusDamage, formulaDamage, largeFormulaDamage })

            if (profDamage) bonusFormula.push("@profs");
            if (dd.situational.mod) bonusFormula.push("@situational");

            switch (sourceItem.system.attack.type) {
                case "melee":
                    if (dd.source.system.abilities.str.dmg)
                        bonusFormula.push("@abilities.str.dmg");
                    break
                case "thrown":
                    if (dd.source.system.abilities.str.dmg)
                        bonusFormula.push("@abilities.str.dmg");
                    break
                case "ranged":
                    break
                default:
                    break
            }

            const rollData = _bonusFormulaHelper(profDamage);
            console.log("combat.js getDamageFormulas", { bonusFormula, rollData, attackMods });

            const formulaTail = (bonusFormula.length ? ` + ` + bonusFormula.join('+') : '');

            let formulaCalcs = (ammoFormulaDmg ? ammoFormulaDmg : formulaDamage) + formulaTail;
            let largeFormulaCalcs = (ammoLargeFormulaDmg ? ammoLargeFormulaDmg : largeFormulaDamage) + formulaTail;

            // console.log("combat.js getDamageFormulas", { formulaCalcs, largeFormulaCalcs, ammoFormulaDmg, ammoLargeFormulaDmg })

            damageFormulas = [{
                formula: formulaCalcs,
                // rawformula: `${formulaDamage}${rawFormulaTail}`,
                rawformula: `${formulaCalcs}`,
                type: ammo ? ammo.system.damage.type : sourceItem.system.damage.type,
                rollData: rollData,
            }];
            largeDamageFormulas = [{
                formula: largeFormulaCalcs,
                // rawformula: `${largeFormulaDamage}${rawFormulaTail}`,
                rawformula: `${largeFormulaCalcs}`,
                type: ammo ? ammo.system.damage.type : sourceItem.system.damage.type,
            }];

            const otherdmg = Object.values(ammo ? ammo.system.damage.otherdmg : sourceItem.system.damage.otherdmg);
            if (otherdmg.length) {
                otherdmg.forEach(partFormula => {
                    damageFormulas.push({
                        formula: partFormula.formula,
                        rawformula: partFormula.formula,
                        type: partFormula.type,
                    });
                    largeDamageFormulas.push({
                        formula: partFormula.formula,
                        rawformula: partFormula.formula,
                        type: partFormula.type,
                    });
                });
            }

        }
        else if (sourceAction && (sourceAction.type === 'damage' || sourceAction.type === 'heal')) {
            // console.log("getDamageFormulas", { sourceAction })
            let formulaDamage = sourceAction.formula;

            if (dd.situational.mod) bonusFormula.push("@situational");
            if (profDamage) bonusFormula.push("@profs");

            if (sourceAction.type !== 'damage') {
                if (dd.source.system?.mods?.heal?.value)
                    bonusFormula.push("@mods.heal.value");
            } else {
                switch (sourceAction.ability) {
                    case "str":
                        bonusFormula.push("@abilities.str.dmg");
                        break;
                    case "dex":
                        break;

                    case "none":
                        break;

                    default:
                        break
                }
            }
            const otherdmg = Object.values(sourceAction.otherdmg);
            // Need to see if the bonusFormula exists and append if so

            const rollData = _bonusFormulaHelper(profDamage);
            console.log("combat.js getDamageFormulas 2", { bonusFormula, rollData, attackMods });

            let formulaCalcs;
            if (bonusFormula.length) {
                formulaCalcs = formulaDamage + " + " + bonusFormula.join(" + ")
            } else {
                formulaCalcs = formulaDamage;
            }

            const rawformula = `${formulaCalcs}`;
            // const rawformula = `${formulaCalcs}` + (dd.situational.mod != 0 ? ` + @situational` : '');
            // formulaCalcs = `${formulaCalcs}` + (dd.situational.mod != 0 ? ` + ${dd.situational.mod}` : '');
            damageFormulas = [{
                formula: formulaCalcs,
                rawformula: rawformula,
                type: sourceAction.damagetype,
                rollData: rollData,
            }];

            if (otherdmg.length) {
                otherdmg.forEach(partFormula => {
                    damageFormulas.push({
                        formula: partFormula.formula,
                        rawformula: partFormula.formula,
                        type: partFormula.type,
                    });
                });
            }
        }


        const returnData = { damageFlavor: damageFlavor, damageFormulas: damageFormulas, largeDamageFormulas: largeDamageFormulas };
        console.log("getDamageFormulas", { damageFlavor: damageFlavor, damageFormulas: damageFormulas, largeDamageFormulas: largeDamageFormulas })
        return returnData;
    }

    /**
     * 
     * system variant 2 uses armor damage system, 1 armor damage per die of damage
     * 
     * @param {*} target 
     * @param {*} dd 
     */
    static armorDamage(target, dd) {
        // system variant 2 uses armor damage system, 1 armor damage per die of damage
        if (game.osric.config.settings.osricVariant == '2' && game.osric.config.settings.variant2ArmorDamage) {
            let armorDamageTotal = 0;
            dd.data.rolled.armorDamage.forEach(i => armorDamageTotal += i);
            console.log("combat.js armor damage", { target, armorDamageTotal });
            if (target?.actor.system?.armors && armorDamageTotal) {
                target.actor.system.armors.forEach(armor => {
                    // .system.protection.points
                    if (armor.system.protection.points.value > 0) {
                        const newAP = (armor.system.protection.points.value - armorDamageTotal);
                        armor.update({ 'system.protection.points.value': Math.max(0, newAP) })
                        if (newAP < 1) {
                            dialogManager.dialogNotification(`Your <b>${armor.name}</b> is no longer functional.`, 'OK', 'Protection Broken')
                            // utilitiesManager.chatMessage(ChatMessage.getSpeaker(), 'Protection Broken', `Your ${armor.name} is no longer functional.`)
                        }
                    }
                })
            }
        }
    }
}