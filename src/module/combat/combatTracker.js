// import { OSRIC } from '../config.js';
import * as utilitiesManager from "../utilities.js";
import * as dialogManager from "../dialog.js";
import * as debug from "../debug.js";
import { CombatManager } from "./combat.js";

export class OSRICCombatTracker extends CombatTracker {

    /** @inheritdoc */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "combat",
            template: "systems/osric/templates/sidebar/combat-tracker.hbs",
            title: "COMBAT.SidebarTitle",
            scrollY: [".directory-list"],
            height: 500,
            resizable: true,
        });
    }

    /** @override
     * we do this so we can inject "game" and a few other things into the combat.turns and turns
     * 
     * turns[x].index gets added so we can look up the correct index even if the turns list has hidden entries
     * turns[x].combatant is added so we can look at token.object.isVisible (so we can hide things their token can't see) and a few other things.
     * 
     */
    async getData(options) {
        const data = await super.getData(options);

        data.game = game;
        // data.turn.css = `${data.turn.css}`
        // console.log("combatTracker.js getData", { data }, this);

        if (data.combat?.turns)
            for (let [i, combatant] of data.combat.turns.entries()) {
                data.combat.turns[i].index = i;
                const turnsIndex = data.turns.findIndex(entry => { return entry.id === data.combat.turns[i].id });
                if (data.turns[turnsIndex]) {
                    data.turns[turnsIndex].index = i;
                    data.turns[turnsIndex].combatant = data.combat.turns[i];
                }
            }

        // data.turns.for(turn => { console.log("combatTracker.js getData", { turn }) })
        return data;
    }

    /**
     * Handle a Combatant control toggle
     * @private
     * @param {Event} event   The originating mousedown event
     */
    async _onCombatantControl(event) {
        // console.log("combatTracker.js _onCombatantControl", { event });
        event.preventDefault();
        event.stopPropagation();
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const combat = this.viewed;
        const c = combat.combatants.get(li.dataset.combatantId);

        // console.log("combatTracker.js _onCombatantControl", { combat, c }, this);

        // Switch control action
        switch (btn.dataset.control) {

            // Toggle combatant visibility
            case "toggleHidden":
                return c.update({ hidden: !c.hidden });

            // Toggle combatant defeated flag
            case "toggleDefeated":
                return this._onToggleDefeatedStatus(c);

            // toggle casting state
            case "toggleCasting":
                await c.setFlag("osric", "initCasting", !c.getFlag("osric", "initCasting"));
                // await c.token.setFlag("osric", "initCasting", !c.token.getFlag("osric", "initCasting"));
                // this.render();
                break;

            // Actively ping the Combatant
            case "pingCombatant":
                return this._onPingCombatant(c);

            // Roll combatant initiative
            case "rollInitiative":
                return await utilitiesManager.rollCombatantInitiative(c, combat, event.ctrlKey);

            // delay turn to end of turn order +1
            case "delayTurn":
                const lastInit = combat._getLastInInitiative();
                // console.log("_onCombatantControl", c.id, combat.combatant.id);
                if (c.id == combat.combatant.id) {
                    // if the combatant and this person are the same, flip to next turn
                    // and reset initiative to last +1
                    await combat.nextTurn();
                    return combat.setInitiative(c.id, lastInit + 1)
                }
                break;
        }
    }
}

/**
 * Extend Combat to tweak initiative order/etc
 */
export class OSRICCombat extends Combat {

    /**
     * Sort initiative lowest to highest
     * 
     * @param {Token} a 
     * @param {Token} b 
     */
    _sortCombatants(a, b) {
        // return ((a.initiative > b.initiative) ? 1 : -1);
        const ia = Number.isNumeric(a.initiative) ? a.initiative : 9999;
        const ib = Number.isNumeric(b.initiative) ? b.initiative : 9999;
        const ascendingInitiative = game.settings.get("osric", "InitiativeAscending");
        let ci = ascendingInitiative ? ia - ib : ib - ia;
        if (ci !== 0) return ci;
        // dont sort this
        // let [an, bn] = [a.token?.nameRaw || "", b.token?.nameRaw || ""];
        // let cn = an.localeCompare(bn);
        // if (cn !== 0) return cn;

        // lastly...
        return a.tokenId - b.tokenId;
    }


    _getLastInInitiative() {
        let lastInit = 0;
        this.combatants.forEach(element => {
            if (element.initiative > lastInit) lastInit = element.initiative;
        });
        return lastInit;
    }
    _getFirstInInitiative() {
        let firstInit = 0;
        this.combatants.forEach(element => {
            if (element.initiative < firstInit) firstInit = element.initiative;
        });
        return firstInit;
    }
    /**
     * Returns the current turns initiative value
     * 
     * @returns Number 
     */
    _getCurrentTurnInitiative() {
        const thisTurn = this.current.turn;
        const result = this.turns[thisTurn].initiative || 0;
        return (result);
    }
    /**
     * @override to remove time passage (it's only advanced at end of round)
     * 
     * Advance the combat to the next turn
     * @return {Promise<Combat>}
     */
    async nextTurn() {
        console.log("combatTracker.js nextTurn START", this);
        let turn = this.turn ?? -1;
        let skip = this.settings.skipDefeated;

        // Determine the next turn number
        let next = null;
        if (skip) {
            for (let [i, t] of this.turns.entries()) {
                if (i <= turn) continue;
                if (t.isDefeated) continue;
                next = i;
                break;
            }
        }
        else next = turn + 1;

        // Maybe advance to the next round
        let round = this.round;
        if ((this.round === 0) || (next === null) || (next >= this.turns.length)) {
            return this.nextRound();
        }

        // Update the document, passing data through a hook first
        const updateData = { round, turn: next };
        // Classic systems update time on the round, not each turn
        // const updateOptions = { advanceTime: CONFIG.time.turnTime, direction: 1 };
        Hooks.callAll("combatTurn", this, updateData);
        return this.update(updateData);
        // return this.update({ round: round, turn: next });
    }

    /**
     * @override to make adjustments to time passage
     * 
     * Advance the combat to the next round
     * @return {Promise<Combat>}
     */
    async nextRound() {
        console.log("combatTracker.js nextRound START", this);
        let turn = this.turn === null ? null : 0; // Preserve the fact that it's no-one's turn currently.
        if (this.settings.skipDefeated && (turn !== null)) {
            turn = this.turns.findIndex(t => !t.isDefeated);
            if (turn === -1) {
                ui.notifications.warn("COMBAT.NoneRemaining", { localize: true });
                turn = 0;
            }
        }
        // let advanceTime = Math.max(this.turns.length - (this.turn || 0), 0) * CONFIG.time.turnTime;
        // advanceTime += CONFIG.time.roundTime;
        // OSR systems advance time by round counter, not turns, per round.
        const advanceTime = CONFIG.time.roundTime;
        return this.update({ round: this.round + 1, turn }, { advanceTime });
    }
    /**
     * @override time is roundTime, not turnTime for a round
     * 
     * Rewind the combat to the previous round
     * @return {Promise<Combat>}
     */
    async previousRound() {
        let turn = (this.round === 0) ? 0 : Math.max(this.turns.length - 1, 0);
        if (this.turn === null) turn = null;
        const round = Math.max(this.round - 1, 0);
        // let advanceTime = -1 * (this.turn || 0) * CONFIG.time.turnTime;
        // let advanceTime = -1 * (this.turn || 0) * CONFIG.time.roundTime;
        const advanceTime = round > 0 ? -CONFIG.time.roundTime : 0;
        return this.update({ round, turn }, { advanceTime });
    }

    /**
     * @override no time for just previous persons play turn
     * 
     * Rewind the combat to the previous turn
     * @return {Promise<Combat>}
     */
    async previousTurn() {
        if ((this.turn === 0) && (this.round === 0)) return this;
        else if ((this.turn <= 0) && (this.turn !== null)) return this.previousRound();
        // const advanceTime = -1 * CONFIG.time.turnTime;
        // return this.update({ turn: (this.turn ?? this.turns.length) - 1 }, { advanceTime });
        return this.update({ turn: (this.turn ?? this.turns.length) - 1 });
    }

    /**
     * 
     * Run "new round" processes.
     * 
     * @param {*} combat 
     */
    processNewRound() {
        // console.log("combatTracker.js newRound", { combat });
        this.combatants.forEach(entry => {
            // clear saveCache
            this.processOneRoundFlags(entry);

            //TODO this would be better on the turn start but the way
            // updateCombat/preUpdateCombat work with initiative rerolling
            // each round it's a mess and I couldn't get it working right.
            this.processOngoingHealthAdjustments(entry);
        });
    }

    /**
     * 
     * ReRoll initative at the start of a round.
     * 
     * @param {*} combat 
     */
    async reRollInitiative() {
        const rollInitEachRound = game.settings.get("osric", "rollInitEachRound");
        if (rollInitEachRound) {
            await this.resetAll();
            await this.rollNPC({ messageOptions: { bulkRolls: true, rollMode: 'blindroll' } });
            await this.update({ turn: 0 });
        }
    }

    /**
     * 
     * Process various changes/updates for start of new round
     * 
     * @param {*} tokenDocument 
     */
    processOneRoundFlags(tokenDocument) {
        console.log("processOneRoundFlags tokenDocument", { tokenDocument });
        // flush saveCache
        // tokenDocument.unsetFlag("osric", "saveCache");
        tokenDocument.token.unsetFlag("osric", "saveCache");
        //flush is Casting flag
        tokenDocument.token?.combatant.unsetFlag("osric", "initCasting");
    }

    /**
     * 
     * Adjust health for regeneration on new round
     * 
     * @param {*} token 
     */

    processOngoingHealthAdjustments(combatant) {
        // console.log("processOngoingHealthAdjustments token", combatant);
        const regenValue = combatant.actor.system?.mods?.heal?.regen ?? 0;
        // console.log("processRegen regenValue", regenValue);
        if (regenValue) utilitiesManager.adjustActorHealth(combatant.actor, regenValue);
        if (!combatant.isDefeated) this.processOngoingDamage(combatant);
    }

    processOngoingDamage(tokenDocument) {
        const modes = game.osric.const.ACTIVE_EFFECT_MODES;
        // console.log("combatTracker.js processOngoingDamage", tokenDocument);
        // combat.combatant.id;
        for (const effect of tokenDocument.actor.effects) {
            if (!effect.disabled && !effect.isSuppressed) {
                for (const change of effect.changes) {
                    if (change.mode == modes.CUSTOM && change.key.startsWith(`dmg.ongoing`)) {
                        // console.log("combatTracker.js processOngoingDamage", { tokenDocument, dmgFormula, dmgType });
                        let match = change.key.match(/[\w\.]+\s(.*)$/i);
                        if (match && match[1]) {
                            const dmgFormula = match[1].trim();
                            const dmgType = change.value.trim();
                            // console.log("combatTracker.js processOngoingDamage", { dmgFormula, dmgType });
                            const roll = new Roll(dmgFormula, tokenDocument.actor.getRollData()).roll({ async: true })
                                .then(async (roll) => {
                                    if (game.dice3d) await game.dice3d.showForRoll(roll, game.user);
                                    // calculate damage and result damage resists
                                    const rolled = {
                                        rawformulas: [roll.formula],
                                        formulas: [roll.formula],
                                        results: [roll.result],
                                        totals: [roll.total],
                                        totalValues: roll.total,
                                        rolls: [roll],
                                    };
                                    let dmgDone = [];
                                    dmgDone.push({
                                        dmg: roll.total,
                                        type: dmgType,
                                        roll: roll,
                                    });
                                    const targetToken = await canvas.tokens.get(tokenDocument.token.id);

                                    const rdmg = await CombatManager.applyDamageAdjustments(targetToken.actor, targetToken, undefined, undefined, true, dmgDone);
                                    // console.log("combatTracker.js processOngoingDamage", { targetToken, rolled, roll, dmgDone, rdmg });
                                    const card = await CombatManager.sendHealthAdjustChatCard(targetToken.actor, targetToken, true, rolled, rdmg.total, rdmg.types, '', 'Ongoing Damage');
                                    utilitiesManager.adjustActorHealth(targetToken.actor, -rdmg.total);
                                });
                        }
                    }
                }
            }
        }

    }


} // end Class OSRICCombat


/**
 * 
 * function run from hook for preUpdateCombat
 * 
 * @param {*} combat 
 * @param {*} updateData 
 * @param {*} options 
 * @param {*} userId 
 */
export async function _preUpdateCombat(combat, updateData, options, userId) {
    if (!game.user.isGM) return;
    const roundUpdate = hasProperty(updateData, "round");
    const turnUpdate = hasProperty(updateData, "turn");
    console.log("combatTracker.js _preUpdateCombat", { combat, updateData, options, userId, roundUpdate, turnUpdate });
}

/**
 * 
 * function run from hook for updateCombat
 * 
 * @param {*} combat 
 * @param {*} updateData 
 * @param {*} options 
 * @param {*} userId 
 */
export async function _updateCombat(combat, updateData, options, userId) {
    console.log("combatTracker.js _updateCombat 1", { combat, updateData, options, userId });
    const initVolumeSound = game.settings.get("osric", "initVolumeSound");
    const roundUpdate = hasProperty(updateData, "round");
    const turnUpdate = hasProperty(updateData, "turn");
    // console.log("combatTracker.js _updateCombat 2", { roundUpdate, turnUpdate }, "COMBAT:", duplicate(combat));
    // console.log("combatTracker.js _updateCombat", { combat, updateData, options, userId, roundUpdate, turnUpdate });
    if (roundUpdate) {
        const initRoundSound = game.settings.get("osric", "initRoundSound");
        const playRoundSound = game.settings.get("osric", "playRoundSound");
        if (playRoundSound) {
            // console.log("combatTracker.js _updateCombat playRoundSound", playRoundSound);
            AudioHelper.play({ src: initRoundSound, volume: initVolumeSound });
        }
        // start of the new round
        if (game.user.isGM) {
            await combat.reRollInitiative();
            combat.processNewRound();
        }
    } else if (turnUpdate) {
        // console.log("combatTracker.js _updateCombat TURNUPDATE =======>", combat.turn);
        const token = combat.combatant.token;
        // console.log("combatTracker.js _updateCombat", { token, turnUpdate }, getProperty(updateData, 'turn'));
        const playTurnSound = game.settings.get("osric", "playTurnSound");
        if (token.isOwner && playTurnSound && combat.turn !== 0) {
            // console.log("combatTracker.js _updateCombat playTurnSound", playTurnSound);
            const initTurnSound = game.settings.get("osric", "initTurnSound");
            AudioHelper.play({ src: initTurnSound, volume: initVolumeSound });
        }

        const panOnInitiative = game.settings.get("osric", "panOnInitiative");
        if (panOnInitiative && token.isOwner) {
            canvas.animatePan({ x: token.x, y: token.y, scale: 1, duration: 1000 });
            // canvas.animatePan({ x: token.data.x, y: token.data.y, scale: Math.max(1, canvas.stage.scale.x), duration: 1000 });

            // this will select the token so vision/etc is updated
            // if you use the pan option it will instant pan, not the slow method as above
            // token?.object?.control({ releaseOthers: true, pan: panOnInitiative });
            token?.object?.control({ releaseOthers: true });
        }

        // const turnCount = getProperty(updateData, "turn");
        // next Combatants Turn
    } else {
        // console.log("combatTracker.js _updateCombat");
    }

    // console.log("combatTracker.js _updateCombat");
}
