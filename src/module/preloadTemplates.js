export const preloadTemplates = async function () {
    const templatePaths = [
        //Character Sheets

        // character-sheet-tab
        "systems/osric/templates/actor/tabs/character-sheet-tab-main.hbs",


        "systems/osric/templates/parts/actor/character-general-block.hbs",
        "systems/osric/templates/parts/actor/description.hbs",
        "systems/osric/templates/parts/actor/abilityScore-block.hbs",
        "systems/osric/templates/parts/actor/save-block.hbs",
        "systems/osric/templates/parts/actor/inventorylist-block.hbs",
        "systems/osric/templates/parts/actor/inventoryList-Sorted-block.hbs",
        "systems/osric/templates/parts/actor/inventoryList-item.hbs",

        "systems/osric/templates/parts/actor/weaponlist-block.hbs",
        "systems/osric/templates/parts/actor/weaponlist-block-mini.hbs",
        "systems/osric/templates/parts/actor/currency-block.hbs",
        "systems/osric/templates/parts/actor/memorization-block.hbs",
        "systems/osric/templates/parts/actor/memorization-block-mini.hbs",
        "systems/osric/templates/parts/actor/skills-block.hbs",
        "systems/osric/templates/parts/actor/skills-block-mini.hbs",
        "systems/osric/templates/parts/actor/classlist-block.hbs",
        "systems/osric/templates/parts/actor/combat-stats-block.hbs",
        // NPC
        "systems/osric/templates/parts/actor/npc-stat-block.hbs",

        // general Actor
        "systems/osric/templates/actor/tabs/tab-combat.hbs",

        "systems/osric/templates/parts/actor/actions-block-list.hbs",
        "systems/osric/templates/parts/actor/actions-block-inventory.hbs",
        "systems/osric/templates/parts/actor/actions-block-inventory-mini.hbs",

        // hud actions
        "systems/osric/templates/parts/actor/actions-block-list-mini.hbs",
        "systems/osric/templates/parts/actor/itemsByTypeList-block.hbs",

        // Items sheets
        "systems/osric/templates/parts/item/item-header.hbs",
        "systems/osric/templates/parts/item/item-description-block.hbs",
        "systems/osric/templates/parts/item/item-nav.hbs",
        "systems/osric/templates/parts/item/actions-block-list.hbs",
        "systems/osric/templates/parts/item/subitem-ability-block.hbs",
        "systems/osric/templates/parts/item/contents-block.hbs",
        "systems/osric/templates/parts/item/item-attributes.hbs",
        "systems/osric/templates/parts/item/item-skillmods-block.hbs",
        "systems/osric/templates/parts/item/conditionals-block.hbs",


        // Actions
        "systems/osric/templates/parts/apps/action-properties-block.hbs",
        // Effects
        "systems/osric/templates/parts/active-effects.hbs",
        "systems/osric/templates/parts/actions-block.hbs",
        //mini
        "systems/osric/templates/parts/actions-block-mini.hbs",
        //sidebar
        "systems/osric/templates/sidebar/party-tracker.hbs",
        "systems/osric/templates/sidebar/combat-tracker-actor-entry.hbs",
        "systems/osric/templates/sidebar/party-dialog-addmember.hbs",
        // Chat Cards
        "systems/osric/templates/parts/chat/chatCard-actions-block.hbs",
        "systems/osric/templates/parts/chat/chat-message.hbs",
        "systems/osric/templates/parts/chat/chatCard-weapon.hbs",
        "systems/osric/templates/parts/chat/chatCard-dice-roll.hbs",
        // dialogs
        "systems/osric/templates/dialogs/dialog-quantity.hbs",
        "systems/osric/templates/dialogs/dialog-get-attack.hbs",
        "systems/osric/templates/dialogs/dialog-get-damage.hbs",
        "systems/osric/templates/dialogs/dialog-trade-requested.hbs",
        "systems/osric/templates/dialogs/dialog-trade-make-request.hbs",
        "systems/osric/templates/dialogs/parts/dialog-rollMode-selection.hbs",


        //apps
        "systems/osric/templates/apps/item-browser.hbs",
        "systems/osric/templates/apps/changelog-view.hbs",


    ];
    return loadTemplates(templatePaths);
};
