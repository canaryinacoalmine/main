// Import Modules
import { OSRIC } from './config.js';
import { registerSystemSettings } from "./settings.js";
import { preloadTemplates } from "./preloadTemplates.js";

// Overrides
import { OSRICActor } from "./actor/actor.js";
import { OSRICItem } from "./item/item.js";
import { OSRICCombatTracker, OSRICCombat } from "./combat/combatTracker.js";
import { CombatManager } from "./combat/combat.js";
import { PartySidebar } from "./sidebar/party.js";
import { OSRICToken, OSRICTokenDocument, OSRICTokenLayer } from "./token/token.js";
import { OSRICRollTable, OSRICActiveEffect, OSRICPermissionControl, OSRICJournalDirectory, OSRICRollTableDirectory, OSRICItemDirectory, OSRICPlaylistDirectory, OSRICCompendiumDirectory, OSRICFolder } from "./overrides.js";
// import { OSRICActiveEffectConfig } from "./effect/active-effect-config.js";
// Applications
import { OSRICActorSheet } from "./actor/actor-sheet.js";
import { OSRICLootableSheet } from "./actor/actor-sheet-lootable.js";
import { OSRICNPCSheet } from "./actor/actor-sheet-npc.js";
import { OSRICCharacterSheet } from "./actor/actor-sheet-character.js";
import { OSRICJournalSheet } from "./journal/journal-sheet.js";
import { OSRICItemSheet } from "./item/item-sheet.js";
import { OSRICItemBrowser } from "./apps/item-browser.js";
// import { OSRICActiveEffectConfig } from "./effect/active-effect-config.js";
import { ActionSheet } from "./apps/action-sheet.js";
// Import Helpers
// import * as chat from "./chat.js";
import * as debug from "./debug.js"
import { DiceManager } from "./dice/dice.js";
import * as effectManager from "./effect/effects.js";
import * as chatManager from "./chat.js";
import * as utilitiesManager from "./utilities.js";
import * as macrosManager from "./macros.js";
import * as actionManager from "./apps/action.js";
import * as dialogManager from "./dialog.js";
import { migrationChecks } from "./system/migration.js";
import * as initHooks from "./hooks.js"
import * as initHandlebars from "./handlebars.js"

Hooks.once('init', async function () {

  console.log("osric | Initializing OSRIC System");

  // console.log("OSRIC: game.system.template", game.system.template);

  // DEBUG hooks
  // CONFIG.debug.hooks = true;
  // 

  // Hide the v10 compatibility warnings
  // CONFIG.compatibility.mode = CONST.COMPATIBILITY_MODES.SILENT

  game.osric = {
    // Variable we use to make sure we don't run the same request if we have multiple GMs.
    runAsGMRequestIds: [],
    applications: {
      ActionSheet,
      OSRICCharacterSheet,
      OSRICNPCSheet,
      OSRICLootableSheet,
      OSRICPermissionControl,
      OSRICFolder,
      // OSRICActiveEffectConfig,
      // OSRICJournalSheet,
      OSRICJournalDirectory,
      OSRICRollTableDirectory,
      OSRICItemDirectory,
      OSRICPlaylistDirectory,
      OSRICCompendiumDirectory,
      OSRICItemBrowser,
    },
    OSRICActor,
    OSRICItem,
    OSRICActorSheet,
    // OSRICJournalSheet,
    OSRICTokenDocument,
    OSRICRollTable,
    config: OSRIC,
    const: CONST,
    diceManager: DiceManager,
    macrosManager: macrosManager,
    chatManager: chatManager,
    effectManager: effectManager,
    actionManager: actionManager,
    utilitiesManager: utilitiesManager,
    dialogManager: dialogManager,
    combatManager: CombatManager,
    migrationChecks: migrationChecks,
    library: {},
    //    chat: chat,
    // rollItemMacro: macrosManager.rollItemMacro,
  };

  // osric config
  CONFIG.OSRIC = OSRIC;

  // default initiative
  CONFIG.Combat.initiative = {
    formula: "1d10",
    decimals: 2
  };

  //TODO: this should be a configurable setting
  if (CONFIG.Combat?.skipDefeated === undefined)
    CONFIG.Combat.skipDefeated = true;

  // round/turn time (in seconds)
  CONFIG.time.roundTime = 60;
  CONFIG.time.turnTime = 600;

  // Define custom Entity classes
  CONFIG.Actor.documentClass = OSRICActor;
  CONFIG.Item.documentClass = OSRICItem;
  CONFIG.Combat.documentClass = OSRICCombat;
  CONFIG.RollTable.documentClass = OSRICRollTable;
  CONFIG.ActiveEffect.documentClass = OSRICActiveEffect;
  CONFIG.Token.documentClass = OSRICTokenDocument;
  CONFIG.Token.objectClass = OSRICToken;

  // CONFIG.Token.documentClass = OSRICFormApplication;

  /**
   * Configuration for the ChatMessage document
   * 
   * needed to tweak the chat-message template
   */
  CONFIG.ChatMessage = {
    documentClass: ChatMessage,
    collection: Messages,
    template: "systems/osric/templates/parts/chat/chat-message.hbs",
    sidebarIcon: "fas fa-comments",
    batchSize: 100
  }

  console.log("CONFIG===============", CONFIG)
  // CONFIG.Token.objectClass = OSRICToken;
  // CONFIG.ActiveEffect.sheetClass = OSRICActiveEffectConfig;
  CONFIG.Canvas.layers.tokens.layerClass = OSRICTokenLayer;
  CONFIG.ui.combat = OSRICCombatTracker;
  CONFIG.ui.journal = OSRICJournalDirectory;
  CONFIG.ui.items = OSRICItemDirectory;
  CONFIG.ui.tables = OSRICRollTableDirectory;
  CONFIG.ui.playlists = OSRICPlaylistDirectory;
  CONFIG.ui.compendium = OSRICCompendiumDirectory;
  CONFIG.ui.party = PartySidebar;
  CONFIG.Folder.documentClass = OSRICFolder;
  // this overrides/extends the PermissionControl app class, we 
  // do subfolder permission settings with this.
  DocumentOwnershipConfig = OSRICPermissionControl;

  // Register System Settings
  registerSystemSettings();

  CONFIG.Combat.initiative.formula = game.settings.get("osric", "initiativeFormula");
  CONFIG.OSRIC.settings = {
    autohitfail: game.settings.get("osric", "useAutoHitFailDice"),
    automateLighting: game.settings.get("osric", "automateLighting"),
    automateVision: game.settings.get("osric", "automateVision"),
    npcLootable: game.settings.get("osric", "npcLootable"),
    debugMode: game.settings.get("osric", "debugMode"),
    ctShowOnlyVisible: game.settings.get("osric", "ctShowOnlyVisible"),
    encumbranceIncludeCoin: game.settings.get("osric", "encumbranceIncludeCoin"),
    identificationActor: game.settings.get("osric", "identificationActor"),
    identificationItem: game.settings.get("osric", "identificationItem"),
    osricVariant: game.settings.get("osric", "osricVariant"),
    variant2ArmorDamage: game.settings.get("osric", "variant2ArmorDamage"),
  }
  // set this setting dependant on variant type
  game.settings.settings.get("osric.variant2ArmorDamage").config = CONFIG.OSRIC.settings.osricVariant == '2';

  // CONFIG.statusEffects.push(
  //   {
  //     id: "protevil",
  //     label: "EFFECT.StatusDegen",
  //     icon: "icons/svg/degen.svg",
  //     changes: [
  //       {
  //         key: 'somekey',
  //         mode: 0,
  //         priority: undefined,
  //         value: 'Protection!'
  //       },
  //     ],

  //   },
  // );

  // set defeated icon to something special
  CONFIG.controlIcons.defeated = OSRIC.icons.general.combat.effects.defeated;
  const deadIndex = CONFIG.statusEffects.findIndex((entry) => { return entry.id === 'dead' });
  if (deadIndex > -1) CONFIG.statusEffects[deadIndex].icon = OSRIC.icons.general.combat.effects.defeated;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  // Actors.registerSheet("osric", OSRICActorSheet, { makeDefault: true });
  Actors.registerSheet("osric", OSRICCharacterSheet, {
    types: ["character"],
    makeDefault: true,
    label: "OSRIC.sheet.actor.character"
  });
  Actors.registerSheet("osric", OSRICNPCSheet, {
    types: ["npc"],
    makeDefault: true,
    label: "OSRIC.sheet.actor.npc"
  });
  Actors.registerSheet("osric", OSRICLootableSheet, {
    types: ["lootable"],
    makeDefault: true,
    label: "OSRIC.sheet.actor.lootable"
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("osric", OSRICItemSheet, { makeDefault: true });

  // DocumentSheetConfig.unregisterSheet(JournalEntry, "core", JournalSheet);
  // DocumentSheetConfig.registerSheet(JournalEntry, "osric", OSRICJournalSheet, { makeDefault: true });

  // set some global consts to be used

  initHandlebars.default();

  await preloadTemplates();
});

initHooks.default();

