import { OSRIC } from '../config.js';
import { OSRICDicer } from "../dice/dicer.js";
import { DiceManager } from "../dice/dice.js";
import * as action from "../apps/action.js";
import * as utilitiesManager from "../utilities.js";
import * as dialogManager from "../dialog.js";
import * as debug from "../debug.js"

/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class OSRICActor extends Actor {

  chatTemplate = {
    "action": "systems/osric/templates/parts/chat/chatCard-action.hbs",
  }

  //getter to return actor alias
  get alias() {
    return this.system.alias;
  }

  /**
   * 
   * Returns name, wrapper for old "getter name" for v10
   * 
   * @returns 
   */
  getName() {
    if (!game.osric.config.settings.identificationActor)
      return this.name;

    if (['npc', 'lootable'].includes(this.type)) {
      if (!game.user.isGM && !this.isIdentified) {
        return this.alias ? this.alias : game.i18n.localize("OSRIC.unknownActor");
      }
    }

    return this.name;
  }

  get nameRaw() {
    return this.name;
  }
  /** getting for identified */
  get isIdentified() {
    return this.system.attributes.identified;
  }
  /**
   * 
   * called before actor created
   * 
   * @param {*} data 
   * @param {*} options 
   * @param {*} user 
   */
  async _preCreate(data, options, user) {
    // do stuff to data, options, etc
    // console.log("actor.js _preCreate", { data, options, user })
    await super._preCreate(data, options, user);
    await this._createTokenPrototype(data);
  }

  async _onCreate(data, options, userId) {
    // console.log("actor.js _onCreate", { data, options, userId })
    await super._onCreate(data, options, userId);
    await this._postCreate(data, options, userId);
  }

  /**@override */
  // async _onUpdate(data, options, userId) {
  //   console.log("actor.js _onUpdate", { data, options, userId })
  //   await super._onUpdate(data, options, userId);

  //   //TODO: use for things we need to adjust on update? 
  //   // await this.actor.update(newData????)

  // }

  async _postCreate(data, options, userId) {
    // console.log("actor.js _postCreate", { data, options, userId })
    if (!CONFIG.OSRIC.icons?.general?.actors) return;

    const normalDefaultIcon = 'icons/svg/mystery-man.svg';

    const defaultActorIcon = CONFIG.OSRIC.icons.general.actors[data.type];
    if (defaultActorIcon && this.img === normalDefaultIcon) {
      // We have to check if the current image is the normal "mystery man" icon because otherwise this replaces
      // icons during duplication. It's a hack but I don't see a way to detect duplication.
      await this.update({ img: defaultActorIcon });
    }
  }

  async _preUpdate(changed, options, user) {
    // do stuff to changed, options, etc
    // console.log("actor.js _preUpdate", { changed, options, user })
    await super._preUpdate(changed, options, user);
  }

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  async prepareData() {
    // console.log("actor.js prepareData", this, this.overrides)

    this.system.mods = {}; // do this or the mods values from ActiveEffects increment during super.prepareData();
    super.prepareData();

    const data = this.system;
    data.config = OSRIC;

    // the order of the following functions matter, keep that in mind

    const isPC = (this.type === "character");

    // @system.abilities.*, ability fields like opendoors, spell failure
    this._buildAbilityFields(data);

    // @mods.* values
    this._prepareMods(data);

    action.prepareDerivedData(this);

    this._prepareCharacterItems(data);

    // Prepare PC specific 
    if (isPC) {
      this._prepareClassProficiencySlots(data);
    } else {
      // NPC specific
    }

    // _prepareCasterLevels needs to run after _prepareCharacterItems() so we have a list of data.classes
    // @rank.levels.{arcane|divine} TOTAL (base+mods)
    this._prepareCasterLevels(data);

    //** wait for this to finish before the rest is processed */
    await this._prepareCharacterData(data);

    //-- build selection lists for 

    // setup Armor values based on worn/etc
    this._prepareArmorClass(data);

    // this._prepareSpellsByLevel(data);
    this._prepareMemorizationSlotData(data);
    await this._prepareMemorization(data);

    //

    // // make sure lighting/vision is set if this has a token
    await this.updateTokenLight();
    await this.updateTokenVision();
  }

  /**
   * Prepare Character type specific data
   */
  async _prepareCharacterData(data) {
    if (this.isOwner)
      await this._prepareClassData();
  }

  /** @override */
  prepareDerivedData() {
    // console.log("actor.js prepareDerivedData", this, this.overrides)
    super.prepareDerivedData();

    // for v10
    this.name = this.getName();

    // const data = this.system;
    const isPC = (this.type === "character");

    if (game.user.isGM) {
      // this is a backhanded way to make sure the party-tracker entry
      // is current.
      if (isPC && game.party?.getMembers().length) {
        game.party.updateMember();
      }
    }
    // console.log("actor.js prepareDerivedData", this);
  }


  /**
     * 
     * We use this to add values that can easily be accessed from
     * handlebars to generate specific output in the spell/memorization
     * section
     * 
     * @param {*} data 
     */
  _prepareMemorizationSlotData(data) {
    const osricVariant = OSRIC.settings.osricVariant;

    // console.log("actor.js _prepareMemorizationSlotData 1", duplicate(data), this)

    // data.spellInfo.slots.arcane.1 ADD #
    data.memorizations = {};
    let hasSpellSlots = false;
    let slotsCount = {};
    for (const spellType in data.spellInfo.slots) {
      slotsCount[spellType] = 0;
      let bTypeHasSlots = false;
      // console.log("actor.js _prepareMemorizationSlotData type", { spellType });
      Object.values(data.spellInfo.slots[spellType].value).forEach((slotCount, index) => {
        if (slotCount > 0) {
          hasSpellSlots = true;
          bTypeHasSlots = true;

          if (spellType === 'divine') {
            const wis = data.abilities.wis.value;
            if (wis > 25) wis = 25;
            if (wis < 1) wis = 1;
            const wisBonusSlots = game.osric.config.wisdomBonusSlots[osricVariant][wis][index];
            const disableWisBonus = this.system.classes.find(entry => entry.system.features.wisSpellBonusDisabled);
            // is disableWisBonus then we dont include high wis spell slot bonuses
            slotCount += disableWisBonus ? 0 : wisBonusSlots;
          }
          //TODO add optional bonus for high int here?
          //TODO add in bonuses from mod.spells.slots[spellType][index] = value here?
          slotsCount[spellType] += parseInt(slotCount);
        }

        // memSlots[spellType][level][index] = { name: item.name, level: level, img: item.img, cast: false, id: item.id, uuid: item.uuid, };
        // populate prepSlots that don't exist so we have blanks to fill in character sheet

        if (slotCount < 1 || !data.spellInfo.memorization[spellType][index]) {
          data.spellInfo.memorization[spellType][index] = {};
        }

        let prepSlots = Object.values(data.spellInfo.memorization[spellType][index]).length || 0;
        if (prepSlots < slotCount) { // add blank slots that were added
          for (let i = prepSlots; i < slotCount; i++) {
            data.spellInfo.memorization[spellType][index][i] = {
              name: null,
              level: index,
            }
          }
        } else if (prepSlots > slotCount) { // remove slots that no longer exist
          for (let i = prepSlots; i > slotCount; i--) {
            delete data.spellInfo.memorization[spellType][index][Object.keys(data.spellInfo.memorization[spellType][index])[Object.keys(data.spellInfo.memorization[spellType][index]).length - 1]]
          }
        }

      });

      if (bTypeHasSlots) {
        data.memorizations[spellType] = {
          'memslots': foundry.utils.deepClone(data.spellInfo.memorization[spellType]),
          'totalSlots': slotsCount[spellType],
          'spellslots': foundry.utils.deepClone(data.spellInfo.slots[spellType]),
          // 'spellsByLevel': data.spellsByLevel[spellType],
        }
      }

    }

    // console.log("actor.js _prepareMemorizationSlotData 2", duplicate(data), this)

    this.hasSpellSlots = hasSpellSlots;
  }


  /**
   * 
   * data for memslots we only need if we have a sheet populated
   * 
   * @param {*} data 
   */
  async _prepareMemorization(data) {
    // console.log("actor.js _prepareMemorization", { data }, this)
    for (const spellType of ['arcane', 'divine']) {
      if (data?.memorizations?.[spellType]) {
        for (let level = 0; level < data.memorizations[spellType].memslots.length; level++) {
          for (let slot = 0; slot < Object.values(data.memorizations[spellType].memslots[level]).length; slot++) {
            const spellId = data.memorizations[spellType].memslots[level][slot].id;
            if (spellId) {
              let spellItem = this.getEmbeddedDocument("Item", spellId)
              if (!spellItem) spellItem = await utilitiesManager.getItem(spellId);

              if (spellItem) data.memorizations[spellType].memslots[level][slot].spellItem = spellItem;
            }
          }
        }
      }
    }
  }


  /**
  * 
  * Flip through classes and calculate levels for @ranks.levels.{}
  * 
  * @param {*} data 
  */
  _prepareCasterLevels(data) {
    // console.log("actor.js", "_prepareCasterLevels", "data", data);

    // build casting structure
    data.rank = {
      levels: {
        arcane: 0,
        divine: 0
      }
    }
    data.rank.levels.arcane = parseInt(data.spellInfo.level.arcane.value + parseInt(data.mods?.levels?.arcane || 0));
    data.rank.levels.divine = parseInt(data.spellInfo.level.divine.value + parseInt(data.mods?.levels?.divine || 0));

    // Go through classes on character and get levels so we can reference as @rank.levels.fighter @rank.levels.paladin etc...
    this.system.classes.forEach(classEntry => {
      // const match = classEntry.name.match(/^(\w+)/);
      // const className = match ? match[1].toLowerCase() : '';
      const className = classEntry.name.slugify({ strict: true });
      if (className) {
        // get advancement records for this class to find level
        const advancement = classEntry.system?.advancement ? Object.values(classEntry.system.advancement) : undefined;
        const level = this.getClassLevel(classEntry);
        if (level) {
          data.rank.levels[className] =
            level + parseInt(data.mods?.levels?.[className] ? data.mods.levels[className] : 0);
        }
      }
    });

    data.rank.levels.max = this.getMaxLevel();
    data.rank.levels.min = this.getMinLevel();
    // console.log("actor.js _prepareCasterLevels====== data 2", data)
  }

  /**
   * 
   * Calculate avaliable weapon/non-weapon slots based on class/levels
   * 
   * @param {*} data 
   */
  _prepareClassProficiencySlots(data) {
    let nonProfPenalty = -5;
    let weaponProfs = 0;
    let weaponEarn = 999;
    let weaponStart = 0;
    let skillProfs = 0;
    let skillEarn = 999;
    let skillStart = 0;

    // find the best weapon/non-weapon prof slot count starting and earned per level
    this.system.classes.forEach(classEntry => {
      if (classEntry.system.proficiencies.weapon.earnLevel < weaponEarn) weaponEarn = classEntry.system.proficiencies.weapon.earnLevel;
      if (classEntry.system.proficiencies.skill.earnLevel < skillEarn) skillEarn = classEntry.system.proficiencies.skill.earnLevel;

      if (classEntry.system.proficiencies.weapon.starting > weaponStart) weaponStart = classEntry.system.proficiencies.weapon.starting;
      if (classEntry.system.proficiencies.skill.starting > skillStart) skillStart = classEntry.system.proficiencies.skill.starting;

      if (classEntry.system.proficiencies.penalty > nonProfPenalty) nonProfPenalty = classEntry.system.proficiencies.penalty;
    });

    weaponProfs += weaponStart;
    skillProfs += skillStart;

    // now apply best prof values at specific levels
    const maxLevel = this.getMaxLevel();
    for (let level = 1; level < maxLevel; level++) {
      if (level % weaponEarn === 0) {
        weaponProfs++;
      }
      if (level % skillEarn === 0) {
        skillProfs++;
      }
    }

    // calculate used weapon/non-weapon profs
    let spentWeapon = 0;
    let spentNonWeapon = 0;
    for (const profItem of data.proficiencies) {
      spentWeapon += parseInt(profItem.system.cost);
    }
    for (const skillItem of data.skills) {
      spentNonWeapon += parseInt(skillItem.system.features.cost);
    }

    data.attributes.proficiencies.weapon.used = spentWeapon;
    data.attributes.proficiencies.skill.used = spentNonWeapon;
    data.attributes.proficiencies.weapon.value = weaponProfs;
    data.attributes.proficiencies.weapon.penalty = nonProfPenalty;
    data.attributes.proficiencies.skill.value = skillProfs;
  }


  /**
   * 
   * Go through inventory, find equipped gear, set armor values
   * 
   * @param {*} data 
   */
  _prepareArmorClass(data) {
    // console.log("actor.js _prepareArmorClass start", this.name, duplicate(data));

    const useAD = (game.osric.config.settings.osricVariant == '2' && game.osric.config.settings.variant2ArmorDamage);
    data.armorClass = {};

    let bestArmor = data.attributes.ac.value;
    let bestArmorMod = 0;
    let shieldArmor = 0;
    let ringArmor = 0;
    let cloakArmor = 0;
    let otherArmor = 0;

    let wornMagicArmor = false;
    let wornArmor = false;
    let wornLeatherArmor = false;

    let wornMagicShield = false;
    let wornShield = false;

    let wornWarding = false;

    let wornRing = false;
    let bestWornRing = 0;
    let wornCloak = false;
    let bestWornCloak = 0;

    for (const item of data.armors) {

      //if using armor damage and the protection points are 0 and max protection points > 0 dont coint it.
      if (useAD && parseInt(item.system.protection.points.value) < 1 && parseInt(item.system.protection.points.max) > 0)
        continue;

      // console.log("actor.js _prepareArmorClass", item)
      if (item.system.location.state === 'equipped') {
        switch (item.system.protection.type) {
          case "armor":
          case 'warding':
            if (item.system.protection.ac < bestArmor) {

              // warding isn't "armor"
              if (item.system.protection.type === 'armor') {
                if (item.system.attributes.magic && !wornMagicArmor) wornMagicArmor = true;
                if (!wornArmor) wornArmor = true;
                if (!wornLeatherArmor && item.name.toLowerCase().includes('leather')) wornLeatherArmor = true;
              } else {
                if (!wornWarding) wornWarding = true;
              }

              bestArmor = parseInt(item.system.protection.ac);
              bestArmorMod = parseInt(item.system.protection.modifier);
            }
            break;

          case "shield":
            shieldArmor += parseInt(item.system.protection.ac) + parseInt(item.system.protection.modifier);
            if (item.system.attributes.magic && !wornMagicShield) wornMagicShield = true;
            if (!wornShield) wornShield = true;

            break;

          case "ring":
            ringArmor = parseInt(item.system.protection.modifier);
            if (!wornRing) wornRing = true;
            if (ringArmor > bestWornRing) bestWornRing = ringArmor;
            break;

          case "cloak":
            cloakArmor = parseInt(item.system.protection.modifier);
            if (!wornCloak) wornRing = true;
            if (cloakArmor > bestWornCloak) bestWornRing = cloakArmor;
            break;

          default:
            otherArmor += parseInt(item.system.protection.modifier);
            break;
        }
      } // is equipped?
    } // for all items

    /**
      * ActiveEffect
      * 
      * data.mods.ac.value 
      * data.mods.ac.base (use lowest)
      */
    if (data.mods?.ac?.base) {
      const modBase = parseInt(data.mods.ac.base);
      if (modBase < bestArmor) bestArmor = modBase;
    }
    let armorMod = 0;
    if (data.mods?.ac?.value) {
      const modValue = parseInt(data.mods.ac.value || 0);
      armorMod = -(modValue);
    }
    // TODO: add an override like data.mods.ac.baseoverride and ac.valueoverride ?

    // we flip values in AC fields so AC: 1 is good, AC: -4 would be bad 
    // so things like shield +1 with AC: 1 make sense to people
    data.armorClass.armor = bestArmor - bestArmorMod;
    data.armorClass.shield = -(shieldArmor);
    // can't wear magic armor with rings, use only best ring
    data.armorClass.ring = -(wornMagicArmor ? 0 : bestWornRing);
    // can't wear non-leather armor, or magic armor of anytype or any shield
    data.armorClass.cloak = -(!wornLeatherArmor || wornShield || wornMagicArmor ? 0 : bestWornCloak);
    data.armorClass.other = -(otherArmor);
    // get dex mod
    data.armorClass.dex = parseInt(data.abilities.dex.defensive);

    // get the best class acDexmod and apply it
    if (Object.values(this.system.activeClasses).length) {
      let acDexMod = 0;
      this.system.classes.forEach((classEntry) => {
        const newAcDex = parseInt(classEntry.classDetails?.acDex) || 0;
        if (acDexMod > newAcDex) acDexMod = newAcDex;
      });
      if (acDexMod)
        data.armorClass.dex = acDexMod;
    }


    /**
     * ActiveEffect
     * 
     * data.mods.ac.melee.value
     * data.mods.ac.ranged.value
     * data.mods.ac.melee.base (use lowest)
     * data.mods.ac.ranged.base (use lowest)
     * 
     * data.mods.ac.rear.value
     * data.mods.ac.rear.ranged
     * data.mods.ac.rear.melee
     * data.mods.ac.front.value
     * data.mods.ac.front.ranged
     * data.mods.ac.front.melee
     * 
     * let armorFront = parseInt(system.mods.ac.front.value || 0);
     * let armorRear  = parseInt(data.mods.ac.rear.value || 0);
     * 
     * let armorMeleeFront = parseInt(data.mods.ac.front.melee || 0);
     * let armorMeleeRear  = parseInt(data.mods.ac.rear.ranged || 0);
     * 
     * let armorRangedFront = parseInt(data.mods.ac.front.melee || 0);
     * let armorRangedRear  = parseInt(data.mods.ac.rear.ranged || 0);
     * 
     */

    // melee/ranged specific AC fields
    let armorRangedBase = data.armorClass.armor;
    let armorRangedMod = 0;

    if (data.mods?.ac?.ranged?.base) {
      const modValue = parseInt(data.mods.ac.ranged.base || bestArmor);
      if (modValue < armorRangedBase) armorRangedBase = modValue;
    }

    if (data.mods?.ac?.ranged?.value) {
      const modValue = parseInt(data.mods.ac.ranged.value || 0);
      armorRangedMod = -(modValue);
    }

    //------------------
    let armorRangedBaseFront = armorRangedBase;
    let armorRangedModFront = armorRangedMod;
    if (data.mods?.ac?.ranged?.front?.base) {
      const modValue = parseInt(data.mods.ac.ranged.front.base || bestArmor);
      if (modValue < armorRangedBaseFront) armorRangedBaseFront = modValue;
    }
    if (data.mods?.ac?.ranged?.front?.value) {
      const modValue = parseInt(data.mods.ac.ranged.front.value || 0);
      armorRangedModFront = -(modValue);
    }

    let armorRangedBaseRear = armorRangedBase;
    if (data.mods?.ac?.ranged?.rear?.base) {
      const modValue = parseInt(data.mods.ac.ranged.rear.base || bestArmor);
      if (modValue < armorRangedBaseRear) armorRangedBaseRear = modValue;
    }
    let armorRangedModRear = (armorRangedMod);
    if (data.mods?.ac?.ranged?.rear?.value) {
      const modValue = parseInt(data.mods.ac.ranged.rear.value || 0);
      armorRangedModRear = -(modValue);
    }

    let armorBaseRear = data.armorClass.armor;
    if (data.mods?.ac?.rear?.base) {
      const modValue = parseInt(data.mods.ac.rear.base || bestArmor);
      if (modValue < armorBaseRear) armorBaseRear = modValue;
    }

    let armorBaseFront = data.armorClass.armor;
    if (data.mods?.ac?.front?.base) {
      const modValue = parseInt(data.mods.ac.front.base || bestArmor);
      if (modValue < armorBaseFront) armorBaseFront = modValue;
    }
    let armorFrontMod = (armorMod);
    if (data.mods?.ac?.front?.value) {
      const modValue = parseInt(data.mods.ac.front.value || 0);
      armorFrontMod = -(modValue);
    }

    let armorRearMod = (armorMod);
    if (data.mods?.ac?.rear?.value) {
      const modValue = parseInt(data.mods.ac.rear.value || 0);
      armorRearMod = -(modValue);
    }

    //----------------

    // if (data.mods?.ac?.melee?.value) {
    //   const modValue = parseInt(data.mods.ac.melee.value || 0);
    //   armorMeleeMod = -(modValue);
    // }

    // let armorFront = data.mods?.ac?.front?.value ? parseInt(data.mods.ac.front.value || 0) : 0;
    // let armorRear = data.mods?.ac?.rear?.value ? parseInt(data.mods.ac.rear.value) : 0;

    // let armorMeleeFront = data.mods?.ac?.front?.melee ? parseInt(data.mods.ac.front.melee) : 0;
    // let armorMeleeRear = data.mods?.ac?.rear?.melee ? parseInt(data.mods.ac.rear.melee) : 0;

    let armorRangedFront = data.mods?.ac?.front?.ranged ? parseInt(data.mods.ac.front.ranged) : 0;
    let armorRangedRear = data.mods?.ac?.rear?.ranged ? parseInt(data.mods.ac.rear.ranged) : 0;

    // console.log("actor.js _prepareArmorClass", { armorBaseFront, armorFrontMod, armorBaseRear, armorRearMod, armorRangedFront, armorRangedRear, })

    // complete AC with everything
    data.armorClass.normal = Math.min(10,
      // data.armorClass.armor +
      armorBaseFront +
      armorFrontMod +
      data.armorClass.shield +
      data.armorClass.ring +
      data.armorClass.cloak +
      // data.armorClass.modEffects +
      data.armorClass.other +
      data.armorClass.dex
    );

    // remove shield
    data.armorClass.shieldless = Math.min(10,
      // data.armorClass.armor +
      armorBaseFront +
      armorFrontMod +
      data.armorClass.ring +
      data.armorClass.cloak +
      // data.armorClass.modEffects +
      data.armorClass.other +
      data.armorClass.dex
    )

    // remove dex/shield
    data.armorClass.rear = Math.min(10,
      // data.armorClass.armor +
      armorBaseRear +
      armorRearMod +
      data.armorClass.ring +
      data.armorClass.cloak +
      // data.armorClass.modEffects +
      data.armorClass.other
    )

    // ignore armor base AC but keep mods
    data.armorClass.touch = Math.min(10,
      data.attributes.ac.value +
      data.armorClass.shield +
      data.armorClass.ring +
      data.armorClass.cloak +
      // data.armorClass.modEffects +
      data.armorClass.other +
      data.armorClass.dex - bestArmorMod
    )

    // ignore armor base ac but keep mods minus dex/shield from rear
    data.armorClass.touchrear = Math.min(10,
      data.attributes.ac.value +
      data.armorClass.ring +
      data.armorClass.cloak +
      // data.armorClass.modEffects +
      data.armorClass.other - bestArmorMod
    )

    // ignore dex
    data.armorClass.nodex = Math.min(10,
      data.armorClass.armor +
      data.armorClass.shield +
      data.armorClass.ring +
      data.armorClass.cloak +
      // data.armorClass.modEffects +
      armorFrontMod +
      data.armorClass.other
    )

    // ranged specific AC
    data.armorClass.ranged = Math.min(10,
      parseInt(
        // armorRangedBase +
        // armorRangedMod +
        armorRangedBaseFront +
        armorRangedModFront +
        data.armorClass.shield +
        data.armorClass.ring +
        data.armorClass.cloak +
        // data.armorClass.modEffects +
        data.armorClass.other +
        armorRangedFront +
        data.armorClass.dex)
    )

    data.armorClass.rangedrear = Math.min(10,
      parseInt(
        // armorRangedBase +
        // armorRangedMod +
        armorRangedBaseRear +
        armorRangedModRear +
        data.armorClass.ring +
        data.armorClass.cloak +
        // data.armorClass.modEffects +
        data.armorClass.other +
        armorRangedRear)
    )

    // melee specific AC
    // data.armorClass.melee = parseInt(
    //   armorMeleeBase +
    //   armorMeleeMod +
    //   data.armorClass.shield +
    //   data.armorClass.ring +
    //   data.armorClass.cloak +
    //   data.armorClass.modEffects +
    //   data.armorClass.other +
    //   armorMeleeFront +
    //   data.armorClass.dex);

    // data.armorClass.meleerear = parseInt(
    //   armorMeleeBase +
    //   armorMeleeMod +
    //   data.armorClass.shield +
    //   data.armorClass.ring +
    //   data.armorClass.cloak +
    //   data.armorClass.modEffects +
    //   data.armorClass.other +
    //   armorMeleeRear);
    // data.armorClass.dex;
  }

  /**
   * 
   * @param {data} data 
   */
  _prepareMods(data) {
    // console.log("actor.js _prepareMods======", "data", data)

    // so we can use '(min(10,@level.arcane))d6' or + @mods.attack.melee
    // these will need to take into account str/dex, situational, effects/etc..
    // data.mods = {
    //   ac: {
    //     value: 0,
    //     base: 10,
    //     shield: 0,
    //   },
    //   attack: {
    //     melee: 0,
    //     ranged: 0,
    //     thrown: 0,
    //   },
    //   damage: {
    //     melee: 0,
    //     ranged: 0,
    //   },
    //   saves: {
    //     all: 0,
    //     paralyzation:0,
    //     poison: 0,
    //     death:0,
    //     rod:0,
    //     staff:0,
    //     wand:0,
    //     petrification: 0,
    //     polymorph:0,
    //     breath:0,
    //     spell:0,
    //  },
    //   checks: {
    //     all: 0,
    //   },
    //   levels: {
    //     divine: 0,
    //     arcane: 0,
    //     turnrank: 0
    //   },
    //   turnrank: {
    //   },
    // };


    // add data.level.{divine|arcane|highest}
    // add data.mods.attack.{melee|ranged|thrown}
    // add data.mods.damage.{melee|ranged|thrown}
    // data.mods.attack = {};
    // data.mods.damage = {};
    // data.mods.levels = {};

    // data.mods.saves = {}; // need to add SAVE effect 
    // data.mods.checks = {}; // need to add CHECK effect 

    // // melee
    // data.mods.attack.melee = (atkMod + meleeAttackMod) || 0;
    // data.mods.damage.melee = (dmgMod + meleeDamageMod) || 0;
    // // range
    // data.mods.attack.range = (atkMod + rangeAttackMod) || 0;
    // data.mods.damage.range = (dmgMod + rangeDamageMod) || 0;
    // // thrown
    // data.mods.attack.thrown = (dmgMod) || 0;
    // data.mods.damage.thrown = (dmgMod) || 0;
    // data.mods.levels.turnrank = turnMod || 0;

  }


  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} data The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterItems(data) {
    // console.log("actor.js _prepareCharacterItems data-->", data);

    data.weapons = this.items.filter(function (item) { return item.type === 'weapon' }).sort(utilitiesManager.sortByRecordName);

    data.potions = this.items.filter(function (item) { return item.type === 'potion' });

    data.proficiencies = this.items.filter(function (item) { return item.type === 'proficiency' }).sort(utilitiesManager.sortByRecordName);

    data.skills = this.items.filter(function (item) { return (item.type === 'skill') }).sort(utilitiesManager.sortByRecordName);

    // spells but not type scroll
    data.spells = this.items.filter(function (item) { return item.type === 'spell' && item.system?.attributes?.type.toLowerCase() !== 'scroll' }).sort(utilitiesManager.sortByRecordName);

    // spells but type set to scroll
    data.scrolls = this.items.filter(function (item) {
      return item.type === 'spell' && item.system?.attributes?.type.toLowerCase() === 'scroll'
    });

    data.armors = this.items.filter(function (item) { return game.osric.config.itemProtectionTypes.includes(item.type) });

    data.containers = this.items.filter(function (item) { return item.type === 'container' });

    data.backgrounds = this.items.filter(function (item) { return item.type === 'background' });

    data.classes = this.items.filter(function (item) { return item.type === 'class' });

    data.abilityList = this.items.filter(function (item) { return item.type === 'ability' });

    data.activeClasses = this.items.filter(function (item) { return item.type === 'class' && item.system.active });
    data.deactiveClasses = this.items.filter(function (item) { return item.type === 'class' && !item.system.active });

    // data.gear = this.items.filter(function (item) { return item.type === 'item' });
    data.gear = this.items.filter(function (item) { return game.osric.config.itemGearTypes.includes(item.type) });

    // find a race item and set it to the details.race
    data.races = this.items.filter(function (item) { return item.type === 'race' });
    data.details.race = data.races[0];

    data.inventory = this.items.filter(function (item) {
      return !game.osric.config.nonInventoryTypes.includes(item.type)
    });

    data.inventory.sort(utilitiesManager.sortByRecordName);

    // for now we're excluding spells, we'll need to sort out how to do scrolls if we want to use them like that
    // TODO How to deal with this for scrolls?
    data.actionInventory = this.items.filter(function (item) {
      if (!item.isIdentified) return false;
      if ((item.type === 'spell' && item.system?.attributes?.type.toLowerCase() === 'scroll'))
        return true;
      if (item.type !== 'spell') return true;
    });
    data.actionInventory.sort(utilitiesManager.sortByRecordName);

    // calculate weight carried/encumbrance
    const weightItems = this.items.filter((item) => {
      return ['equipped', 'carried'].includes(item.system?.location?.state);
    });
    let carriedWeight = 0;
    weightItems.forEach(item => {
      const count = parseFloat(item.system?.quantity) || 0;
      const weight = parseFloat(item.system?.weight) || 0;
      carriedWeight += (count * weight);
    });
    //TODO: add coin weight carried 
    if (game.osric.config.settings.encumbranceIncludeCoin) {
      const coinWeight = (1 / OSRIC.currencyWeight[game.osric.config.settings.osricVariant]);
      let coinCount = 0;
      for (const coin in data.currency) {
        if (!isNaN(data.currency[coin]))
          coinCount += parseInt(data.currency[coin]) || 0;
      }
      if (coinCount) {
        const coinTotalWeight = (coinCount * coinWeight);
        carriedWeight += coinTotalWeight;
      }
    }

    this.carriedweight = Number(carriedWeight).toFixed(2);
    //

  }

  /**
     * This populates the ability fields for the sheet display and
     * it also populates values that can be used for modifiers such as 
     * @data.abilities.str.dmg 
     * 
     * @param {*} data 
     */
  _buildAbilityFields(data) {
    const osricVariant = OSRIC.settings.osricVariant;
    for (let [key, abl] of Object.entries(data.abilities)) {
      // sanity checks.
      if (abl.value > 25) abl.value = 25;
      if (abl.value < 1) abl.value = 1;

      // console.log("_buildAbilityFields", { abl })
      switch (key) {
        case "str":
          let strValue = abl.value;
          if (abl.value === 18 && abl.percent > 0) {
            if (abl.percent >= 100) {
              strValue = 100;
            } else if (abl.percent >= 91) {
              strValue = 99;
            } else if (abl.percent >= 76) {
              strValue = 90;
            } else if (abl.percent >= 51) {
              strValue = 75;
            } else {
              strValue = 50;
            }
          }
          abl.fields = {
            hit: {
              value: OSRIC.strengthTable[osricVariant][strValue][0],
              label: OSRIC.strengthTable[osricVariant][0][0]
            },
            dmg: {
              value: OSRIC.strengthTable[osricVariant][strValue][1],
              label: OSRIC.strengthTable[osricVariant][0][1]
            }
            ,
            allow: {
              value: OSRIC.strengthTable[osricVariant][strValue][2],
              label: OSRIC.strengthTable[osricVariant][0][2]
            }
            ,
            press: {
              value: OSRIC.strengthTable[osricVariant][strValue][3],
              label: OSRIC.strengthTable[osricVariant][0][3]
            }
            ,
            open: {
              value: OSRIC.strengthTable[osricVariant][strValue][4],
              label: OSRIC.strengthTable[osricVariant][0][4]
            },
            bendbars: {
              value: OSRIC.strengthTable[osricVariant][strValue][5],
              label: OSRIC.strengthTable[osricVariant][0][5]
            }
          };
          abl.hit = OSRIC.strengthTable[osricVariant][strValue][0];
          abl.dmg = OSRIC.strengthTable[osricVariant][strValue][1];
          abl.allow = OSRIC.strengthTable[osricVariant][strValue][2];
          abl.press = OSRIC.strengthTable[osricVariant][strValue][3];
          abl.open = OSRIC.strengthTable[osricVariant][strValue][4];
          abl.bendbars = OSRIC.strengthTable[osricVariant][strValue][5];
          break;

        case "dex":
          abl.fields = {
            reaction: {
              value: OSRIC.dexterityTable[osricVariant][abl.value][0],
              label: OSRIC.dexterityTable[osricVariant][0][0]
            },
            missile: {
              value: OSRIC.dexterityTable[osricVariant][abl.value][1],
              label: OSRIC.dexterityTable[osricVariant][0][1]
            },
            defensive: {
              value: OSRIC.dexterityTable[osricVariant][abl.value][2],
              label: OSRIC.dexterityTable[osricVariant][0][2]
            }
          };
          abl.reaction = OSRIC.dexterityTable[osricVariant][abl.value][0],
            abl.missile = OSRIC.dexterityTable[osricVariant][abl.value][1];
          abl.defensive = OSRIC.dexterityTable[osricVariant][abl.value][2];
          break;

        case "con":
          abl.fields = {
            hp: {
              value: OSRIC.constitutionTable[osricVariant][abl.value][0].join('/'),
              label: OSRIC.constitutionTable[osricVariant][0][0]
            },
            shock: {
              value: OSRIC.constitutionTable[osricVariant][abl.value][1],
              label: OSRIC.constitutionTable[osricVariant][0][1]
            },
            survival: {
              value: OSRIC.constitutionTable[osricVariant][abl.value][2],
              label: OSRIC.constitutionTable[osricVariant][0][2]
            },
            poison: {
              value: OSRIC.constitutionTable[osricVariant][abl.value][3],
              label: OSRIC.constitutionTable[osricVariant][0][3]
            },
            regen: {
              value: OSRIC.constitutionTable[osricVariant][abl.value][4],
              label: OSRIC.constitutionTable[osricVariant][0][4]
            }
          };
          abl.hp = OSRIC.constitutionTable[osricVariant][abl.value][0];
          abl.shock = OSRIC.constitutionTable[osricVariant][abl.value][1];
          abl.survival = OSRIC.constitutionTable[osricVariant][abl.value][2];
          abl.poison = OSRIC.constitutionTable[osricVariant][abl.value][3];
          abl.regen = OSRIC.constitutionTable[osricVariant][abl.value][4];
          break;

        case "int":
          abl.fields = {
            languages: {
              value: OSRIC.intelligenceTable[osricVariant][abl.value][0],
              label: OSRIC.intelligenceTable[osricVariant][0][0]
            },
            level: {
              value: OSRIC.intelligenceTable[osricVariant][abl.value][1],
              label: OSRIC.intelligenceTable[osricVariant][0][1]
            },
            chance: {
              value: OSRIC.intelligenceTable[osricVariant][abl.value][2],
              label: OSRIC.intelligenceTable[osricVariant][0][2]
            },
            max: {
              value: OSRIC.intelligenceTable[osricVariant][abl.value][3],
              label: OSRIC.intelligenceTable[osricVariant][0][3]
            },
            imm: {
              value: OSRIC.intelligenceTable[osricVariant][abl.value][4],
              label: OSRIC.intelligenceTable[osricVariant][0][4]
            }
          };
          abl.languages = OSRIC.intelligenceTable[osricVariant][abl.value][0];
          abl.level = OSRIC.intelligenceTable[osricVariant][abl.value][1];
          abl.chance = OSRIC.intelligenceTable[osricVariant][abl.value][2];
          abl.max = OSRIC.intelligenceTable[osricVariant][abl.value][3];
          abl.imm = OSRIC.intelligenceTable[osricVariant][abl.value][4];
          break;

        case "wis":
          abl.fields = {
            magic: {
              value: OSRIC.wisdomTable[osricVariant][abl.value][0],
              label: OSRIC.wisdomTable[osricVariant][0][0]
            },
            bonus: {
              value: OSRIC.wisdomTable[osricVariant][abl.value][1],
              label: OSRIC.wisdomTable[osricVariant][0][1],
              tip: abl.value >= 17 ? OSRIC.wisdomTable[osricVariant][(parseInt(abl.value) + 100)][1] : '',
            },
            failure: {
              value: OSRIC.wisdomTable[osricVariant][abl.value][2],
              label: OSRIC.wisdomTable[osricVariant][0][2]
            },
            imm: {
              value: OSRIC.wisdomTable[osricVariant][abl.value][3],
              label: OSRIC.wisdomTable[osricVariant][0][3],
              tip: abl.value >= 17 ? OSRIC.wisdomTable[osricVariant][(parseInt(abl.value) + 100)][3] : '',
            }
          };
          abl.magic = OSRIC.wisdomTable[osricVariant][abl.value][0];
          abl.bonus = OSRIC.wisdomTable[osricVariant][abl.value][1];
          abl.failure = OSRIC.wisdomTable[osricVariant][abl.value][2];
          abl.imm = OSRIC.wisdomTable[osricVariant][abl.value][3];
          break;

        case "cha":
          abl.fields = {
            max: {
              value: OSRIC.charismaTable[osricVariant][abl.value][0],
              label: OSRIC.charismaTable[osricVariant][0][0]
            },
            loyalty: {
              value: OSRIC.charismaTable[osricVariant][abl.value][1],
              label: OSRIC.charismaTable[osricVariant][0][1]
            },
            reaction: {
              value: OSRIC.charismaTable[osricVariant][abl.value][2],
              label: OSRIC.charismaTable[osricVariant][0][2]
            }
          };
          abl.max = OSRIC.charismaTable[osricVariant][abl.value][0];
          abl.loyalty = OSRIC.charismaTable[osricVariant][abl.value][1];
          abl.reaction = OSRIC.charismaTable[osricVariant][abl.value][2];
          break;

        default:
          break;
      }
    }

  }

  /**
   * return the token for this actor
   */
  getToken() {
    let token;
    if (this.type !== 'character') {
      token = this.token;
    } else {
      token = this.getActiveTokens(true, true)[0];
      if (!token) token = this.sheet?.token;
    }
    return token;
  }

  /**
   * return the tokenid for this actor 
   */
  getTokenId() {
    // console.log("actor.js getTokenId", this)
    if (this.token) {
      return this.getToken()?.id;
    } else {
      return this.getToken.id;
    }

  }

  /**
   * 
   * _chatRoll: create chatCard for action/item/etc
   * 
   * @param {*} data 
   */
  async _chatRoll(data = {}, actionSource = this) {
    let chatData = {
      user: game.user.id,
      speaker: ChatMessage.getSpeaker()
    };

    const token = this.getToken();

    // console.log("actor.js _chatRoll this ", this);
    // console.log("actor.js _chatRoll token===================", { token });
    console.log("actor.js _chatRoll", { data, actionSource });

    const actions = data.actionGroup ? actionSource.system.actionList[data.actionGroup].actions : actionSource.system.actionList;
    const rAG = actionSource.system.actionList[data.actionGroup];

    let cardData = {
      ...this,
      item: data.item,
      sourceActor: this,
      sourceToken: token,
      owner: this.id,
      actions: actions,
      actionGroupData: rAG,
      actionGroups: actionSource.system.actionList,
      ...data
    };

    chatData.content = await renderTemplate(this.chatTemplate[data.type], cardData);

    return ChatMessage.create(chatData);
  }


  /**
   * 
   * Return the level of this class record
   * 
   * @param {*} classEntry 
   * @returns 
   */
  getClassLevel(classEntry) {
    let level = 0;
    const advancement = classEntry.system?.advancement ? Object.values(classEntry.system.advancement) : undefined;
    if (advancement && advancement.length) {
      // level = advancement[advancement.length - 1].level;
      level = advancement.length;
    }
    return level;
  }

  /**
   * Get max level of this actor
   * 
   * @returns maxLevel
   */
  getMaxLevel() {
    let maxLevel = 1;
    this.system.classes.forEach((classEntry) => {
      const classSource = this.items.get(classEntry.id);
      const advBundle = Object.values(classSource.system.advancement);
      if (advBundle.length > maxLevel) maxLevel = advBundle.length;
    });

    return maxLevel;
  }
  getMinLevel() {
    let minlevel = 1;
    this.system.classes.forEach((classEntry) => {
      const classSource = this.items.get(classEntry.id);
      const advBundle = Object.values(classSource.system.advancement);
      if (advBundle.length < minlevel) minlevel = advBundle.length;
    });

    return minlevel;
  }
  /**
    * 
    * @returns max level for active class
    */
  getMaxActiveLevel() {
    let maxLevel = 1;
    this.system.classes.forEach((classEntry) => {
      const classSource = this.items.get(classEntry.id);
      const advBundle = Object.values(classSource.system.advancement);
      if (classSource.active && advBundle.length > maxLevel) maxLevel = advBundle.length;
    });

    return maxLevel;
  }

  /**
   * 
   * Return the max active classes for this character
   * 
   * @returns 
   */
  getActiveClassCount() {
    let classCount = 0;
    this.system.classes.forEach((classEntry) => {
      if (classEntry.system.active) classCount++;
    });

    return classCount;
  }

  /**
   * 
   * @returns max level for deactive class
   */
  getMaxDeactiveLevel() {
    let maxLevel = 1;
    this.system.classes.forEach((classEntry) => {
      const classSource = this.items.get(classEntry.id);
      const advBundle = Object.values(classSource.system.advancement);
      if (!classSource.active && advBundle.length > maxLevel) maxLevel = advBundle.length;
    });

    return maxLevel;
  }


  /**
   * 
   * Setup options on prototype token
   * 
   * @param {*} data 
   */
  async _createTokenPrototype(data) {
    console.log("actor.js __createTokenPrototype", { data }, duplicate(data));
    let createData = {}

    // if (!data.prototypeToken) {
    if (data.type === "character") {
      console.log("actor.js _preCreate createData:PC");
      mergeObject(createData,
        {
          // "token.bar1": { "attribute": "attributes.hp" },           // Default Bar 1 to hp
          "prototypeToken.displayName": CONST.TOKEN_DISPLAY_MODES.ALWAYS,    // Default display name to be on always
          "prototypeToken.displayBars": CONST.TOKEN_DISPLAY_MODES.ALWAYS,    // Default display bars to be on always
          "prototypeToken.disposition": CONST.TOKEN_DISPOSITIONS.FRIENDLY,   // Default disposition to friendly
          "prototypeToken.name": data.name,                                  // Set token name to actor name
          "prototypeToken.vision": true,
          "prototypeToken.actorLink": true,
        });
      // Default characters to HasVision = true and Link Data = true
      // createData.token.vision = true;
      // createData.token.actorLink = true;
    } else if (data.type === 'npc') {
      // NPC settings
      console.log("actor.js _preCreate createData:NPC");
      let visionRange = 0;
      let sizeSetting = 1;
      if (data.system) {
        const visionCheck = data.system?.specialDefenses + data.system?.specialAttacks;
        const match = visionCheck?.match(/vision (\d+)/i);
        if (match && match[1]) visionRange = parseInt(match[1]);
        switch (data.system.attributes.size) {
          case 'tiny':
            sizeSetting = 0.25;
            break;
          case 'small':
            sizeSetting = 0.5;
            break;
          case 'medium':
            sizeSetting = 1;
            break;
          case 'large':
            sizeSetting = 1;
            break;
          case 'huge':
            sizeSetting = 2;
            break;
          case 'gargantuan':
            sizeSetting = 3;
            break;
        }

      }
      mergeObject(createData,
        {
          "prototypeToken.vision": visionRange ? true : false,
          "prototypeToken.brightSight": visionRange,
          "prototypeToken.dimSight": visionRange,
          "prototypeToken.width": sizeSetting,
          "prototypeToken.height": sizeSetting,
          // "prototypeToken.bar1": { "attribute": "attributes.hp" },                 // Default Bar 1 to hp
          "prototypeToken.displayName": CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,     // Default display name to be on always for owner
          "prototypeToken.displayBars": CONST.TOKEN_DISPLAY_MODES.OWNER,           // Default display bars to be on always for owner
          "prototypeToken.disposition": CONST.TOKEN_DISPOSITIONS.HOSTILE,          // Default disposition to hostile
          "prototypeToken.name": data.name                                         // Set token name to actor name
        }, { insertValues: true });                                       // sometimes npcs dont have token.bar1, this makes sure it is created
    } else if (data.type === 'lootable') {

      mergeObject(createData,
        {
          "prototypeToken.vision": false,
          "prototypeToken.brightSight": 0,                                         // default vision range is 0 w/o light
          "prototypeToken.dimSight": 0,
          "prototypeToken.displayName": CONST.TOKEN_DISPLAY_MODES.OWNER,          // Default display name to be on always for owner
          "prototypeToken.displayBars": CONST.TOKEN_DISPLAY_MODES.NONE,           // Default display bars to be on always for owner
          "prototypeToken.disposition": CONST.TOKEN_DISPOSITIONS.HOSTILE,          // Default disposition to hostile
          "prototypeToken.name": data.name                                         // Set token name to actor name
        }, { insertValues: true });                                       // sometimes npcs dont have token.bar1, this makes sure it is created
    } else {
      // unknown actor type
    }

    console.log("actor.js _createTokenPrototype", { createData });
    // await this.data.update(createData);
    // await this.update(createData);
    await this.updateSource(createData);
    // }
  }


  /**
  * 
  * @override We override to remove sub-items also
  * 
  * @param {String} embeddedName The name of the embedded Document type
  * @param {Array.<string>} ids An array of string ids for each Document to be deleted
  * @param {{}} contextopt  Additional context which customizes the deletion workflow
  * @returns 
  */
  async deleteEmbeddedDocuments(embeddedName, ids, contextopt) {
    // console.log("actor.js EmbeddedDocuments delete", { embeddedName, ids, contextopt });
    // console.trace();
    if (embeddedName === 'Item') {
      for (const itemId of ids) {
        const source = this.items.get(itemId);
        if (source && source.system?.itemList && source.system.itemList.length) {
          let itemIdList = [];
          for (const entry of source.system.itemList) {
            const deleteItem = this.getEmbeddedDocument("Item", entry.id);
            if (deleteItem) itemIdList.push(entry.id);
          }
          if (itemIdList.length) {
            if (!contextopt?.hideChanges && !this.isLooting)
              await dialogManager.dialogShowItems(this, itemIdList, `Contained Items Deleted`, `Items Removed`);
            await this.deleteEmbeddedDocuments("Item", itemIdList, contextopt);
          }
        }
      }
    }
    return super.deleteEmbeddedDocuments(embeddedName, ids, contextopt);
  }

  /**
   * 
   * Override to create itemList sub-items
   * 
   * @param {String} embeddedName The name of the embedded Document type
   * @param {Array.<object>} data An array of data objects used to create multiple documents
   * @param {DocumentModificationContext} contextopt Additional context which customizes the creation workflow
   */
  async createEmbeddedDocuments(embeddedName, data, contextopt) {
    console.log("actor.js createEmbeddedDocuments create", { embeddedName, data, contextopt });

    for (const newItem of data) {
      const itemFound = utilitiesManager.findSimilarItem(this, newItem);
      if (itemFound) {
        // console.log("actor.js createEmbeddedDocuments create", { itemFound });
        const combine = await dialogManager.dialogConfirm(`Combine ${newItem.name} and ${itemFound.name}?`, `Combine Items?`);
        if (combine) {
          const addQuantity = parseInt(newItem.system.quantity);
          await itemFound.update({ 'system.quantity': (parseInt(itemFound.system.quantity) + addQuantity) })
          const index = data.findIndex(item => item.name === newItem.name);
          if (index !== -1) {
            data.splice(index, 1);
          }
        }
      }
    }
    // if we didnt splice (dups) everything out then we continue
    if (data.length) {
      // console.trace();
      if (embeddedName === 'Item') {
        // console.log("actor.js createEmbeddedDocuments checking for sub-items");
        for (const source of data) {
          // console.log("actor.js createEmbeddedDocuments source", { source });

          // check for sub-items and add them
          if (source && source.system?.itemList && source.system.itemList.length) {
            const createSubItems = [];
            const addedSubItemIds = [];
            for (const subItem of source.system.itemList) {
              // console.log("actor.js createEmbeddedDocuments sub-item", { subItem });
              // we manage 'class' subitems as characters level up
              // so if it's a class item we skip it and if the id is same as us we skip it
              if (source.type !== 'class' && subItem.id !== source._id) {
                // let newSubItem = game.items.get(subItem.id);
                let newSubItem = subItem.uuid ? await fromUuid(subItem.uuid) : await utilitiesManager.getItem(subItem.id);
                // if (!newSubItem && subItem.uuid) newSubItem = await fromUuid(subItem.uuid);
                // console.log("actor.js createEmbeddedDocuments newSubItem 2", { newSubItem });
                if (newSubItem) {
                  const newItemData = foundry.utils.deepClone(newSubItem.toObject());
                  // console.log("actor.js createEmbeddedDocuments", { newItemData });
                  if (newItemData) {
                    createSubItems.push(newItemData);
                  }
                }
              }
            }

            if (createSubItems.length) {
              const newList = await this.createEmbeddedDocuments("Item", createSubItems, contextopt);
              // rebuild itemList
              if (newList.length) {
                source.system.itemList = [];
                for (const itm of newList) {
                  addedSubItemIds.push(itm.id);
                  source.system.itemList.push(utilitiesManager.makeItemListRecord(itm));
                }
                if (!contextopt?.hideChanges && !this.isLooting)
                  await dialogManager.dialogShowItems(this, addedSubItemIds, `Included Items`, `Items Added`)
              }
              // console.log("actor.js createEmbeddedDocuments newList", { newList });
            }
          }
          // save the original origin/source item id to the item
          if (source.system) source.system.sourceId = source._id;
        }
      }

      let items;
      try {
        items = await super.createEmbeddedDocuments(embeddedName, data, contextopt);
        for (let i = 0; i < items.length; i++) {
          const item = items[i];
          switch (item.type) {

            case 'background':
            case 'race':
              if (this.sheet) {
                this.sheet._reconfigureAcademics(item, this.getMaxLevel());
              }
              break

            case 'weapon':
              const foundProf = utilitiesManager.findSimilarItem(this, item, ['weapon'], 'proficiency');
              if (foundProf) {
                const include = await dialogManager.dialogConfirm(`Add ${item.name} to proficiency ${foundProf.name}?`, `Include in proficiency?`);
                if (include) {
                  let appliedBundle = foundry.utils.deepClone(Object.values(getProperty(foundProf, "system.appliedto")) || []);
                  // appliedBundle = Object.values(appliedBundle);
                  appliedBundle.push({ id: item.id })
                  await foundProf.update({ "system.appliedto": appliedBundle });
                }
              }
              break;

            default:
              break;
          }
        }

      } catch (error) {
        ui.notifications.warn(`Error: ${error}.`);
        // console.log("actor.js createEmbeddedDocuments", error);
      }

      return items;
    }
    return undefined;
  }

  // //TODO: experimenting, maybe remove this if I dont use it in a few days
  // _onCreateEmbeddedDocuments(type, documents, result, options, userId) {
  //   console.log("actor.js _onCreateEmbeddedDocuments", { type, documents, result, options, userId });
  //   super._onCreateEmbeddedDocuments(type, documents, result, options, userId);

  //   // const changes = [];
  //   // documents.forEach((item) => {
  //   //   switch (item.type) {
  //   //     case 'ability':
  //   //       // for (const property in item.system.attributes?.properties) {
  //   //       Object.values(item.system.attributes?.properties)?.forEach((entry, index) => {
  //   //         console.log("actor.js _onCreateEmbeddedDocuments", { entry, index });
  //   //       });
  //   //       break;
  //   //   }
  //   // });
  // }

  /**
   * Does this actor have spell slots
   * 
   * @returns boolean
   */
  canCastSpells() {
    let hasSlots = false;
    for (const spellType in this.system.spellInfo.slots) {
      Object.values(this.system.spellInfo.slots[spellType].value).forEach((slotCount, index) => {
        if (slotCount > 0) {
          hasSlots = true;
        }
      });
    }
    return hasSlots;
  }

  /**
   * getting to retrieve complete inventory list for Equipment tab
   * This sorts it by alpha and containers.
   */
  get inventoryItems() {
    // console.log("actor.js inventoryItems", this)
    const sortedItems = foundry.utils.deepClone(Array.from(this.items));
    // console.log("actor.js inventoryItems", { sortedItems })
    sortedItems.sort(utilitiesManager.sortBySort);
    const inventory = [];

    for (const item of sortedItems) {
      // console.log("actor.js inventoryItems", item.name, item.inContainer)
      if (!game.osric.config.nonInventoryTypes.includes(item.type) && !item.inContainer) {
        inventory.push(item);
        if (item.contains) {
          const sortSubItems = item.contains;
          sortSubItems.sort(utilitiesManager.sortBySort);
          for (const subItem of sortSubItems) {
            if (!game.osric.config.nonInventoryTypes.includes(item.type))
              inventory.push(subItem);
          }
        }

      }
    }
    // console.log("actor.js inventoryItems", { inventory })
    return inventory;
  }

  // is this npc dead?
  get isDead() {
    if (this.type === 'character') return false;
    // console.log("actor.js isDead", this.system.attributes.hp)
    return (this.system.attributes.hp.max !== 0 && this.system.attributes.hp.value === 0);
  }

  get isLootable() {

    // console.log("actor.js permission", this.isDead, game.osric.config.settings.npcLootable, this.system.lootable)

    switch (this.type) {
      case 'npc':
        const npcLootable = game.osric.config.settings.npcLootable;
        if (this.isDead && npcLootable) return true;
        break;

      case 'lootable':
        const lootable = this.system.lootable;
        // if this is loot actor and it's a token
        if (lootable && this.isToken) return true;
        break;
    }

    return false;
  }

  // give pcs limited view if the npc is lootable
  get permission() {
    // console.log("actor.js permission", super.permission, Math.max(super.permission, 1), this.isLootable)
    if (game.user.isGM || !this.isLootable) {
      return super.permission;
    }
    if (['lootable', 'npc'].includes(this.type)) return Math.max(super.permission, 1);
  }

  /**
   * 
   * give pcs limited permissions if npc is lootable (dead)
   * 
   * @param {*} user 
   * @param {*} permission 
   * @param {*} options 
   * @returns 
   */
  testUserPermission(user, permission, options) {
    // console.log("actor.js testUserPermission", [user, permission, options])
    if (game.user.isGM || !this.isLootable) {
      return super.testUserPermission(user, permission, options);
    }
    if ([1, "LIMITED"].includes(permission) && !options) {
      return this.permission >= CONST.DOCUMENT_PERMISSION_LEVELS.LIMITED;
    }
    return super.testUserPermission(user, permission, options);
  }

  // re-render if sheet is showing
  reRender() {
    if (this.sheet && this.type === 'character') {
      if (this.sheet.rendered)
        this.sheet.render(true);
    }

  }


  /**
   * Perform a long rest for the actor
   * 
   * reset memorizations for the day
   * reset all items with daily resets
   * 
   */
  longRest() {
    console.log("actor.js longRest:", this.name)
    let updates = [];
    // reset all spell memorizations
    const memBundle = foundry.utils.deepClone(this.system.spellInfo.memorization);
    for (const spellType of ['arcane', 'divine']) {
      for (let level = 0; level < memBundle[spellType].length; level++) {
        for (let slot = 0; slot < Object.values(memBundle[spellType][level]).length; slot++) {
          memBundle[spellType][level][slot].cast = false;
        }
      }
    }
    //

    // reset daily actions on actor directly
    const actionBundle = foundry.utils.deepClone(this.system.actons) || [];
    for (let act = 0; act < actionBundle.length; act++) {
      if (actionBundle[act].resource.type === 'daily') {
        actionBundle[act].resource.value = 0;
      }
    }
    this.update({
      'system.spellInfo.memorization': memBundle,
      'system.actions': actionBundle
    });

    // scan inventory of actor for items with daily resets
    for (const item of this.items) {
      const itemActionBundle = foundry.utils.deepClone(item.system.actions) || [];
      for (let act = 0; act < itemActionBundle.length; act++) {
        if (itemActionBundle[act].resource.type === 'charged'
          && itemActionBundle[act].resource.reusetime === 'daily') {
          itemActionBundle[act].resource.count.value = 0;
        }
      }
      item.update({
        'system.actions': itemActionBundle
      });
    }
    //  
  }

  /**
    * 
    * Prepare token with light effects, or default for npc/pc
    * 
    * I've commented out the sight manipulation
    * 
    */
  async updateTokenLight() {
    if (game.user.isGM) {
      if (!game.osric.config.settings.automateLighting) {
        console.log("actor.js updateTokenLight automateLighting disabled");
        return;
      }

      // console.log("actor.js updateTokenLight", this);
      const token = this.getToken();
      if (token) {
        const light = {
          dim: 0,
          bright: 0,
          angle: 0,
          // alpha: token.light.alpha,
          luminosity: token.light.luminosity,
          // color: 0,
          animation: {
            type: 0,
          }
        };
        // console.log("actor.js updateTokenLight", token);
        for (const effect of this.getActiveEffects()) {
          for (const change of effect.changes) {
            if (change.key === "LIGHT" && change.value.length) {
              try {
                const [match, ...remain] = change.value?.match(/^(#[A-Z\d]+)(\s+(\d+)?\s?(\d+)?\s?(\d+)?\s?(\w+)?)?/i);
                const params = change.value.split(' ');
                // console.log("actor.js updateTokenLight", { match, remain })
                if (params.length) {
                  const color = params[0];
                  const dim = parseInt(params[1]) || 0;
                  const bright = parseInt(params[2]) || dim;
                  const angle = parseInt(params[3]) || 360;
                  const animationType = params[4] || null;
                  const alpha = parseFloat(params[5]) || null;
                  console.log("actor.js updateTokenLight", { color, dim, bright, angle, animationType, alpha })
                  if (light.dim < dim || light.dim === 0 && dim) {
                    light.dim = dim;
                    light.bright = bright;
                    light.angle = angle;
                    light.color = color;
                    light.animation.type = animationType;
                    light.alpha = alpha;
                  }
                }
              } catch (err) {
                ui.notifications.error(`[${err}] LIGHT data field "${change.value}" is formated incorrectly. It must be at least "#htmlcolor lightrange"`);
              }
            }
          }
        }

        // console.log("actor.js updateTokenLight========>", { token, light })
        // token.update({ light })
        // Hooks.call("osricUpdateToken", this, token, { light }, 600);
        // const _timeout1 = setTimeout(async () => await token.update({ light }), 300);
        // if (token?.target)
        await token.update({ light });
      } else {
        // console.log("actor.js updateTokenLight no token?", this)
      }
      // console.log("actor.js updateTokenLight", { lightFound, sightFound });
    }
  }

  /**
   * Update token vision based on effects applied
   */
  async updateTokenVision() {
    if (game.user.isGM) {
      if (!game.osric.config.settings.automateVision) {
        console.log("actor.js updateTokenVision automateVision disabled");
        return;
      }

      // console.log("actor.js updateTokenVision", this);
      const token = this.getToken();

      if (token) {

        const sight = {
          enabled: false,
          range: 0,
          sightAngle: 360,
          visionMode: 'basic',
        };

        // console.log("actor.js updateTokenVision", token);
        for (const effect of this.getActiveEffects()) {
          // console.log("actor.js updateTokenVision ", { effect })
          for (const change of effect.changes) {
            if (change.key === "VISION" && change.value.length) {
              try {
                // const [match, ...remain] = change.value.match(/^(\d+)(\s?(\d+)?\s?(\d+)?)?/i);
                const params = change.value.split(' ');
                if (params.length) {
                  const range = parseInt(params[0])
                  // const brightSight = parseInt(params[1]) || range;
                  const sightAngle = parseInt(params[1]) || 360;
                  const visionMode = params[2] || 'basic';
                  if (sight.range < range) {
                    sight.enabled = true;
                    sight.range = range;
                    sight.sightAngle = sightAngle;
                    sight.visionMode = visionMode;
                  }
                }
              } catch (err) {
                ui.notifications.error(`VISION data field "${change.value}" is formated incorrectly. It must be at least "sightRange#"`);
              }
            }
          }
        }
        if (!sight.enabled) {
          function setSight(actor) {
            sight.enabled = actor.prototypeToken.sight.enabled;
            sight.range = actor.prototypeToken.sight.range;
            sight.sightAngle = actor.prototypeToken.sight.angle;
            sight.visionMode = actor.prototypeToken.sight.visionMode;
          }
          // console.log("actor.js updateTokenVision no sightFound")
          let visionRange = 0;
          if (this.type === 'npc') {
            const visionCheck = `${this.system?.specialDefenses} ${this.system?.specialAttacks}`;
            const [match, ...remain] = visionCheck.match(/vision (\d+)/i) || [];
            visionRange = match ? parseInt(remain[0]) : 0;
            if (visionRange) {
              sight.enabled = true;
              sight.range = visionRange;
            } else {
              setSight(this)
            }
          } else if (this.type === 'character') {
            setSight(this)
          }
        }


        // console.log("actor.js updateTokenVision sight", { sight })
        // if the actor has blind status we dont touch vision settings. Foundry will handle that
        const blind = (this?.effects.find(e => e.getFlag("core", "statusId") === 'blind'));
        // console.log("actor.js updateTokenVision blind", { blind })
        if (!blind) {
          // Hooks.call("osricUpdateToken", this, token, { sight }, 300);
          // const _timeout1 = setTimeout(async () => await token.update({sight}), 300);
          // if (token?.target)
          await token.update({ sight });
        }

      } else {
        // console.log("actor.js updateTokenVision no token?", this)
      }
    }
  }

  /**
   * getter for spellsByLevel array
   */
  get spellsByLevel() {
    const actor = this;
    // console.log("actor.js _prepareSpellsByLevel", actor.name);

    const _insertSpellByLevel = (spellsByLevel, spell, spellType) => {
      const spellLevel = spell.system.level;
      const spellId = spell.id;
      if (!spellsByLevel[spellType][spellLevel]) spellsByLevel[spellType][spellLevel] = {};
      spellsByLevel[spellType][spellLevel][spellId] = spell.name;
      return spellsByLevel;
    };

    const arcaneSpellList = game.collections.get('Item').filter(i =>
      i.type === 'spell' &&
      i.system?.attributes?.type.toLowerCase() !== 'scroll' &&
      i.system.type.toLowerCase() === 'arcane');
    const divineSpellList = game.collections.get('Item').filter(i =>
      i.type === 'spell' &&
      i.system?.attributes?.type.toLowerCase() !== 'scroll' &&
      i.system.type.toLowerCase() === 'divine');

    const packItems = game.osric.library.packs?.items;

    // console.log("actor.js _prepareSpellsByLevel", { packItems });
    const arcanePackList = packItems?.filter(i =>
      i.type === 'spell' &&
      i.system?.attributes?.type.toLowerCase() !== 'scroll' &&
      i.system.type.toLowerCase() === 'arcane');
    const divinePackList = packItems?.filter(i =>
      i.type === 'spell' &&
      i.system?.attributes?.type.toLowerCase() !== 'scroll' &&
      i.system.type.toLowerCase() === 'divine');

    // console.log("actor.js _prepareSpellsByLevel", { arcaneSpellList, divineSpellList, divinePackList, arcanePackList });

    const arcaneSpells = arcanePackList ? arcaneSpellList.concat(arcanePackList) : arcaneSpellList;
    const divineSpells = divinePackList ? divineSpellList.concat(divinePackList) : divineSpellList;

    arcaneSpells.sort(utilitiesManager.sortByRecordName);
    divineSpells.sort(utilitiesManager.sortByRecordName);

    // console.log("actor.js _prepareSpellsByLevel", { arcaneSpells, divineSpells });
    const spellsByLevel = { arcane: {}, divine: {} };

    // arcane and local divine spells
    for (const typeOfSpell of ['arcane', 'divine']) {
      // check inventory for arcane spells for everyone
      actor.system.spells.forEach(spell => {
        const spellType = spell.system.type.toLowerCase();
        //TODO check if the spell is "known" ?
        if (spellType && spellType === typeOfSpell) {
          const spellLevel = spell.system.level;
          const spellLearned = spell.system?.learned;
          const spellId = spell.id;
          if ((actor.system.spellInfo.slots[spellType].value[spellLevel])) {
            if ((spellType === 'arcane' && spellLearned) || (spellType === 'divine'))
              _insertSpellByLevel(spellsByLevel, spell, spellType);
          }
        }
      });
    } // end for

    if (actor.type === 'npc') {
      //they get everything
      arcaneSpells.forEach(spell => {
        _insertSpellByLevel(spellsByLevel, spell, 'arcane');
      });
    }

    // for divine spells, filter by minor/major focus
    let minorFocusFilters = '';
    let majorFocusFilters = '';
    let majorFilter = [];
    let minorFilter = [];
    actor.system.classes.forEach(c => {
      if (c.system.features.focus.minor)
        minorFilter.push(c.system.features.focus.minor)
      if (c.system.features.focus.major)
        majorFilter.push(c.system.features.focus.major)
    });
    if (minorFilter) minorFocusFilters = minorFilter.join("|").replace(/,/g, '|');
    if (majorFilter) majorFocusFilters = majorFilter.join("|").replace(/,/g, '|');

    // console.log("actor.js _prepareSpellsByLevel", { minorFocusFilters, majorFocusFilters });
    if (minorFocusFilters || majorFocusFilters) {

      let minorSpells;
      let majorSpells;
      if (minorFocusFilters) {
        //level 1-3
        minorSpells = divineSpells.filter(i => {
          return (
            (parseInt(i.system.level) || 0) <= 3 &&
            String(i.system.sphere)?.match(new RegExp(`${minorFocusFilters}`, 'ig')));
        });
        if (minorSpells) {
          minorSpells.forEach(spell => {
            _insertSpellByLevel(spellsByLevel, spell, 'divine');
          });
        }
      }
      if (majorFocusFilters) {
        //level 1-max
        majorSpells = divineSpells.filter(i => {
          return (String(i.system.sphere)?.match(new RegExp(`${majorFocusFilters}`, 'ig')));
        });

        majorSpells.forEach(spell => {
          _insertSpellByLevel(spellsByLevel, spell, 'divine');
        });
      }
      // console.log("actor.js _prepareSpellsByLevel", { majorSpells, minorSpells });
    } else {
      //no major/minor, they get everything
      divineSpells.forEach(spell => {
        _insertSpellByLevel(spellsByLevel, spell, 'divine');
      });
    }
    // console.log("actor.js _prepareSpellsByLevel 1", { spellsByLevel });
    return spellsByLevel;
  } // end spellsByLevel


  /**
 * getter effective HD level of the NPC
 * 
 * @returns 
 */
  get effectiveLevel() {
    if (this.type === 'npc') {
      let [hdValue, modValue] = this.getHitDice;
      const bonusHD = ((modValue - (modValue % 4)) / 4);
      if (modValue > 0) {
        hdValue += bonusHD;
      } else {
        hdValue -= bonusHD;
      }
      // console.log("actor.js getEffectiveLevel", { hdValue, modValue })
      return hdValue;
    } else {
      return this.getMaxLevel();
    }
  }
  //getter to return effective HD like 4+4 is 5/etc.
  get effectiveHD() {
    return this.effectiveLevel;
  }
  //getter to return HD, HDMOD
  get getHitDice() {
    if (this.type === 'npc') {
      const hitDice = this.system.hitdice;
      const [match, ...remain] = hitDice.match(/^(\d+)(([+\-])(\d+))?/);
      let hdValue = 0;
      if (remain.length) {
        hdValue = remain[0];
        let modType = remain[2] || '';
        let modValue = remain[3] || 0;
        if (hdValue) hdValue = parseInt(hdValue);
        if (modValue) modValue = parseInt(`${modType}${modValue}`) || 0;
        // console.log("actor.js getHitDice", { hdValue, modValue })
        return ([hdValue, modValue]);
      }
    } else {
      return [this.getMaxLevel(), 0];
    }
    return [0, 0];
  }

  /**
   * Table 48: Hit Dice Vs. Immunity
   * Hit Dice             Hits creatures requiring
   * 4+1 or more          +1 weapon
   * 6+2 or more          +2 weapon
   * 8+3 or more          +3 weapon
   * 10+4 or more         +4 weapon
   * 
   * if system.mods.magicpotency use it if it's higher
   * 
   * return nMod 
   */
  get effectiveMagicPotency() {
    let nMod = 0;
    if (this.type === 'npc') {
      const [hd, hdmod] = this.getHitDice;
      const effectiveHD = this.getEffectiveHD;
      //TODO: Manage the variant type settings here
      if (effectiveHD > 10 || (hd >= 10 && hdmod >= 4)) {
        nMod = 4;
      } else if (effectiveHD > 9 || (hd >= 8 && hdmod >= 3)) {
        nMod = 3;
      } else if (effectiveHD > 7 || (hd >= 6 && hdmod >= 2)) {
        nMod = 2;
      } else if (effectiveHD > 5 || (hd >= 4 && hdmod >= 1)) {
        nMod = 1;
      }
    }

    const modsPotency = parseInt(this.system.mods?.magicpotency) || 0;
    if (!nMod || nMod < modsPotency)
      nMod = modsPotency;

    return nMod;
  }


  /**
   * 
   * Use a item to initiate a attack
   * 
   * @param {*} item 
   */
  async _makeAttackWithItem(event, item) {
    const byPass = event?.ctrlKey;

    let situationalFlavor = item ? await item.getStatBlock() : 'Attacking ...';
    const attackDetails = byPass ? null : await dialogManager.getAttack('Attack', 'Cancel', 'Attack Details', situationalFlavor);
    if (attackDetails || byPass) {
      const dd = OSRICDicer.create(
        {
          event: event,
          sourceActor: this,
          sourceItem: item,
          sourceAction: null,
        },
        {
          isCastSpell: false,
          isWeapon: true,
          isAction: false,
          acLocation: attackDetails ? attackDetails.acLocation : 'normal',
          situational: { mod: attackDetails ? attackDetails.mod : 0, rollMode: attackDetails ? attackDetails.rollMode : undefined },
        });
      DiceManager.rollAttack(dd);
    }

  }


  /**
   * 
   * Get any modifiers applied for name from equipped items
   * and some specials like race
   * 
   * @param {String} name 
   * @returns Integer
   */
  getEquipmentSkillMods(name) {
    let mod = 0;
    // get list of equipped items
    const equippedItems = this.items.filter((item) => {
      return ['equipped'].includes(item.system?.location?.state);
    });
    // get items that are not "equipped" like skills/race/etc
    const specialItems = this.items.filter((item) => {
      return ['race', 'background', 'ability'].includes(item.type);
    });
    const itemList = equippedItems.concat(specialItems);

    itemList.forEach(item => {
      if (item.system.attributes?.skillmods)
        for (const skillmod of Object.values(item.system.attributes.skillmods)) {
          if (name.toLowerCase() === skillmod.name.toLowerCase()) {
            mod += skillmod.value;
          }
        }
    });
    return mod;
  }

  /**
   * 
   * Returns enabled/active effects
   * 
   * @param {*} sourceItem Item this is from? (or not)
   * @returns {Array}
   */
  getActiveEffects(sourceItem = undefined) {
    // let activeEffects = [];
    // console.log("actor.js getActiveEffects", { sourceItem })
    let activeEffects = this.effects.filter(effect => !effect.disabled && !effect.isSuppressed);

    // check for effects that are on the item only used in combat
    if (sourceItem) {
      const inCombatOnly = sourceItem.getItemUseOnlyEffects();
      if (inCombatOnly.length)
        activeEffects = activeEffects.concat(inCombatOnly);
    }

    // console.log("actor.js getActiveEffects", { activeEffects })
    return activeEffects;
  }


  /**
  * 
  * 
  * 
  * @param {*} actor (target)
  * @param {*} thisActor (this)
  * @param {*} sourceItem 
  * @returns 
  */
  getModifiers(actor, thisActor = this, processDirection = undefined, sourceItem = undefined) {
    console.log("actor.js getModifiers", { actor, thisActor, processDirection, sourceItem })
    const modifiers = {
      attacker: {
        attack: {
          melee: [],
          ranged: [],
          thrown: [],
          value: [],
        },
        damage: {
          melee: [],
          ranged: [],
          thrown: [],
          value: [],
        },
        save: {
          all: [],
          "paralyzation": [],
          "poison": [],
          "death": [],
          "rod": [],
          "staff": [],
          "wand": [],
          "petrification": [],
          "polymorph": [],
          "breath": [],
          "spell": [],

        }
      },
      target: {
        attack: {
          melee: [],
          ranged: [],
          thrown: [],
          value: [],
        },
        damage: {
          melee: [],
          ranged: [],
          thrown: [],
          value: [],
        },
        save: {
          all: [],
          "paralyzation": [],
          "poison": [],
          "death": [],
          "rod": [],
          "staff": [],
          "wand": [],
          "petrification": [],
          "polymorph": [],
          "breath": [],
          "spell": [],

        }
      }
    };

    function _applyModifierFormula(direction, resultAction, change) {

      if (resultAction.startsWith('attack')) {
        switch (resultAction) {
          case 'attack':
            modifiers[direction].attack.value.push(change.value);
            break;
          case 'attack.melee':
            modifiers[direction].attack.melee.push(change.value);
            break;
          case 'attack.ranged':
            modifiers[direction].attack.ranged.push(change.value);
            break;
          case 'attack.thrown':
            modifiers[direction].attack.thrown.push(change.value);
            break;
          default:
            ui.notifications.warn(`actor.js getModifiers() unknown resultAction ${resultAction}`);
            console.log("actor.js getModifiers", { resultAction })
            break;
        }

      } else if (resultAction.startsWith('damage')) {
        switch (resultAction) {

          case 'damage':
            modifiers[direction].damage.value.push(change.value);
            break;
          case 'damage.melee':
            modifiers[direction].damage.melee.push(change.value);
            break;
          case 'damage.ranged':
            modifiers[direction].damage.ranged.push(change.value);
            break;
          case 'damage.thrown':
            modifiers[direction].damage.thrown.push(change.value);
            break;

          default:
            ui.notifications.warn(`actor.js getModifiers() unknown resultAction ${resultAction}`);
            console.log("actor.js getModifiers", { resultAction })
            break;
        }

      } else if (resultAction.startsWith('save')) {

        // split save,poison,spell,rod
        const saveTypes = resultAction.toLowerCase().split(',').map(text => text.trim());
        saveTypes.splice(0, 1);

        if (saveTypes.length) {
          for (const sType of saveTypes) {
            modifiers[direction]['save'][sType].push(change.value);
          }
        } else {
          modifiers[direction]['save']['all'].push(change.value);
        }
      } else {
        ui.notifications.warn(`actor.js getModifiers() unknown resultAction ${resultAction}`);
        console.log("actor.js getModifiers", { resultAction })
      }
    }
    function _processEffectTests(change, weapon) {
      //trigger.string:action to apply
      //direction.type.value:resultAction
      //target.alignment.le:attack
      const keys = change.key.toLowerCase().split(':').map(text => text.trim());
      const resultAction = keys[1];
      // target.alignment.le
      const triggers = keys[0].split(".").map(text => text.trim());
      //target, alignment, le
      const [direction, triggerType, triggerValue] = triggers;

      console.log("actor.js getModifiers _processEffectTests", { change, weapon, actor, keys, resultAction, triggers, direction, triggerType, triggerValue })

      switch (triggerType) {

        case 'alignment':
          const alignments = triggerValue.toLowerCase().split(',').map(text => text.trim());
          if (alignments.includes(actor.system.details.alignment)) {
            _applyModifierFormula(direction, resultAction, change)
          }
          break;

        case 'type':
          const types = actor.type === 'character' ?
            [actor.system.details.race.name.toLowerCase().trim()] :
            actor.system.details.type.toLowerCase().split(',').map(text => text.trim());
          const triggerTypes = triggerValue.toLowerCase().split(',').map(text => text.trim());
          for (const typeTrigger of triggerTypes) {
            if (types.includes(typeTrigger)) {
              _applyModifierFormula(direction, resultAction, change)
            }
          }
          break;

        case 'size':
          const sizes = triggerValue.toLowerCase().split(',').map(text => text.trim());
          if (sizes.includes(actor.system.attributes.size)) {
            _applyModifierFormula(direction, resultAction, change)
          }
          break;

        case 'distance':
          console.log("actor.js getModifiers TODO", { thisActor, actor, triggerType, resultAction, triggerValue })
          const distances = triggerValue.split(',').map(text => text.trim());
          const minValue = parseInt(distances[0] || 0);
          const maxValue = parseInt(distances[1] || 1);
          const distance = thisActor.getToken().getDistance(actor.getToken());
          if (distance <= maxValue && distance >= minValue) {
            _applyModifierFormula(direction, resultAction, change)
          }
          break;

        case 'always':
          console.log("actor.js getModifiers always", { triggerType, resultAction });
          _applyModifierFormula(direction, resultAction, change);
          break;

        case 'hitdice':
          console.log("actor.js getModifiers TODO", { triggerType, resultAction })
          break;

        case 'weapontype':
          console.log("actor.js getModifiers TODO", { triggerType, resultAction })
          break;


        default:
          ui.notifications.warn(`actor.js _processEffectTests() unknown triggerType (${triggerType})`)
          console.log("actor.js _processEffectTests() unknown triggerType", { triggerType })
          break;
      }
    }

    // we only want inCombatOnly item effects when we're processing for "target" not "attacker"
    for (const effect of this.getActiveEffects((processDirection === 'target' ? sourceItem : undefined))) {
      for (const change of effect.changes) {
        if (change?.key &&
          (change.key.toLowerCase().startsWith("target.") || change.key.toLowerCase().startsWith("attacker."))) {
          _processEffectTests(change, sourceItem);
        }
      }
    }

    //TODO: check for item conditionals
    const itemConditionals = sourceItem ? sourceItem.conditionals : [];
    // console.log("actor.js getModifiers itemConditionals", { itemConditionals })
    for (const cond of itemConditionals) {
      // console.log("actor.js getModifiers cond", { cond })
      _processEffectTests(cond, sourceItem);
    }


    return modifiers;
  }

  /**
   * Helper to set direction field for getCombatMods()
   * 
   * @param {*} actor Actor targeted for this attack
   * @param {*} type 'attack|damage' combat mods
   * @param {*} itemSource 
   * @returns 
   */
  async getTargetCombatMods(actor, type = 'attack', itemSource) {
    return this.getCombatMods(actor, type, 'target', itemSource)
  }
  /**
   * 
   * @param {*} actor Actor targeted for this attack
   * @param {*} type 'attack|damage' combat mods
   * @param {*} itemSource 
   * @returns 
   */
  async getAttackerCombatMods(actor, type = 'attack', itemSource) {
    return this.getCombatMods(actor, type, 'attacker', itemSource)
  }
  /**
   * 
   * @param {*} actor Actor targeted for this attack
   * @param {*} type 'attack|damage' combat mods
   * @param {*} itemSource 
   * @returns 
   */
  async getCombatMods(actor, type = 'attack', direction = 'target', itemSource) {
    console.log("actor.js getCombatMods", { actor, type, direction, itemSource })
    const atkMods = {
      value: 0,
      melee: 0,
      thrown: 0,
      ranged: 0,
      save: {
        all: 0,
        "paralyzation": 0,
        "poison": 0,
        "death": 0,
        "rod": 0,
        "staff": 0,
        "wand": 0,
        "petrification": 0,
        "polymorph": 0,
        "breath": 0,
        "spell": 0,

      }
    };

    if (!actor) return atkMods;

    const EffectMods = this.getModifiers(actor, this, direction, itemSource);
    // console.log("actor.js getAttackMods", { EffectMods })

    //TODO consider not evaluating the formula here and leaving it to the roll? We might need to do it here
    // to get the rollData from target/attacker when applied to other side?
    async function _updateEffModsHelper(thisActor, modsArray, iType, nested = undefined) {
      // console.log("actor.js _updateEffModsHelper", { thisActor, modsArray, iType, nested })
      if (modsArray && modsArray.length) {
        for (const formula of modsArray) {
          // console.log("actor.js _updateEffModsHelper", { formula })
          const mod = parseInt(await utilitiesManager.evaluateFormulaValue(formula, thisActor.getRollData())) || 0;
          // console.log("actor.js _updateEffModsHelper", { mod })
          if (nested) {
            atkMods[nested][iType] += mod;
          } else {
            atkMods[iType] += mod;
          }
        }
      }
    };

    // console.log("actor.js _updateEffModsHelper", this, { EffectMods, direction, type }, EffectMods[direction][type])
    // for (const direction of ['target', 'attacker']) {
    switch (type) {

      case 'save':
        for (const saveType in EffectMods[direction][type]) {
          // console.log("actor.js _updateEffModsHelper", { saveType })
          if (EffectMods[direction][type][saveType].length)
            await _updateEffModsHelper(this, EffectMods[direction][type][saveType], saveType, 'save');
        }
        break;

      default:
        await _updateEffModsHelper(this, EffectMods[direction][type].value, 'value');
        await _updateEffModsHelper(this, EffectMods[direction][type].melee, 'melee');
        await _updateEffModsHelper(this, EffectMods[direction][type].thrown, 'thrown');
        await _updateEffModsHelper(this, EffectMods[direction][type].ranged, 'ranged');
        break;

    }
    // };

    // console.log("actor.js _updateEffModsHelper END", { atkMods })
    return atkMods;
  }
  /**
   * 
   * This finds modifiers from effects and creates a formula that is returned.
   * Special save modifiers, look for system.mods.saves.{type} with value \d+ (fire,cold,acid property optional)
   * or a formula
   * 
   * @param {String} saveType 
   * @param {*} action 
   * @returns String
   */
  getSaveModifiersFromEffects(saveType, action) {
    // const effectMods = ['@mods.saves.all', `@mods.saves.${saveType}`];
    const effectMods = [];
    if (this.system?.mods?.saves?.all)
      effectMods.push('@mods.saves.all');
    if (this.system?.mods?.saves?.[`${saveType}`])
      effectMods.push(`@mods.saves.${saveType}`);
    const modes = game.osric.const.ACTIVE_EFFECT_MODES;
    // console.log("actor.js getSaveModifiersFromEffects", { saveType, action })
    // for (const effect of this.effects) {
    for (const effect of this.getActiveEffects()) {
      for (const change of effect.changes) {
        // console.log("dice.js getSaveModifiersFromEffects change", { change })
        if (change.mode == modes.CUSTOM &&
          (change.key === `system.mods.saves.${saveType}` || change.key === `system.mods.saves.all`)) {
          if (change.value.match(/^(\d+)( (.*))?/)) {
            const [match, amount, remaining] = change.value.match(/^(\d+)( (.*))?/);
            // console.log("actor.js getSaveModifiersFromEffects keys", { change, match, amount, remaining })
            if (!remaining) { // this is a simple single value, no properties
              effectMods.push(amount);
            } else if (amount && remaining) { // has amount and property(s)
              if (action && action.properties.length) {
                const actionProperties = action.properties.map(text => text.toLowerCase().trim());
                const effectProperties = remaining.toLowerCase().split(",").map(text => text.trim());
                // console.log("actor.js getSaveModifiersFromEffects save effect mods ", { actionProperties, effectProperties })
                // see if the properties of the effect save mod matchines the property of the action such as "fire" or "charm"
                for (const actionProperty of actionProperties) {
                  if (effectProperties.includes(actionProperty.toLowerCase())) {
                    effectMods.push(amount);
                    break;
                  }
                }
              } // end action parts
            }
            // otherwise we assume formula and push it.
          } else if (change.value) {
            effectMods.push(change.value);
          }
        }

      }
    }
    const sEffectMods = effectMods.length ? `${effectMods.join('+')}` : '';
    console.log("actor.js getSaveModifiersFromEffects", { sEffectMods, effectMods })
    return sEffectMods;
  }


  /**
   * Calculate max hp from class(s)
   * 
   * Set class name/level 
   * 
   */
  async _prepareClassData() {
    const rollData = this.getRollData();
    const updates = {};
    const osricVariant = OSRIC.settings.osricVariant;
    const activeClassCount = Object.values(this.system.activeClasses).length;
    if (activeClassCount) { // unless n/pc has a class we dont mess with this
      // let className = "";
      // let levelName = "";
      const hpMaxOrig = this.system.attributes.hp.max;

      // data.system.attributes.hp.max = data.system.attributes.hp.base || 0;
      let maxHP = this.system.attributes.hp.base || 0;
      let currentHP = this.system.attributes.hp.value || 0;


      const conScore = this.system.abilities.con.value;
      // get normal con bonus from table
      const conBonus = game.osric.config.constitutionTable[osricVariant][conScore][0][0];
      // get warrior style con bonus from table
      let conBonusWarrior = game.osric.config.constitutionTable[osricVariant][conScore][0][1];
      if (!conBonusWarrior) conBonusWarrior = conBonus;
      let conBonusTotal = 0;
      for (let classEntry of this.system.classes) {
        // console.log("actor.js _prepareClassData classEntry", { classEntry });
        const maxLevel = Object.keys(classEntry.system.advancement).length;
        // console.log("actor.js _prepareClassData", { classEntry, maxLevel }, classEntry.system.ranks[maxLevel]?.xp);
        classEntry.classDetails = {
          "level": maxLevel,
          "xp": classEntry.xp || 0,
          "neededxp": classEntry.system.ranks[maxLevel - 1]?.xp || 0,
          //store the dex ac modifier formula if anything
          "acDex": await utilitiesManager.evaluateFormulaValue(classEntry.system.features.acDexFormula, rollData),
          //BUG: this doesnt work, needs to be async but then hp calcs break below
          "hpBonusCon": await utilitiesManager.evaluateFormulaValue(classEntry.system.features.hpBonusConFormula, rollData),
          // disable wisdom spell bonus using toggle in class entry
          "wisSpellBonusDisabled": classEntry.system.features.wisSpellBonusDisabled,
        }
        Object.values(classEntry.system.advancement).forEach((entry, index) => {
          // console.log("actor.js _prepareClassData entry", { entry });
          const addHP = activeClassCount > 0 ? Math.ceil(entry.hp / activeClassCount) : entry.hp;
          // const lastHDLevel = classEntry.system.features.lasthitdice;
          // calculate con bonus
          // const finalHp = (addHP + (Math.ceil(hpBonus / activeClassCount)));
          // const finalHp = (addHP + (Math.ceil(hpBonus / activeClassCount)));
          // console.log("actor.js _prepareClassData entry", { hpMaxOrig, maxHP, addHP, conScore, lastHDLevel, conBonus, conBonusWarrior, hpBonus, activeClassCount, finalHp });

          const conBonusMod = classEntry.system.features.bonuscon ? conBonusWarrior : conBonus;
          conBonusTotal += (conBonusMod / activeClassCount);
          maxHP += addHP;
        });
      };
      // add in Constitution HP modifier
      maxHP += (Math.ceil(conBonusTotal));
      // console.log("actor.js _prepareClassData entry", { maxHP, conBonusTotal });

      // if hp.max changed adjust value for that change
      // if (data.system.attributes.hp.max > hpMaxOrig) {
      if (maxHP > hpMaxOrig) {
        const diff = maxHP - hpMaxOrig;
        if (diff != 0) {
          // data.system.attributes.hp.value += diff;
          currentHP += diff;
        }
      }
      // if current hp is greater than max now, adjust to current max.
      if (currentHP > maxHP) {
        currentHP = maxHP;
      }

      const classname = this.system.classes.map(c => {
        return [c.name, Object.values(c.system.advancement).length].filterJoin(" ");
      }).join("/");

      // console.log("actor.js _prepareClassData", { backgroundname, racename, classname });
      updates['system.attributes.hp.max'] = maxHP;
      updates['system.attributes.hp.value'] = currentHP;
      updates['system.classname'] = classname;

      // await this.update({
      //   'system.attributes.hp.max': maxHP,
      //   'system.attributes.hp.value': currentHP,
      //   'system.racename': racename,
      //   'system.backgroundname': backgroundname,
      //   'system.classname': classname,
      // })
      // data.system.classname = `${className} ${levelName}`;
    } else {
      // no classes
      if (this.type === 'character') {
        updates['system.attributes.hp.max'] = 0;
        updates['system.attributes.hp.value'] = 0;
        updates['system.classname'] = 'No Class';
      }
    }

    const backgroundname = this.system.backgrounds.length ? this.system.backgrounds.map(c => c.name).join("/") : null;
    const racename = this.system.races.length ? this.system.races.map(c => c.name).join("/") : null;
    // set "class #level" name

    if (racename)
      updates['system.racename'] = racename;
    if (backgroundname)
      updates['system.backgroundname'] = backgroundname;

    const propOwn = Object.getOwnPropertyNames(updates);
    // console.log("actor.js _prepareClassData", { updates, propOwn }, this);
    // check ._id as it doesnt exist at initial creation
    if (this._id && propOwn.length > 1)
      this.update(updates)
  }

  /**
   * 
   * @param {String} statusName such as "dead" "blind" "deaf" "sleep"
   * @returns Boolean
   * 
   */
  hasStatusEffect(statusName = '') {
    const status = statusName ? (this.getActiveEffects().find(e => e.getFlag("core", "statusId") === statusName)) : false;
    return status;
  }



  /**
   * 
   * //TODO: do we want to have pre-configured mods for specific statusEffects
   * 
   * return statusModifiers for this actor's modifiers 
   * 
   * @returns statusEffect
   */
  statusModifiers() {
    const variantStandard = (game.osric.config.settings.osricVariant == '0');
    const statusEffect = {
      attack: {
        value: 0,
        melee: 0,
        ranged: 0,
        thrown: 0,
        tags: [],
      },
      attacked: {
        value: 0,
        melee: 0,
        ranged: 0,
        thrown: 0,
        tags: [],
      },
      damage: {
        value: 0,
        melee: 0,
        ranged: 0,
        thrown: 0,
        tags: [],
      },
      save: {
        all: 0,
        "paralyzation": 0,
        "poison": 0,
        "death": 0,
        "rod": 0,
        "staff": 0,
        "wand": 0,
        "petrification": 0,
        "polymorph": 0,
        "breath": 0,
        "spell": 0,
        tags: [],
      },
      ac: {
        base: undefined,
        mod: 0,
        tags: [],
      },
      statusList: [],
    };

    if (this.hasStatusEffect('blind')) {
      // TODO: check for blind fighting?
      statusEffect.attack.value += -4;
      // statusEffect.attacked.value += -4;
      statusEffect.ac.mod += 4;
      statusEffect.save.all += -4;
      if (!statusEffect.statusList.includes('blind'))
        statusEffect.statusList.push('blind');
    }
    if (this.hasStatusEffect('stun')) {
      statusEffect.attacked.value += 4;
      if (!statusEffect.statusList.includes('stun'))
        statusEffect.statusList.push('stun');
    }
    if (this.hasStatusEffect('paralysis')) {
      statusEffect.attacked.value += 4;
      if (!statusEffect.statusList.includes('paralysis'))
        statusEffect.statusList.push('paralysis');
    }
    if (this.hasStatusEffect('prone')) {
      statusEffect.attacked.melee += 4;
      statusEffect.attacked.ranged += -2;
      statusEffect.attacked.thrown += -2;
      if (!statusEffect.statusList.includes('prone'))
        statusEffect.statusList.push('prone');
    }
    if (this.hasStatusEffect('restrain')) {
      statusEffect.attacked.value += 2;
      if (!statusEffect.statusList.includes('restrain'))
        statusEffect.statusList.push('restrain');
    }
    //TODO: Need to add this status to the list
    if (this.hasStatusEffect('charge')) {
      statusEffect.attack.value += 2;
      statusEffect.ac.mod += 2;
      //negate dex?!?!?
      statusEffect.ac.tags.push('nodex');
      if (!statusEffect.statusList.includes('charge'))
        statusEffect.statusList.push('charge');
    }
    if (this.hasStatusEffect('invisible')) {
      statusEffect.attack.value += 2;
      statusEffect.attacked.value += -4;
      if (!statusEffect.statusList.includes('invisible'))
        statusEffect.statusList.push('invisible');
    }

    //TODO cover?
    // if (this.hasStatusEffect('cover')) {
    // }
    // if (this.hasStatusEffect('concealment')) {
    // }

    return statusEffect;
  }


  /**
   * 
   * get combatant record
   * 
   * @returns 
   */
  getCombatant() {
    const token = this.getToken();
    if (token) {
      return token.combatant;
    }
    return false;
  }

  /**getter returns initiative */
  get initiative() {
    const combatant = this.getCombatant();
    return combatant ? combatant.initiative : null;
  }

} // end export

