import { OSRICActorSheet } from "./actor-sheet.js";
import { DiceManager } from "../dice/dice.js";
import { onManageActiveEffect, prepareActiveEffectCategories } from "../effect/effects.js";
import * as actionManager from "../apps/action.js";
import * as utilitiesManager from "../utilities.js";
import * as debug from "../debug.js"

export class OSRICCharacterSheet extends OSRICActorSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["osric", "sheet", "actor", "character"],
            template: "systems/osric/templates/actor/character-sheet.hbs",
            // template: "systems/osric/templates/actor/character-sheet.hbs",
            actor: this.actor, // for actor access in character-sheet.hbs
            width: 550,
            height: 800,
            // height: "auto",
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "main" }]
        });
    }

}