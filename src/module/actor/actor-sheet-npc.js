import { OSRIC } from '../config.js';
import { OSRICActorSheet } from "./actor-sheet.js";
import { DiceManager } from "../dice/dice.js";
import { onManageActiveEffect, prepareActiveEffectCategories } from "../effect/effects.js";
import * as utilitiesManager from "../utilities.js";
import * as dialogManager from "../dialog.js";
import * as debug from "../debug.js"
export class OSRICNPCSheet extends OSRICActorSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["osric", "sheet", "actor", "npc"],
            template: "systems/osric/templates/actor/npc-sheet.hbs",
            actor: this.actor, // for actor access in character-sheet.hbs
            width: 550,
            height: 900,
            // height: "auto",
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "main" }]
        });
    }

    /** @override */
    get template() {
        if (!this.actor.isOwner || !game.user.isGM) {
            if (this.canLoot) {
                console.log("actor-sheet.js template using npc-loot-sheet")
                this.position.width = 500;
                this.position.height = 500;
                return `systems/osric/templates/actor/npc-loot-sheet.hbs`;
            } else {
                console.log("actor-sheet.js template using npc-limited-sheet")
                return `systems/osric/templates/actor/npc-limited-sheet.hbs`;
            }
        }

        return `systems/osric/templates/actor/npc-sheet.hbs`;
    }

    /** @override */
    get title() {
        if (this.canLoot) {
            const actorName = this.token?.name ?? this.actor.name;
            if (this.actor.isDead) {
                return `${actorName} [DEAD]`; // `;
            } else {
                return actorName;
            }
        }
        return super.title;
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        if (this.actor.isOwner) {
            html.find(".saves-recalculate").click((event) => this._recalculateSaves(event));
        }
        // clicking share coins button, can't use actual button because of ownership
        html.find(".share-looted-coins").click(async (event) => {
            if (await dialogManager.dialogConfirm('Split coins with party?', 'Share Coins')) {
                utilitiesManager.runAsGM({
                    operation: 'partyShareLootedCoins',
                    user: game.user.id,
                    sourceTokenId: this.actor.token.id,
                });
            } else {
                console.log("actor-sheet-lootable.js activateListeners FALSE CONFIRM");
            }
            // game.party.shareLootedCoins(this.actor);
        });

    }

    /**
     * recalculate npc's saving throws based on current hitdice value
     * 
     * @param {*} event 
     */
    async _recalculateSaves(event) {
        console.log("actor-sheet-npc.js __recalculateSaves", { event, CONFIG }, this)
        const osricVariant = game.osric.config.settings.osricVariant;
        const systemSaveTable = OSRIC.npcSaveTable[osricVariant];
        const hdValue = this.actor.effectiveHD;
        if (await dialogManager.dialogConfirm(`Set saves for effective HD${hdValue}?`, 'Recalculate Saves')) {
            const saveBundle = foundry.utils.deepClone(this.actor.system.saves);
            for (let [key, cSaveType] of Object.entries(saveBundle)) {
                saveBundle[key].value = systemSaveTable[hdValue][OSRIC.saveArrayMap[key]];
            };
            this.actor.update({ 'system.saves': saveBundle });
        } else {
            console.log("actor-sheet-nps.js _recalculateSaves FALSE CONFIRM");
        }

    }

    /** @override to apply/manage locking lootable npc */
    async render(force, options) {
        // console.log("actor-sheet-npc.js render", { force, options }, this)
        // when pc openes the loot window, set opened flag
        utilitiesManager.cleanStaleSheetLocks(this);
        if (game.user.isGM) {
            super.render(force, options);
        } else if (force && !game.user.isGM && !game.paused && !this.object.system.opened) {
            utilitiesManager.runAsGM({
                sourceFunction: 'actor-sheet-npc.js render',
                operation: 'actorUpdate',
                user: game.user.id,
                targetTokenId: this.object.token.id,
                update: { 'system.opened': game.user.id },
            });
            utilitiesManager.chatMessage(ChatMessage.getSpeaker(), 'Looting Body', `${game.user.character?.name ?? game.user.name} is looting ${this.object.token.name}`, this.object.token.img);
            super.render(force, options);
        } else if (!game.paused && game.user.id === this.object.system.opened) {
            super.render(force, options);
        } else if (!game.paused && this.object.system?.opened && game.user.id !== this.object.system.opened) {
            const userLooting = game.users.get(this.object.system.opened);
            ui.notifications.error(`${this.object.token.name} is already being looted by ${userLooting ? userLooting.name : '[Disconnected?]'}.`)
        } else if (game.paused) {
            ui.notifications.error(`${this.object.token.name} cannot be looted while game is paused.`)
        }
    }

} // end OSRICNPCSheet