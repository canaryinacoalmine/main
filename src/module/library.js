import { OSRICItem } from "./item/item.js";
import * as chatManager from "./chat.js";
import * as utilitiesManager from "./utilities.js";
import * as debug from "./debug.js";

// export function refreshWorldSpellsList() {
//     console.log("library.js refreshWorldSpellsList()")
//     const worldSpells = game.items.filter(it => { return it.type === 'spell' });
//     const spellListByLevel = { arcane: [], divine: [] };
//     ['arcane', 'divine'].forEach(spellType => {
//         // console.log("game.osric.library spellType", spellType)
//         worldSpells.forEach(spell => {
//             // console.log("game.osric.library spell", spell)
//             if (spellType && spell.system.type.toLowerCase() === spellType) { // get divine or arcane
//                 if (!spellListByLevel[spellType][spell.system.level]) spellListByLevel[spellType][spell.system.level] = {};
//                 spellListByLevel[spellType][spell.system.level][spell.id] = spell.name;
//             } else if (!spellType) { // no type
//             }
//         });
//     });
//     game.osric.library['spellListByLevel'] = spellListByLevel;
// }

export async function buildPackItemList() {
    console.log("Building pack items list for game.osric.library.packs.items", { game });
    const packItems = await utilitiesManager.getPackItems('Item', game.user.isGM);
    game.osric.library['packs'] = { items: packItems };
    console.log("Pack items list built as game.osric.library.packs.items", { game });
}
/**
 * 
 * This scans compendiums and world spells and places them into game.osric.library.spells.*
 * for use in various spell lists
 * 
 */
export default async function () {

    // const packItems = await utilitiesManager.getPackItems('Item', game.user.isGM);
    // const GMpackItems = await utilitiesManager.getPackItems('Item', true);
    // set some global consts to be used

    game.osric.library = {
        const: {
            location: {
                CARRIED: 'carried',
                EQUIPPED: 'equipped',
                NOCARRIED: 'nocarried',
            }
        }
    }

    buildPackItemList();

    // refreshWorldSpellsList();

    // console.log("game.osric.library spellListByLevel", game.osric.library.spellListByLevel)

    // /**
    //  * This scans compendiums and world spells and places them into game.osric.library.spells.*
    //  * 
    //  *  build list of spells from all packs and world files
    //  *  used for priest spell selection in memorization block
    //  */
    // if (!game.osric.library.isImporting) {

    //     //await game.packs.get("world.spell.compendium").getIndex({fields: ['data']}) // value.data.*
    //     const allItemPacks = game.packs.filter(p => { return p.metadata.type === 'Item' });
    //     // console.log("library.js allItemPacks=>", { allItemPacks })
    //     game.osric.library.allItems = [];
    //     for (const pack of allItemPacks) {
    //         const items = await pack.getIndex({ fields: ['data'] });
    //         console.log("game.osric.library.allItems.push items", items)
    //         // const items = [];
    //         // console.log("library.js items=>", { items })
    //         // insert the pack it's from
    //         for (const item of items) {
    //             // console.log("library.js entry=>", { item })
    //             const libEntry = {
    //                 name: item.name,
    //                 img: item.img,
    //                 type: item.type,
    //                 id: item._id,
    //                 _id: item._id,
    //                 pack: pack,
    //                 data: {
    //                     level: parseInt(item.level || 0),
    //                     type: item.type || '',
    //                 }
    //             }
    //             console.log("game.osric.library.allItems.push libEntry", libEntry)
    //             game.osric.library.allItems.push(libEntry);
    //             console.log("game.osric.library.allItems.push libEntry", game.osric.library.allItems)
    //         }
    //         await pack.getIndex();  // reindex and remove fields from above otherwise it messes up compendium display (images vanish)
    //     }
    //     const compendiumSpells = game.osric.library.allItems.filter(s => { return s.type === 'spell' });
    //     const worldSpells = game.items.filter(it => { return it.type === 'spell' });
    //     const spellListByLevel = { arcane: [], divine: [] };

    //     // create a list of spells
    //     const _spellCombine = (spellType) => {
    //         const _createPackElement = (element) => { return { img: element.img, id: element._id, level: element.data.level, name: element.name, spellType: element.data.type.toLowerCase(), pack: element.pack } };
    //         const _createWorldElement = (element) => { return { img: element.img, id: element.id, level: element.system.level, name: element.name, spellType: element.system.type.toLowerCase() } };
    //         let spellList = [];

    //         compendiumSpells.forEach(element => {
    //             if (spellType && element.data.type && element.data.type.toLowerCase() === spellType) { // get divine or arcane
    //                 if (!spellListByLevel[spellType][element.data.level]) spellListByLevel[spellType][element.data.level] = {};
    //                 spellListByLevel[spellType][element.data.level][element.id] = element.name;
    //                 spellList.push(_createPackElement(element));
    //             } else if (!spellType) { /// grab all spells
    //                 spellList.push(_createPackElement(element));
    //             }
    //         });

    //         worldSpells.forEach(element => {
    //             if (spellType && element.system.type.toLowerCase() === spellType) { // get divine or arcane
    //                 if (!spellListByLevel[spellType][element.data.level]) spellListByLevel[spellType][element.data.level] = {};
    //                 spellListByLevel[spellType][element.data.level][element.id] = element.name;
    //                 spellList.push(_createWorldElement(element));
    //             } else if (!spellType) { /// grab all spells
    //                 spellList.push(_createWorldElement(element));
    //             }
    //         });
    //         spellList.sort(utilitiesManager.sortByRecordName)
    //         return spellList;
    //     }

    //     // create library.spells.arcane/divine/all
    //     const arcaneList = _spellCombine('arcane');
    //     const divineList = _spellCombine('divine');
    //     game.osric.library = {
    //         spells: {
    //             all: _spellCombine(),
    //             arcane: {
    //                 all: arcaneList,
    //             },
    //             divine: {
    //                 all: divineList,
    //             },
    //         },
    //         spellListByLevel: spellListByLevel,
    //     }

    //     // console.log("library.js completed game.osric.library.* entries built.",)
    // }
}

