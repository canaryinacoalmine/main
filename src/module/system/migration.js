/**
 * 
 * Functions to manage version migration/updates
 * 
 */
import * as debug from "../debug.js";

class OSRICChangeLogView extends Application {
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            resizable: true,
            minimizable: true,
            id: "changelog-view-sheet",
            classes: ["osric", "changelog-view"],
            title: "Change Log View",
            template: "systems/osric/templates/apps/changelog-view.hbs",
            width: 700,
            height: 600,
            // scrollY: [".filter-list", ".item-list"],
        });
    }

    async getData() {
        const data = await super.getData();

        data.gameSystem = game.system;

        return data;
    }

}

/**
 * This checks for updates and then displays details. Not really a change log but gives details if changeed.
 */
export function changeLogChecks() {
    const logVersion = game.settings.get("osric", "systemChangeLogVersion");
    const needsReview = !logVersion || logVersion != game.system.version; //isNewerVersion('2023.01.01', logVersion);

    console.log("migration.js changeLogChecks", { logVersion, needsReview }, game.system.version);

    if (needsReview) {
        console.log("migration.js changeLogChecks REVIEW!");
        game.settings.set("osric", "systemChangeLogVersion", game.system.version);
        if (game.user.isGM) {
            if (!game.osric.ui?.changelogView) {
                game.osric.ui = {
                    changelogView: new OSRICChangeLogView()
                }
            }
            game.osric.ui.changelogView.render(true);
        }
    }
}

export function migrationChecks() {
    const currentVersion = game.settings.get("osric", "systemMigrationVersion");
    console.log("migration.js migrationChecks", { currentVersion }, game.system.version);

    if (!currentVersion) {
        return game.settings.set("osric", "systemMigrationVersion", game.system.version);
    }

    const needsMigration = !currentVersion || isNewerVersion('2023.01.01', currentVersion);
    if (!needsMigration) {
        if (game.system.version != currentVersion)
            return game.settings.set("osric", "systemMigrationVersion", game.system.version);
        return;
    }

    console.log("migration.js migrationChecks UPDATING");
    migrateWorld();
}

/**
 * 
 * Migrate all world data
 * 
 */
export async function migrateWorld() {
    ui.notifications.info(`Applying System Migration for version ${game.system.version}. Please be patient and do not close your game or shut down your server.`, { permanent: true });

    // const migrationData = await getMigrationData();
    let migrationData;

    // Migrate World Actors
    for (let a of game.actors) {
        try {
            const updateData = migrateActorData(a.toObject(), migrationData);
            if (!foundry.utils.isObjectEmpty(updateData)) {
                console.log(`Migrating Actor document ${a.name}`);
                await a.update(updateData, { enforceTypes: false });
            }
        } catch (err) {
            err.message = `Failed system migration for Actor ${a.name}: ${err.message}`;
            console.error(err);
        }
    }

    // Migrate World Items
    for (let i of game.items) {
        try {
            const updateData = migrateItemData(i.toObject(), migrationData);
            if (!foundry.utils.isObjectEmpty(updateData)) {
                console.log(`Migrating Item document ${i.name}`);
                await i.update(updateData, { enforceTypes: false });
            }
        } catch (err) {
            err.message = `Failed system migration for Item ${i.name}: ${err.message}`;
            console.error(err);
        }
    }

    // Migrate World Macros
    // for (const m of game.macros) {
    //     try {
    //         const updateData = migrateMacroData(m.toObject(), migrationData);
    //         if (!foundry.utils.isObjectEmpty(updateData)) {
    //             console.log(`Migrating Macro document ${m.name}`);
    //             await m.update(updateData, { enforceTypes: false });
    //         }
    //     } catch (err) {
    //         err.message = `Failed system migration for Macro ${m.name}: ${err.message}`;
    //         console.error(err);
    //     }
    // }

    // Migrate Actor Override Tokens
    // for (let s of game.scenes) {
    //     try {
    //         const updateData = migrateSceneData(s.data, migrationData);
    //         if (!foundry.utils.isObjectEmpty(updateData)) {
    //             console.log(`Migrating Scene document ${s.name}`);
    //             await s.update(updateData, { enforceTypes: false });
    //             // If we do not do this, then synthetic token actors remain in cache
    //             // with the un-updated actorData.
    //             s.tokens.forEach(t => t._actor = null);
    //         }
    //     } catch (err) {
    //         err.message = `Failed system migration for Scene ${s.name}: ${err.message}`;
    //         console.error(err);
    //     }
    // }

    // Migrate World Compendium Packs
    for (let p of game.packs) {
        if (p.metadata.package !== "world") continue;
        if (!["Actor", "Item", "Scene"].includes(p.documentName)) continue;
        await migrateCompendium(p);
    }

    // Set the migration as complete
    console.log("migration.js migrationChecks UPDATING COMPLETE");
    game.settings.set("osric", "systemMigrationVersion", game.system.version);
    ui.notifications.info(`System Migration to version ${game.system.version} completed!`, { permanent: true });
}

/**
 * 
 * Migrate compendium data
 * 
 * @param {*} pack 
 * @returns 
 */
export const migrateCompendium = async function (pack) {
    const documentName = pack.documentName;
    if (!["Actor", "Item", "Scene"].includes(documentName)) return;

    // const migrationData = await getMigrationData();
    let migrationData; // currently not used

    // Unlock the pack for editing
    const wasLocked = pack.locked;
    await pack.configure({ locked: false });

    // Begin by requesting server-side data model migration and get the migrated content
    await pack.migrate();
    const documents = await pack.getDocuments();

    // Iterate over compendium entries - applying fine-tuned migration functions
    for (let doc of documents) {
        let updateData = {};
        try {
            switch (documentName) {
                case "Actor":
                    updateData = migrateActorData(doc.toObject(), migrationData);
                    break;
                case "Item":
                    updateData = migrateItemData(doc.toObject(), migrationData);
                    break;
                // case "Scene":
                //     updateData = migrateSceneData(doc.data, migrationData);
                //     break;
            }

            // Save the entry, if data was changed
            if (foundry.utils.isObjectEmpty(updateData)) continue;
            await doc.update(updateData);
            console.log(`Migrated ${documentName} document ${doc.name} in Compendium ${pack.collection}`);
        }

        // Handle migration failures
        catch (err) {
            err.message = `Failed system migration for document ${doc.name} in pack ${pack.collection}: ${err.message}`;
            console.error(err);
        }
    }

    // Apply the original locked status for the pack
    await pack.configure({ locked: wasLocked });
    console.log(`Migrated all ${documentName} documents from Compendium ${pack.collection}`);
};

/**
 * 
 * Migrate actor data
 * 
 * @param {*} actor 
 * @param {*} migrationData 
 * @returns 
 */
export const migrateActorData = function (actor, migrationData) {
    const updateData = {};

    // Actor Data Updates
    if (actor.data) {

        // _migrateActorMovement(actor, updateData);

    }

    // Migrate Owned Items
    if (!actor.items) return updateData;
    const items = actor.items.reduce((arr, i) => {
        // Migrate the Owned Item
        const itemData = i instanceof CONFIG.Item.documentClass ? i.toObject() : i;
        let itemUpdate = migrateItemData(itemData, migrationData);

        // Update the Owned Item
        if (!isObjectEmpty(itemUpdate)) {
            itemUpdate._id = itemData._id;
            arr.push(expandObject(itemUpdate));
        }
        return arr;
    }, []);
    if (items.length > 0) updateData.items = items;
    return updateData;
};

/**
 * 
 * migrate item data changes
 * 
 * @param {*} item 
 * @param {*} migrationData 
 * @returns 
 */
export const migrateItemData = function (item, migrationData) {
    const updateData = {};

    _migrateCost(item, updateData);

    return updateData;
};
/**
 * 
 * Convert from original "100 gp" to new value/currency
 * 
 * @param {*} item 
 * @param {*} updateData 
 * @returns 
 */
function _migrateCost(item, updateData) {
    console.log("migration.js _migrateCost", { item, updateData })
    if (['item', 'weapon', 'armor', 'potion'].includes(item.type)) {
        const originalCost = item.cost;
        if (originalCost) {
            if (originalCost.match(/\d+ \w+/)) {
                const [itemCost, itemCurrency] = originalCost.split(" ");
                const newCost = { value: parseInt(itemCost) || 0, currency: itemCurrency };
                updateData["cost"] = newCost;
            } else {
                updateData["-=cost"] = null;
            }
        }
    }
    return updateData;
}