import * as utilitiesManager from "../utilities.js";
import * as dialogManager from "../dialog.js";
import * as debug from "../debug.js"



/**
 * Manage Active Effect instances through the Actor Sheet via effect control buttons.
 * @param {MouseEvent} event      The left-click event on the effect control
 * @param {Actor|Item} owner      The owning entity which manages this effect
 */
export async function onManageActiveEffect(event, owner) {
    event.preventDefault();
    const a = event.currentTarget;
    const li = a.closest("li");
    const effect = li.dataset.effectId ? owner.effects.get(li.dataset.effectId) : null;

    console.log("effects.js onManageActiveEffect", { event, owner, a, li, effect });

    switch (a.dataset.action) {
        case "create":
            return await owner.createEmbeddedDocuments("ActiveEffect", [{
                label: "New Effect",
                icon: "icons/svg/aura.svg",
                origin: owner.uuid,
                "duration.rounds": li.dataset.effectType === "temporary" ? 1 : undefined,
                disabled: li.dataset.effectType === "inactive"
            }]);
            break;
        case "edit":
            return effect.sheet.render(true);
            break;
        case "delete":
            if (await dialogManager.dialogConfirm(`Delete <b>${effect.label}</b> effect?`, 'Delete Effect')) {
                return effect.delete();
            }
            break;
        case "toggle":
            return effect.update({ disabled: !effect.disabled });
            break;
    }
}

/**
 * Prepare the data structure for Active Effects which are currently applied to an Actor or Item.
 * @param {ActiveEffect[]} effects    The array of Active Effect instances to prepare sheet data for
 * @return {object}                   Data for rendering
 */
export function prepareActiveEffectCategories(effects) {

    // console.log("effects.js prepareActiveEffectCategories effects:", effects);

    // Define effect header categories
    const categories = {
        temporary: {
            type: "temporary",
            label: "Temporary Effects",
            info: ["Temporary Effects"],
            effects: []
        },
        passive: {
            type: "passive",
            label: "Passive Effects",
            info: ["Passive Effects"],
            effects: []
        },
        inactive: {
            type: "inactive",
            label: "Inactive Effects",
            info: ["Inactive Effects"],
            effects: []
        },
        suppressed: {
            type: "suppressed",
            label: `Suppressed`,
            effects: [],
            info: [`Suppressed`]
        }
    };

    // Iterate over active effects, classifying them into categories
    for (let e of effects) {
        e._getSourceName(); // Trigger a lookup for the source name
        if (e.isSuppressed) categories.suppressed.effects.push(e);
        else if (e.disabled) categories.inactive.effects.push(e);
        else if (e.isTemporary) categories.temporary.effects.push(e);
        else categories.passive.effects.push(e);
    }

    return categories;
}

/**
   * get the changes.key that matches 'keymatch' string
   * 
   * getEffectsByChangesKey("ATTACK", effects);
   * 
   * @param {String} keymatch 
   * @param {List of Effects} effects 
   */
export function getEffectsByChangesKey(keymatch, effects) {
    // console.log("effects.js", "getEffectsByChangesKey", { keymatch, effects });
    return effects.filter((e) => e.changes?.find((ee) => ee.key === keymatch));
}

// /**
//  * 
//  * Returns effects with remaining uses of keymatch
//  * 
//  * @param {String} keymatch  MIRRORIMAGE or STONESKIN 
//  * @param {Array of Object} effects array
//  * @returns array of effects
//  */
// export function getEffectKeyWithUses(keymatch, effects) {
//     const filtered = effects.filter((effect) => {
//         if (!effect.disabled && !effect.isSuppressed) {
//             for (let step = 0; step < effect.changes.length; step++) {
//                 const nCount = parseInt(effect.changes[step].value || 0)
//                 return (nCount && effect.changes[step].key === keymatch);
//             }
//         }
//     });
//     return filtered;
// }

// /**
//  * Returns the total changes.value for list of effects as "1 + 2d6 + 3"
//  * First, collect a list of effects that contain the values you want.
//  * hand off to this function and get a total from all values appened together
//  * 
//  * getEffectChangesValueTotal("ATTACK",effectList)
//  * 
//  * @param {String} keymatch 
//  * @param {List of effects} effectList 
//  */
// export function getEffectChangesValueTotal(keymatch, effectList) {
//     let formula = "";
//     effectList.forEach(element => {
//         element.changes.forEach(chg => {
//             if (keymatch === chg.key) {
//                 if (chg.value.length > 0) {
//                     formula.length > 0 ? (formula += " + " + chg.value) : formula = chg.value
//                 }
//             }
//         });
//     });

//     return formula;
// }


/**
 * 
 * Create ActiveEffect from sourceActor on targetActor using action properties
 * 
 * @param {*} sourceActor  The actor source of the action triggering the effect application
 * @param {*} targetActor The token target of the action effect.
 * @param {*} action The action that contains the effect to apply to targetActor
 * @param {*} chatUser The user that executed the application (used for when GM has to apply the effect)
 * 
 */
export async function applyActionEffect(sourceActor, targetToken, action, chatUser = game.user.id) {
    console.log("effects.js applyActionEffect ", { sourceActor, targetToken, action, chatUser });
    /**
     * 
     * we use this since you can't async in a string.replace() and we need it to get formula results
     * 
     * @param {*} str 
     * @param {*} regex 
     * @param {*} asyncFn 
     * @returns 
     */
    async function replaceAsync(str, formulaActor, regex, regexCleanup, asyncFn) {
        const promises = [];
        str.replace(regex, (match, ...args) => {
            const promise = asyncFn(match, formulaActor, regexCleanup, ...args);
            promises.push(promise);
        });
        const data = await Promise.all(promises);
        return str.replace(regex, () => data.shift());
    }
    const _evaluateFormulaValue = async (match, formulaActor, regexCleanup, p1, offset, string) => {
        console.log("effects.js applyActionEffect ", { match, formulaActor, regexCleanup, p1, offset, string });
        /// evaluate [[1d6]] or [[@valid.formula +3]] in key/value before applying
        // let formula = p1.replace(/[\{}]/g, '');
        // let formula = p1.replace(/[\[\[\]\]]/g, '');
        let formula = p1.replace(regexCleanup, '');
        console.log("effects.js applyActionEffect ", { formula });
        // const rollData = { system: targetToken.actor.getRollData() };
        //be able to formulate source or target actor using {{}} for source and [[]] for target
        // const rollData = useSource? sourceActor.getRollData(): targetToken.actor.getRollData();
        const rollData = formulaActor ? formulaActor.getRollData() : null;
        const formulaResult = await utilitiesManager.evaluateFormulaValue(formula, rollData);
        // console.log("effects.js applyActionEffect ", { formulaResult });
        return formulaResult;
    }
    const _evalKeys = async (changes) => {
        for (let index = 0; index < changes.length; index++) {
            // manage "dice rolls" within the key/value
            // const regEx = new RegExp("(\\[.*\\])", "ig");
            //be able to formulate source or target actor using {{}} for source and [[]] for target
            const regExSource = new RegExp("({{.*}})", "ig");
            const regExSourceCleanup = /[\{\}]/g;

            const regExTarget = new RegExp("(\\[\\[.*\\]\\])", "ig");
            const regExTargetCleanup = /[\[\]]/g;

            // check for source formula
            if (changes[index].key.match(regExSource)) {
                changes[index].key = await replaceAsync(changes[index].key, sourceActor, regExSource, regExSourceCleanup, _evaluateFormulaValue);
            }
            if (changes[index].value.match(regExSource)) {
                changes[index].value = await replaceAsync(changes[index].value, sourceActor, regExSource, regExSourceCleanup, _evaluateFormulaValue);
            }

            // check for target formula 
            if (changes[index].key.match(regExTarget)) {
                changes[index].key = await replaceAsync(changes[index].key, targetToken.actor, regExTarget, regExTargetCleanup, _evaluateFormulaValue);
            }
            if (changes[index].value.match(regExTarget)) {
                changes[index].value = await replaceAsync(changes[index].value, targetToken.actor, regExTarget, regExTargetCleanup, _evaluateFormulaValue);
            }
        }
        return changes;
    };

    // console.log("effects.js applyActionEffect", { sourceActor, targetToken, action });

    //look for a status "effect" entry (will only use last one, not multiple)
    let statusId = '';
    action.effect.changes.forEach(async (change, index) => {
        console.log("effects.js applyActionEffect", { change, index })
        if (change.key.toLowerCase() === 'status' && change.value) {
            statusId = change.value.toLowerCase();
        }
    });

    // console.log("effects.js", "applyActionEffect", { ef });
    // let durationRollResult;
    const formula = action.effect.duration.formula || '';
    // console.log("effects.js applyActionEffect", { formula });
    // if (formula) durationRollResult = await new Roll(String(formula), sourceActor.data).roll({ async: true });
    const durationRollResult = formula ? await utilitiesManager.evaluateFormulaValue(formula, sourceActor.getRollData()) : 0;
    // console.log("effects.js applyActionEffect", { durationRollResult });
    let effectDuration = utilitiesManager.convertTimeToSeconds((formula ? durationRollResult : 0), action.effect.duration.type);

    //TODO: support apply time variables in KEY/VALUE parameters in changes using targetToken.actor.data
    // let newChanges = action.effect.changes; // loop through changes, update key/value looking for evaluation of things like "[1d8]" or [@system.abilities.str.value/2] sorta things.
    const effect = await targetToken.actor.createEmbeddedDocuments("ActiveEffect", [{
        flags: statusId ? { core: { statusId } } : {},
        label: action.name,
        icon: action.img,
        // id: randomID(16),
        origin: `${targetToken.actor.uuid}`,
        "duration.seconds": effectDuration,
        // "duration.rounds": effectDuration,
        // "duration.turns": effectDuration,
        "duration.startTime": game.time.worldTime,
        "duration.startRound": game.combat?.round,
        "duration.startTurn": game.combat?.turn,
        transfer: false,
        "changes": await _evalKeys(foundry.utils.deepClone(action.effect.changes)),
    }], { parent: targetToken.actor });
    // console.log("effects.js effect", { effect });
    const appliedEffect = Object.values(effect)[0];
    console.log("effects.js applyActionEffect", { appliedEffect });

    // if (game.combat) {
    //     effect.duration.startRound = game.combat.round;
    //     effect.duration.startTurn = game.combat.turn;
    //     //TODO get the "combat.id" if they are in combat and add here?
    //     // effect.duration.combat = game.combat.id;
    // }


    // TODO what does .priority do?
    // effect.changes.priority = 

    let cardData = {
        "action": action,
        "targetActor": targetToken.actor,
        "targetToken": targetToken,
        "sourceActor": sourceActor,
        "owner": sourceActor.id,
        "effect": appliedEffect,
        "game": game,
    };

    let chatData = {
        user: chatUser,
        speaker: ChatMessage.getSpeaker()
    };

    chatData.content = await renderTemplate("systems/osric/templates/parts/chat/chatCard-action-effectApplied.hbs", cardData);

    ChatMessage.create(chatData);
    // });
}


/**
 * Apply effect to sourceActor's targets using sourceAction properties
 * 
 * @param {*} sourceActor 
 * @param {*} sourceAction 
 */
export async function applyEffect(sourceActor, sourceAction) {
    // console.log("effects.js applyEffect", { sourceActor, sourceAction });
    const targets = game.user.targets || undefined; // these are "targeted"
    //const aTargets =canvas.tokens.controlled;      // these are "selected"
    // game.actors.tokens[]
    if (targets.size) {
        targets.forEach(target => {
            // console.log("effects.js applyEffect", { target });
            // if (game.user.isGM) {
            //     applyActionEffect(sourceActor, target, sourceAction);
            // } else {
            utilitiesManager.runAsGM({
                operation: 'applyActionEffect',
                sourceFunction: 'applyEffect',
                user: game.user.id,
                targetActorId: target.actor.id,
                targetTokenId: target.id,
                sourceActorId: sourceActor.id,
                sourceTokenId: sourceActor.token ? sourceActor.token.id : null,
                sourceAction: sourceAction
            });
            // }
        });
    } else {
        ui.notifications.error(`Need target to apply effect.`);
    }
}

/**
 * Remove effect on actor by ID
 * 
 * @param {*} actor 
 * @param {*} effectId 
 */
// export async function undoEffect(token, effectId) {
export async function undoEffect(targetToken, effectId) {
    // console.log("effects.js undoEffect", { targetToken, effectId });

    if (!game.user.isGM) return;

    let deleted = false;
    try {
        deleted = await targetToken.deleteEmbeddedDocuments("ActiveEffect", [effectId]);
    } catch { }
    // console.log("effects.js undoEffect", { deleted });
    if (!deleted) {
        ui.notifications.error(`Unable to find effect ${effectId} on ${targetToken.name}.`);
    } else {
        ui.notifications.info(`Removed effect ${deleted[0].label} on ${targetToken.name}.`);
    }
}

/**
 * 
 * Manage expirations for effects on NPCs in combat tracker
 * and PCs in the Party Tracker (ignored otherwise)
 * 
 * Currently duration.seconds is all we watch for duration
 * 
 * @param {*} worldTime 
 */
export async function manageEffectExpirations(worldTime) {

    console.log("effects.js manageEffectExpirations CONFIG.time.roundTime", CONFIG.time.roundTime);

    /**
     * 
     * Checks actor for expired effects, flags them and bulk removes
     * 
     * @param {*} actor 
     */
    async function _expireEffects(actor) {
        if (actor) {
            let reRender = false;
            // console.log("effects.js manageEffectExpirations _expireEffects", { actor });
            const expiredEffects = [];
            const disableEffects = []; //TODO add option to disable only expired effect?
            for (const effect of actor.getActiveEffects()) {
                // console.log("effects.js manageEffectExpirations _expireEffects !effect.disabled && !effect.isSuppressed");
                if (effect.duration?.seconds) {
                    // console.log("effects.js manageEffectExpirations _expireEffects effect.duration?.seconds");
                    if (!effect.duration?.startTime) {
                        // console.log("effects.js manageEffectExpirations _expireEffects !effect.duration?.startTime");
                        // set a startTime to game.time.worldTime?
                        effect.update({
                            'duration.startTime': worldTime
                        })
                    } else {
                        const startTime = effect.duration.startTime;
                        const duration = effect.duration.seconds;
                        const timePassed = (worldTime - startTime);
                        // const roundRemainder = timePassed % CONFIG.time.roundTime;
                        // const timePassedInRounds = ((timePassed - roundRemainder) / CONFIG.time.roundTime);
                        // console.log("effects.js manageEffectExpirations _expireEffects", { timePassed, roundRemainder, timePassedInRounds });
                        // expired?
                        // console.log("effects.js manageEffectExpirations _expireEffects", (worldTime - startTime));
                        if (timePassed >= duration) {
                            // if this is a from a item that the pc owns then we dont delete the effect
                            if (effect.origin.startsWith(`Actor.${actor.id}.Item.`)) {
                                console.log("effects.js manageEffectExpirations _expireEffects disabled", { effect });
                                disableEffects.push(effect.id);
                            } else {
                                console.log("effects.js manageEffectExpirations _expireEffects expired", { effect });
                                expiredEffects.push(effect.id);
                            }
                        }
                    }
                }
            }

            if (disableEffects.length) {
                for (const effectId of disableEffects) {
                    const effect = await actor.getEmbeddedDocument("ActiveEffect", effectId);
                    console.log("effects.js manageEffectExpirations _expireEffects disableEffects", { effect }, effect._source.duration.seconds);
                    effect.update({
                        'duration.startTime': 0,
                        'disabled': true
                    });
                }
                reRender = true;
            }
            if (expiredEffects.length) {
                actor.deleteEmbeddedDocuments("ActiveEffect", expiredEffects, { "expiry-reason": 'expired duration' });
                console.log("effects.js manageEffectExpirations _expireEffects expired ------->", { actor })
                reRender = true;
            }
            if (reRender) actor.sheet.render();
        }
    }

    if (game.combat?.active) {
        // flip through all NPCs in combat tracker
        for (const actor of game.combat.combatants.map(co => co.actor)) {
            // check everything in CT but PCs
            if (actor.type !== 'character') {
                _expireEffects(actor);
            }
        }
    }
    // flip through all PCs in party tracker
    // const partyMembers = game.party.getMembers();
    // for (const member of partyMembers) {
    for (const [key, actor] of game.party.members) {
        // const actor = game.actors.get(member.id);
        _expireEffects(actor);
    }


    console.log("effects.js manageEffectExpirations _expireEffects DONE");
}