import * as debug from "../debug.js"
/**
 * 
 * This was for testing when I was trying to add additional field to changes.* (inCombatOnly) but with how
 * things are currently in Foundry I cannot do that.
 * 
 */

/**
 * A form designed for creating and editing an Active Effect on an Actor or Item entity.
 * @implements {FormApplication}
 *
 * @param {ActiveEffect} object     The target active effect being configured
 * @param {object} [options]        Additional options which modify this application instance
 */
export class OSRICActiveEffectConfig extends ActiveEffectConfig {
    // /** @override */
    // static get defaultOptions() {
    //     return mergeObject(super.defaultOptions, {
    //         classes: ["sheet", "active-effect-sheet"],
    //         title: "EFFECT.ConfigTitle",
    //         template: "templates/effects/active-effect-config.hbs",
    //         width: 560,
    //         height: "auto",
    //         tabs: [{ navSelector: ".tabs", contentSelector: "form", initial: "details" }]
    //     });
    // }

    get template() {
        return "systems/osric/templates/effects/active-effect-config.hbs";
    }

    getData() {
        const sheetData = mergeObject(super.getData(), {
            config: CONFIG.OSRIC
        });

        console.log("active-effect-config.js getData", { sheetData })

        return sheetData;
    }


    /** @override */
    async _addEffectChange() {
        const idx = this.document.data.changes.length;
        console.log("active-effect-config.js _addEffectChange", { idx })
        return this.submit({
            preventClose: true, updateData: {
                [`changes.${idx}`]: { key: "", mode: CONST.ACTIVE_EFFECT_MODES.ADD, value: "", inCombatOnly: false }
            }
        });
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
        html.find(".effect-change-toggleCombatOnly").click(this._toggleCombatOnly.bind(this));
    }

    async _toggleCombatOnly(event) {
        event.preventDefault();
        const element = event.currentTarget;
        const li = element.closest("li");
        console.log("active-effect-config.js _toggleCombatOnly", this, { event }, li.dataset, li.dataset.id, li.dataset.index)
        const changeBundle = foundry.utils.deepClone(this.object.data.changes);
        changeBundle[li.dataset.index].value = "THIS-WORKS-CHANGES";
        // console.log("active-effect-config.js _toggleCombatOnly 1", this.object.data.changes[li.dataset.index].value)
        // this.object.data.changes[li.dataset.index].inCombatOnly = !this.object.data.changes[li.dataset.index].inCombatOnly;
        // this.object.data.changes[li.dataset.index].value = "THIS-WORKS-CHANGES";
        // const currentFlag = this.object.getFlag(`system.changes.${li.dataset.index}`, "osric", "inCombatOnly");
        // await this.object.setFlag(`system.changes.${li.dataset.index}`, "osric", "inCombatOnly", !currentFlag);
        // await this.object.update({ [`system.changes.${li.dataset.index}.value`]: "THIS-CHANGED" });
        // await this.object.update({ [`system.changes.${li.dataset.index}.inCombatOnly`]: !this.object.data.changes[li.dataset.index].inCombatOnly });
        console.log("active-effect-config.js _toggleCombatOnly===========>", this.object.data)
        console.log("active-effect-config.js _toggleCombatOnly", { changeBundle })
        await this.object.update({ 'changes': changeBundle });
        return true;
    }
}