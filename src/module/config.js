export const OSRIC = {};

OSRIC.settings = {};

// OSRIC.icons.general = {
//     combat = {
//         "cast": "systems/osric/icons/general/RangedColor.png",
//         "damage": "systems/osric/icons/general/DamageColor.png",
//         "heal": "systems/osric/icons/general/HealColor.png",
//         "effect": "systems/osric/icons/general/EffectColor.png",
//         "castmelee": "systems/osric/icons/general/MeleeColor.png",
//         "castrange": "systems/osric/icons/general/RangedColor.png",
//         "range": "systems/osric/icons/general/RangedColor.png",
//         "melee": "systems/osric/icons/general/MeleeColor.png",
//         "thrown": "systems/osric/icons/general/RangedColor.png",
//     },
// }

OSRIC.icons = {
    "general": {
        "move": "systems/osric/icons/general/footprint-left.png",
        "items": {
            "item": "icons/commodities/leather/leather-bolt-brown.webp",
            "ability": "icons/skills/social/wave-halt-stop.webp",
            "armor": "icons/equipment/chest/breastplate-banded-steel.webp",
            "background": "icons/environment/settlement/house-farmland-small.webp",
            "bundle": "icons/containers/chest/chest-worn-oak-tan.webp",
            "container": "icons/containers/bags/case-simple-brown.webp",
            "class": "icons/skills/trades/academics-merchant-scribe.webp",
            "encounter": "icons/creatures/eyes/lizard-single-slit-green.webp",
            "potion": "icons/consumables/potions/bottle-round-corked-pink.webp",
            "proficiency": "icons/weapons/swords/sword-guard-bronze.webp",
            "race": "icons/environment/people/group.webp",
            "spell": "icons/sundries/scrolls/scroll-bound-sealed-red.webp",
            "skill": "icons/tools/hand/hammer-and-nail.webp",
            "weapon": "icons/weapons/hammers/hammer-war-rounding.webp",
        },
        "combat": {
            "cast": "icons/weapons/staves/staff-ornate-red.webp",
            "damage": "icons/skills/wounds/blood-drip-droplet-red.webp",
            "heal": "icons/magic/holy/prayer-hands-glowing-yellow-white.webp",
            "effect": "icons/magic/time/hourglass-yellow-green.webp",
            "castmelee": "icons/weapons/swords/sword-winged-pink.webp",
            "castranged": "icons/weapons/wands/wand-gem-purple.webp",
            "ranged": "icons/skills/ranged/target-bullseye-arrow-glowing.webp",
            "melee": "icons/weapons/swords/swords-cutlasses-white.webp",
            "thrown": "icons/weapons/thrown/bomb-fuse-red-black.webp",
            "save": "icons/magic/defensive/illusion-evasion-echo-purple.webp",
            "effects": {
                "defeated": "systems/osric/icons/general/rip.png",
            }
        },
        "currency": {
            "cp": "icons/commodities/currency/coin-engraved-waves-copper.webp",
            "sp": "icons/commodities/currency/coin-embossed-unicorn-silver.webp",
            "gp": "icons/commodities/currency/coin-plain-portal-gold.webp",
            "ep": "icons/commodities/currency/coin-engraved-oval-steel.webp",
            "pp": "icons/commodities/currency/coin-inset-lightning-silver.webp",
        },
        "actors": {
            "lootable": "icons/containers/chest/chest-reinforced-steel-brown.webp"
        }
    }
}

OSRIC.sounds = {
    initiative: {
        'start': 'systems/osric/sounds/init/alert1.wav',
        'turn': 'systems/osric/sounds/init/alert2.wav',
    },
    save: {
        'success': 'systems/osric/sounds/checks/success-fanfare-trumpets.mp3',
        'failure': 'systems/osric/sounds/checks/failure_slide.mp3',
    },
    combat: {
        'melee-hit': 'systems/osric/sounds/combat/sword-hit.wav',
        'melee-miss': 'systems/osric/sounds/combat/sword-miss.mp3',
        'melee-hit-crit': 'systems/osric/sounds/combat/sword-hit-crit.wav',
        'missile-hit': 'systems/osric/sounds/combat/arrow-hit.wav',
        'missile-miss': 'systems/osric/sounds/combat/arrow-miss.WAV',
        'missile-hit-crit': 'systems/osric/sounds/combat/sword-hit-crit.wav',
        'death': 'systems/osric/sounds/combat/death-wilhelm.wav',
    }
};

OSRIC.chargedActions = [
    'cast',
    'castmelee',
    'castranged',
    'ranged',
    'melee',
    'thrown'
]
OSRIC.ammoAttacks = [
    'ranged',
    'thrown'
]

OSRIC.weaponAttackTypes = [
    "melee",
    "ranged",
    "thrown",
]

OSRIC.itemProtectionTypes = [
    'armor',
    'shield',
    'ring',
    'cloak',
    'warding',
    'other'
]

OSRIC.itemGearTypes = [
    "item",
    "container",
]

OSRIC.notLargeCreature = [
    'tiny',
    'small',
    'medium'
];

OSRIC.inventoryTypes = [
    "item",
    "armor",
    "potion",
    "spell",
    "weapon",
    // "container",
];

OSRIC.lootableItemTypes = [
    "spell",
    "item",
    // "container",
];

OSRIC.academicTypes = [
    "proficiency",
    "ability",
    "skill",
]
OSRIC.detailsItemTypes = [
    "race",
    "background",
]
OSRIC.nonInventoryTypes = [
    "class",
    "race",
    "background",
    "proficiency",
    "ability",
    "skill",
]

OSRIC.classSubItemTypes = [
    'ability',
    'skill',
    'proficiency'
]

OSRIC.skillGroupNames = [
    "warrior",
    "priest",
    "rogue",
    "mage",
    "other",
    "none"
]

// OSRIC.attackLocations = [
//     "normal",
//     "rear",
//     "shieldless"
// ]
OSRIC.attackLocations = {
    "normal": "OSRIC.defensestyle.normal",
    "rear": "OSRIC.defensestyle.rear",
    "shieldless": "OSRIC.defensestyle.shieldless",
};

OSRIC.damageStyles = {
    "normal": "OSRIC.damageAdjustments.normal",
    "half": "OSRIC.damageAdjustments.half",
    "max": "OSRIC.damageAdjustments.max",
    "x2": "OSRIC.damageAdjustments.x2",
    "x3": "OSRIC.damageAdjustments.x3",
    "x4": "OSRIC.damageAdjustments.x4",
    "x5": "OSRIC.damageAdjustments.x5",
    "x6": "OSRIC.damageAdjustments.x6",
    "double": "OSRIC.damageAdjustments.double",
};

OSRIC.inventoryTypeMaps = {
    "weapon": "weapons",
    "armor": "armors",
    "potion": "potions",
    "spell": "spells",
    // "item": "gear",
    "item": "inventory",
    "container": "containers",
    "class": "classes",
    "race": "races",
    "proficiency": "proficiencies",
    "background": "backgrounds",
    "ability": "abilityList",
    "skill": "skills",
};

OSRIC.abilities = {
    "str": "OSRIC.abilityTypes.str",
    "int": "OSRIC.abilityTypes.int",
    "wis": "OSRIC.abilityTypes.wis",
    "dex": "OSRIC.abilityTypes.dex",
    "con": "OSRIC.abilityTypes.con",
    "cha": "OSRIC.abilityTypes.cha"
};
OSRIC.abilityTypes = {
    "none": "OSRIC.abilityTypes.none",
    "str": "OSRIC.abilityTypes.str",
    "int": "OSRIC.abilityTypes.int",
    "wis": "OSRIC.abilityTypes.wis",
    "dex": "OSRIC.abilityTypes.dex",
    "con": "OSRIC.abilityTypes.con",
    "cha": "OSRIC.abilityTypes.cha",
};

OSRIC.abilitiesShort = {
    "str": "OSRIC.abilityTypes.strabbr",
    "int": "OSRIC.abilityTypes.intabbr",
    "wis": "OSRIC.abilityTypes.wisabbr",
    "dex": "OSRIC.abilityTypes.dexabbr",
    "con": "OSRIC.abilityTypes.conabbr",
    "cha": "OSRIC.abilityTypes.chaabbr"
};
OSRIC.armorTypes = {
    "armor": "OSRIC.armorTypes.armor",
    "shield": "OSRIC.armorTypes.shield",
    "warding": "OSRIC.armorTypes.warding",
    "ring": "OSRIC.armorTypes.ring",
    "cloak": "OSRIC.armorTypes.cloak",
    "other": "OSRIC.armorTypes.other"
};

OSRIC.saves = {
    "paralyzation": "OSRIC.saveTypes.paralyzation",
    "poison": "OSRIC.saveTypes.poison",
    "death": "OSRIC.saveTypes.death",
    "rod": "OSRIC.saveTypes.rod",
    "staff": "OSRIC.saveTypes.staff",
    "wand": "OSRIC.saveTypes.wand",
    "petrification": "OSRIC.saveTypes.petrification",
    "polymorph": "OSRIC.saveTypes.polymorph",
    "breath": "OSRIC.saveTypes.breath",
    "spell": "OSRIC.saveTypes.spell"
};

OSRIC.targeting = {
    "target": "OSRIC.targeting.target",
    "self": "OSRIC.targeting.self"
};
OSRIC.successAction = {
    "none": "OSRIC.successAction.none",
    "halve": "OSRIC.successAction.halve",
    "remove": "OSRIC.successAction.remove"
};
OSRIC.resources = {
    "none": "OSRIC.resources.none",
    "charged": "OSRIC.resources.charged",
    "item": "OSRIC.resources.item"
};
OSRIC.consumed = {
    "item": "OSRIC.consumed.item",
    "charged": "OSRIC.consumed.charged"
};
OSRIC.reusetime = {
    "none": "OSRIC.reusetime.none",
    "daily": "OSRIC.reusetime.daily",
    "weekly": "OSRIC.reusetime.weekly",
    "monthly": "OSRIC.reusetime.monthly",
};
OSRIC.saveTypes = {
    "paralyzation": "OSRIC.saveTypes.paralyzation",
    "poison": "OSRIC.saveTypes.poison",
    "death": "OSRIC.saveTypes.death",
    "rod": "OSRIC.saveTypes.rod",
    "staff": "OSRIC.saveTypes.staff",
    "wand": "OSRIC.saveTypes.wand",
    "petrification": "OSRIC.saveTypes.petrification",
    "polymorph": "OSRIC.saveTypes.polymorph",
    "spell": "OSRIC.saveTypes.spell",
    "breath": "OSRIC.saveTypes.breath",
    "none": "OSRIC.actions.abilityTypes.none",
};
OSRIC.currency = {
    "pp": "OSRIC.currency.pp",
    "ep": "OSRIC.currency.ep",
    "gp": "OSRIC.currency.gp",
    "sp": "OSRIC.currency.sp",
    "cp": "OSRIC.currency.cp"
};
OSRIC.currencyAbbrv = {
    "pp": "OSRIC.currency.abbr.pp",
    "ep": "OSRIC.currency.abbr.ep",
    "gp": "OSRIC.currency.abbr.gp",
    "sp": "OSRIC.currency.abbr.sp",
    "cp": "OSRIC.currency.abbr.cp"
};
OSRIC.ArmorClass = "OSRIC.ArmorClass";
OSRIC.ArmorClassShort = "OSRIC.ArmorClassShort";
OSRIC.HitPoints = "OSRIC.HitPoints";
OSRIC.BaseHitPoints = "OSRIC.BaseHitPoints";
OSRIC.hpbase = "OSRIC.hpbase";
OSRIC.HitPointsShort = "OSRIC.HitPointsShort";

OSRIC.spellTypes = {
    "arcane": "OSRIC.spellTypes.arcane",
    "divine": "OSRIC.spellTypes.divine"
}

OSRIC.weaponTypes = {
    "melee": "OSRIC.weaponTypes.melee",
    "ranged": "OSRIC.weaponTypes.ranged",
    "thrown": "OSRIC.weaponTypes.thrown"
};
OSRIC.weaponDamageTypes = {
    "slashing": "OSRIC.damageTypes.slashing",
    "piercing": "OSRIC.damageTypes.piercing",
    "bludgeoning": "OSRIC.damageTypes.bludgeoning",
    "acid": "OSRIC.damageTypes.acid",
    "cold": "OSRIC.damageTypes.cold",
    "fire": "OSRIC.damageTypes.fire",
    "force": "OSRIC.damageTypes.force",
    "gas": "OSRIC.damageTypes.gas",
    "lightning": "OSRIC.damageTypes.lightning",
    "necrotic": "OSRIC.damageTypes.necrotic",
    "poison": "OSRIC.damageTypes.poison",
    "radiant": "OSRIC.damageTypes.radiant",
    "none": "OSRIC.damageTypes.none"
};

OSRIC.dmgTypeIcons = {
    "fire": "icons/svg/fire.svg",
    "lightning": "/icons/svg/lightning.svg"
}

OSRIC.resistTypes = {
    "resist": "OSRIC.resistTypes.resist",
    "immune": "OSRIC.resistTypes.immune",
    "vulnerable": "OSRIC.resistTypes.vulnerable",
    "magicpotency": "OSRIC.resistTypes.magicpotency",
    "perdice": "OSRIC.resistTypes.perdice"
};

OSRIC.metalprotections = {
    "halve": "OSRIC.metalprotections.halve",
    "full": "OSRIC.metalprotections.full",
};

OSRIC.alignmentTypes = {
    "n": "OSRIC.alignmentTypes.n",
    "c": "OSRIC.alignmentTypes.c",
    "l": "OSRIC.alignmentTypes.l",
};

OSRIC.alignmentLongNameMap = {
    "n": "neutral",
    "c": "chaotic",
    "l": "lawful",
};

OSRIC.sizeTypes = {
    "tiny": "OSRIC.sizeTypes.tiny",
    "small": "OSRIC.sizeTypes.small",
    "medium": "OSRIC.sizeTypes.medium",
    "large": "OSRIC.sizeTypes.large",
    "huge": "OSRIC.sizeTypes.huge",
    "gargantuan": "OSRIC.sizeTypes.gargantuan",
};
OSRIC.skillTypes = {
    "ascending": "OSRIC.skillTypes.ascending",
    "decending": "OSRIC.skillTypes.decending",
};

OSRIC.actions = {};
OSRIC.actions.abilityTypes = {
    "str": "OSRIC.actions.abilityTypes.str",
    "dex": "OSRIC.actions.abilityTypes.dex",
    "none": "OSRIC.actions.abilityTypes.none"
};
OSRIC.actions.types = {
    "cast": "OSRIC.actions.type.cast",
    "damage": "OSRIC.actions.type.damage",
    "heal": "OSRIC.actions.type.heal",
    "effect": "OSRIC.actions.type.effect",
    "use": "OSRIC.actions.type.use",
    // "castmelee": "OSRIC.actions.type.castmelee",
    // "castranged": "OSRIC.actions.type.castranged",
    "melee": "OSRIC.actions.type.melee",
    "thrown": "OSRIC.actions.type.thrown",
    "ranged": "OSRIC.actions.type.ranged",
};
OSRIC.actions.durationTypes = {
    "round": "OSRIC.actions.duration.type.round",
    "turn": "OSRIC.actions.duration.type.turn",
    "hour": "OSRIC.actions.duration.type.hour",
    "day": "OSRIC.actions.duration.type.day"
};
OSRIC.actions.effect_modes = {
    0: "OSRIC.actions.effect_modes.custom",
    1: "OSRIC.actions.effect_modes.multiply",
    2: "OSRIC.actions.effect_modes.add",
    3: "OSRIC.actions.effect_modes.downgrade",
    4: "OSRIC.actions.effect_modes.upgrade",
    5: "OSRIC.actions.effect_modes.override",
};

OSRIC.locationStates = {
    "carried": "OSRIC.locationStates.carried",
    "nocarried": "OSRIC.locationStates.nocarried",
    "equipped": "OSRIC.locationStates.equipped"
};
OSRIC.locationStates.images = {
    "carried": "icons/svg/chest.svg",
    "nocarried": "icons/svg/cancel.svg",
    "equipped": "icons/svg/combat.svg"
};

OSRIC.locationStates.fasicons = {
    "carried": "box",
    "nocarried": "exclamation-circle",
    "equipped": "tshirt"
};


OSRIC.strengthTable = {}
// Strength[abilityScore]={hit adj, dam adj, weight allow, max press, open doors, bend bars, light enc, moderate enc, heavy enc, severe enc, max enc}
OSRIC.strengthTable["0"] = {
    0: [
        "OSRIC.abilityFields.str.hit",
        "OSRIC.abilityFields.str.dmg",
        "OSRIC.abilityFields.str.allow",
        "OSRIC.abilityFields.str.press",
        "OSRIC.abilityFields.str.open",
        "OSRIC.abilityFields.str.bendbars",
        "OSRIC.abilityFields.str.encumbrance.light",
        "OSRIC.abilityFields.str.encumbrance.moderate",
        "OSRIC.abilityFields.str.encumbrance.heavy",
        "OSRIC.abilityFields.str.encumbrance.severe",
        "OSRIC.abilityFields.str.encumbrance.max"
    ],
    1: [-3, -3, -30, 110, "1(0)", 0, 5, 40, 75, 110],
    2: [-3, -3, -30, 110, "1(0)", 0, 5, 40, 75, 110],
    3: [-3, -3, -30, 110, "1(0)", 0, 5, 40, 75, 110],
    4: [-2, -2, -20, 120, "1(0)", 0, 15, 50, 85, 120],
    5: [-2, -2, -20, 120, "1(0)", 0, 15, 50, 85, 120],
    6: [-1, -1, -10, 130, "1(0)", 0, 25, 60, 95, 130],
    7: [-1, -1, -10, 130, "1(0)", 0, 25, 60, 95, 130],
    8: [-1, -1, -10, 130, "1(0)", 0, 25, 60, 95, 130],
    9: [0, 0, 0, 140, "1-2(0)", 0, 35, 70, 105, 140],
    10: [0, 0, 0, 140, "1-2(0)", 0, 35, 70, 105, 140],
    11: [0, 0, 0, 140, "1-2(0)", 0, 35, 70, 105, 140],
    12: [0, 0, 0, 140, "1-2(0)", 0, 35, 70, 105, 140],
    13: [1, 1, 10, 150, "1-3(0)", 0, 45, 80, 115, 150],
    14: [1, 1, 10, 150, "1-3(0)", 0, 45, 80, 115, 150],
    15: [1, 1, 10, 150, "1-3(0)", 0, 45, 80, 115, 150],
    16: [2, 2, 20, 160, "1-4(0)", 0, 55, 90, 125, 160],
    17: [2, 2, 20, 160, "1-4(0)", 0, 55, 90, 125, 160],
    18: [3, 3, 30, 170, "1-4(0)", 0, 65, 100, 135, 170],
    19: [3, 6, 300, 0, "1-5(1)", 40, 336, 371, 406, 450],
    20: [3, 8, 535, 700, "17(10)", 60, 536, 580, 610, 670, 700],
    21: [4, 9, 635, 810, "17(12)", 70, 636, 680, 720, 790, 810],
    22: [4, 10, 785, 970, "18(14)", 80, 786, 830, 870, 900, 970],
    23: [5, 11, 935, 1130, "18(16)", 90, 936, 960, 1000, 1090, 1130],
    24: [6, 12, 1235, 1440, "19(17)", 95, 1236, 1290, 1300, 1380, 1440],
    25: [7, 14, 1535, 1750, "19(18)", 99, 1536, 1590, 1600, 1680, 1750],
};
// Strength[abilityScore]={hit adj, dam adj, weight allow, max press, open doors, bend bars, light enc, moderate enc, heavy enc, severe enc, max enc}
OSRIC.strengthTable["1"] = {
    0: [
        "OSRIC.abilityFields.str.hit",
        "OSRIC.abilityFields.str.dmg",
        "OSRIC.abilityFields.str.allow",
        "OSRIC.abilityFields.str.press",
        "OSRIC.abilityFields.str.open",
        "OSRIC.abilityFields.str.bendbars",
        "OSRIC.abilityFields.str.encumbrance.light",
        "OSRIC.abilityFields.str.encumbrance.moderate",
        "OSRIC.abilityFields.str.encumbrance.heavy",
        "OSRIC.abilityFields.str.encumbrance.severe",
        "OSRIC.abilityFields.str.encumbrance.max"],
    1: [-3, -1, -35, 0, "1(0)", 0, 0, 0, 0, 0, 0],
    2: [-3, -1, -35, 0, "1(0)", 0, 0, 0, 0, 0, 0],
    3: [-3, -1, -35, 0, "1(0)", 0, 0, 0, 0, 0, 0],
    4: [-2, -1, -25, 0, "1(0)", 0, 0, 0, 0, 0, 0],
    5: [-2, -1, -25, 0, "1(0)", 0, 0, 0, 0, 0, 0],
    6: [-1, 0, -15, 0, "1(0)", 0, 0, 0, 0, 0, 0],
    7: [-1, 0, -15, 0, "1(0)", 0, 0, 0, 0, 0, 0],
    8: [0, 0, 0, 0, "1-2(0)", 0, 0, 0, 0, 0, 0],
    9: [0, 0, 0, 0, "1-2(0)", 0, 0, 0, 0, 0, 0],
    10: [0, 0, 0, 0, "1-2(0)", 0, 0, 0, 0, 0, 0],
    11: [0, 0, 0, 0, "1-2(0)", 0, 0, 0, 0, 0, 0],
    12: [0, 0, 10, 0, "1-2(0)", 0, 0, 0, 0, 0, 0],
    13: [0, 0, 10, 0, "1-2(0)", 0, 0, 0, 0, 0, 0],
    14: [0, 0, 20, 0, "1-2(0)", 0, 0, 0, 0, 0, 0],
    15: [0, 0, 20, 0, "1-2(0)", 0, 0, 0, 0, 0, 0],
    16: [0, 1, 35, 0, "1-3(0)", 0, 0, 0, 0, 0, 0],
    17: [1, 1, 50, 0, "1-3(0)", 0, 0, 0, 0, 0, 0],
    18: [1, 2, 75, 0, "1-3(0)", 0, 0, 0, 0, 0, 0],
    19: [3, 6, 300, 0, "1-5(1)", 0, 0, 0, 0, 0, 0],
    20: [3, 8, 535, 700, "17(10)", 0, 0, 0, 0, 0, 0],
    21: [4, 9, 635, 810, "17(12)", 0, 0, 0, 0, 0, 0],
    22: [4, 10, 785, 970, "18(14)", 0, 0, 0, 0, 0, 0],
    23: [5, 11, 935, 1130, "18(16)", 0, 0, 0, 0, 0, 0],
    24: [6, 12, 1235, 1440, "19(17)", 0, 0, 0, 0, 0, 0],
    25: [7, 14, 1535, 1750, "19(18)", 0, 0, 0, 0, 0, 0],
    // Deal with 18 01-100 strength
    50: [1, 3, 100, 0, "1-3(0)", 0, 0, 0, 0, 0, 0],
    75: [2, 3, 125, 0, "1-3(0)", 0, 0, 0, 0, 0, 0],
    90: [2, 4, 150, 0, "1-4(0)", 0, 0, 0, 0, 0, 0],
    99: [2, 5, 200, 0, "1-4(1)", 0, 0, 0, 0, 0, 0],
    100: [3, 6, 300, 0, "1-5(2)", 0, 0, 0, 0, 0, 0],
};

// Strength[abilityScore]={hit adj, dam adj, weight allow, max press, open doors, bend bars, light enc, moderate enc, heavy enc, severe enc, max enc}
OSRIC.strengthTable["2"] = {
    0: ["OSRIC.abilityFields.str.hit", "OSRIC.abilityFields.str.dmg", "OSRIC.abilityFields.str.allow", "OSRIC.abilityFields.str.press", "OSRIC.abilityFields.str.open", "OSRIC.abilityFields.str.bendbars", "OSRIC.abilityFields.str.encumbrance.light", "OSRIC.abilityFields.str.encumbrance.moderate", "OSRIC.abilityFields.str.encumbrance.heavy", "OSRIC.abilityFields.str.encumbrance.severe", "OSRIC.abilityFields.str.encumbrance.max"],
    1: [-5, -4, 1, 3, "1(0)", 0, 2, 3, 4, 5, 7],
    2: [-3, -2, 1, 5, "1(0)", 0, 2, 3, 4, 5, 7],
    3: [-3, -1, 5, 10, "2(0)", 0, 2, 3, 4, 5, 7],
    4: [-2, -1, 10, 25, "3(0)", 0, 11, 14, 17, 20, 25],
    5: [-2, -1, 10, 25, "3(0)", 0, 11, 14, 17, 20, 25],
    6: [-1, 0, 20, 55, "4(0)", 0, 21, 30, 39, 47, 55],
    7: [-1, 0, 20, 55, "4(0)", 0, 21, 30, 39, 47, 55],
    8: [0, 0, 35, 90, "5(0)", 1, 36, 51, 66, 81, 90],
    9: [0, 0, 35, 90, "5(0)", 1, 36, 51, 66, 81, 90],
    10: [0, 0, 40, 115, "6(0)", 2, 41, 59, 77, 97, 110],
    11: [0, 0, 40, 115, "6(0)", 2, 41, 59, 77, 97, 110],
    12: [0, 0, 45, 140, "7(0)", 4, 46, 70, 94, 118, 140],
    13: [0, 0, 45, 140, "7(0)", 4, 46, 70, 94, 118, 140],
    14: [0, 0, 55, 170, "8(0)", 7, 56, 86, 116, 146, 170],
    15: [0, 0, 55, 170, "8(0)", 7, 56, 86, 116, 146, 170],
    16: [0, 1, 70, 195, "9(0)", 10, 71, 101, 131, 161, 195],
    17: [1, 1, 85, 220, "10(0)", 13, 86, 122, 158, 194, 220],
    18: [1, 2, 110, 255, "11(0)", 16, 111, 150, 189, 228, 255],
    19: [3, 7, 485, 640, "16(8)", 50, 486, 500, 550, 600, 640],
    20: [3, 8, 535, 700, "17(10)", 60, 536, 580, 610, 670, 700],
    21: [4, 9, 635, 810, "17(12)", 70, 636, 680, 720, 790, 810],
    22: [4, 10, 785, 970, "18(14)", 80, 786, 830, 870, 900, 970],
    23: [5, 11, 935, 1130, "18(16)", 90, 936, 960, 1000, 1090, 1130],
    24: [6, 12, 1235, 1440, "19(17)", 95, 1236, 1290, 1300, 1380, 1440],
    25: [7, 14, 1535, 1750, "19(18)", 99, 1536, 1590, 1600, 1680, 1750],
    // Deal with 18 01-100 strength
    50: [1, 3, 135, 280, "12(0)", 20, 136, 175, 214, 253, 280],
    75: [2, 3, 160, 305, "13(0)", 25, 161, 200, 239, 278, 305],
    90: [2, 4, 185, 330, "14(0)", 30, 186, 225, 264, 303, 330],
    99: [2, 5, 235, 380, "15(3)", 35, 236, 275, 314, 353, 380],
    100: [3, 6, 335, 480, "16(6)", 40, 336, 375, 414, 453, 480],
};

OSRIC.dexterityTable = {}
// Dexterity[abilityScore]={reaction, missile, defensive}
OSRIC.dexterityTable["0"] = {
    0: [
        "OSRIC.abilityFields.dex.reaction",
        "OSRIC.abilityFields.dex.missile",
        "OSRIC.abilityFields.dex.defensive"
    ],
    1: [0, -4, 5],
    2: [0, -4, 4],
    3: [0, -3, 3],
    4: [0, -2, 2],
    5: [0, -2, 2],
    6: [0, -1, 1],
    7: [0, -1, 1],
    8: [0, -1, 1],
    9: [0, 0, 0],
    10: [0, 0, 0],
    11: [0, 0, 0],
    12: [0, 0, 0],
    13: [0, 1, -1],
    14: [0, 1, -1],
    15: [0, 1, -1],
    16: [0, 1, -2],
    17: [0, 2, -2],
    18: [0, 3, -3],
    19: [0, 3, -4],
    20: [0, 3, -4],
    21: [3, 3, -4],
    22: [3, 3, -4],
    23: [3, 3, -4],
    24: [3, 3, -4],
    25: [3, 3, -4]
};
OSRIC.dexterityTable["1"] = {
    0: ["OSRIC.abilityFields.dex.reaction", "OSRIC.abilityFields.dex.missile", "OSRIC.abilityFields.dex.defensive"],
    1: [-6, -6, 5],
    2: [-4, -4, 5],
    3: [-3, -3, 4],
    4: [-2, -2, 3],
    5: [-1, -1, 2],
    6: [0, 0, 1],
    7: [0, 0, 0],
    8: [0, 0, 0],
    9: [0, 0, 0],
    10: [0, 0, 0],
    11: [0, 0, 0],
    12: [0, 0, 0],
    13: [0, 0, 0],
    14: [0, 0, 0],
    15: [0, 0, -1],
    16: [1, 1, -2],
    17: [2, 2, -3],
    18: [3, 3, -4],
    19: [3, 3, -4],
    20: [3, 3, -4],
    21: [4, 4, -5],
    22: [4, 4, -5],
    23: [4, 4, -5],
    24: [5, 5, -6],
    25: [5, 5, -6]
};
OSRIC.dexterityTable["2"] = {
    0: ["OSRIC.abilityFields.dex.reaction", "OSRIC.abilityFields.dex.missile", "OSRIC.abilityFields.dex.defensive"],
    1: [-6, -6, 5],
    2: [-4, -4, 5],
    3: [-3, -3, 4],
    4: [-2, -2, 3],
    5: [-1, -1, 2],
    6: [0, 0, 1],
    7: [0, 0, 0],
    8: [0, 0, 0],
    9: [0, 0, 0],
    10: [0, 0, 0],
    11: [0, 0, 0],
    12: [0, 0, 0],
    13: [0, 0, 0],
    14: [0, 0, 0],
    15: [0, 0, -1],
    16: [1, 1, -2],
    17: [2, 2, -3],
    18: [2, 2, -4],
    19: [3, 3, -4],
    20: [3, 3, -4],
    21: [4, 4, -5],
    22: [4, 4, -5],
    23: [4, 4, -5],
    24: [5, 5, -6],
    25: [5, 5, -6]
};


OSRIC.wisdomBonusSlots = {}
// ability score (1-25) : 0,1,2,3,4,5,6,7 (level/slots)
OSRIC.wisdomBonusSlots["0"] = {
    0: [],
    1: [0, 0, 0, 0, 0, 0, 0, 0],
    2: [0, 0, 0, 0, 0, 0, 0, 0],
    3: [0, 0, 0, 0, 0, 0, 0, 0],
    4: [0, 0, 0, 0, 0, 0, 0, 0],
    5: [0, 0, 0, 0, 0, 0, 0, 0],
    6: [0, 0, 0, 0, 0, 0, 0, 0],
    7: [0, 0, 0, 0, 0, 0, 0, 0],
    8: [0, 0, 0, 0, 0, 0, 0, 0],
    9: [0, 0, 0, 0, 0, 0, 0, 0],
    10: [0, 0, 0, 0, 0, 0, 0, 0],
    11: [0, 0, 0, 0, 0, 0, 0, 0],
    12: [0, 0, 0, 0, 0, 0, 0, 0],
    13: [0, 0, 0, 0, 0, 0, 0, 0],
    14: [0, 0, 0, 0, 0, 0, 0, 0],
    15: [0, 0, 0, 0, 0, 0, 0, 0],
    16: [0, 0, 0, 0, 0, 0, 0, 0],
    17: [0, 0, 0, 0, 0, 0, 0, 0],
    18: [0, 0, 0, 0, 0, 0, 0, 0],
    19: [0, 0, 0, 0, 0, 0, 0, 0],
    20: [0, 0, 0, 0, 0, 0, 0, 0],
    21: [0, 0, 0, 0, 0, 0, 0, 0],
    22: [0, 0, 0, 0, 0, 0, 0, 0],
    23: [0, 0, 0, 0, 0, 0, 0, 0],
    24: [0, 0, 0, 0, 0, 0, 0, 0],
    25: [0, 0, 0, 0, 0, 0, 0, 0],
};
// ability score (1-25) : 0,1,2,3,4,5,6,7 (level/slots)
OSRIC.wisdomBonusSlots["1"] = {
    0: [],
    1: [0, 0, 0, 0, 0, 0, 0, 0],
    2: [0, 0, 0, 0, 0, 0, 0, 0],
    3: [0, 0, 0, 0, 0, 0, 0, 0],
    4: [0, 0, 0, 0, 0, 0, 0, 0],
    5: [0, 0, 0, 0, 0, 0, 0, 0],
    6: [0, 0, 0, 0, 0, 0, 0, 0],
    7: [0, 0, 0, 0, 0, 0, 0, 0],
    8: [0, 0, 0, 0, 0, 0, 0, 0],
    9: [0, 0, 0, 0, 0, 0, 0, 0],
    10: [0, 0, 0, 0, 0, 0, 0, 0],
    11: [0, 0, 0, 0, 0, 0, 0, 0],
    12: [0, 0, 0, 0, 0, 0, 0, 0],
    13: [0, 1, 0, 0, 0, 0, 0, 0],
    14: [0, 2, 0, 0, 0, 0, 0, 0],
    15: [0, 2, 1, 0, 0, 0, 0, 0],
    16: [0, 2, 2, 0, 0, 0, 0, 0],
    17: [0, 2, 2, 1, 0, 0, 0, 0],
    18: [0, 2, 2, 1, 1, 0, 0, 0],
    19: [0, 3, 2, 2, 1, 0, 0, 0],
    20: [0, 3, 3, 2, 2, 0, 0, 0],
    21: [0, 3, 3, 3, 2, 1, 0, 0],
    22: [0, 3, 3, 3, 3, 2, 0, 0],
    23: [0, 4, 3, 3, 3, 2, 1, 0],
    24: [0, 4, 3, 3, 3, 3, 2, 0],
    25: [0, 4, 3, 3, 3, 3, 3, 1],
};
// ability score (1-25) : 0,1,2,3,4,5,6,7 (level/slots)
OSRIC.wisdomBonusSlots["2"] = {
    0: [],
    1: [0, 0, 0, 0, 0, 0, 0, 0],
    2: [0, 0, 0, 0, 0, 0, 0, 0],
    3: [0, 0, 0, 0, 0, 0, 0, 0],
    4: [0, 0, 0, 0, 0, 0, 0, 0],
    5: [0, 0, 0, 0, 0, 0, 0, 0],
    6: [0, 0, 0, 0, 0, 0, 0, 0],
    7: [0, 0, 0, 0, 0, 0, 0, 0],
    8: [0, 0, 0, 0, 0, 0, 0, 0],
    9: [0, 0, 0, 0, 0, 0, 0, 0],
    10: [0, 0, 0, 0, 0, 0, 0, 0],
    11: [0, 0, 0, 0, 0, 0, 0, 0],
    12: [0, 0, 0, 0, 0, 0, 0, 0],
    13: [0, 1, 0, 0, 0, 0, 0, 0],
    14: [0, 2, 0, 0, 0, 0, 0, 0],
    15: [0, 2, 1, 0, 0, 0, 0, 0],
    16: [0, 2, 2, 0, 0, 0, 0, 0],
    17: [0, 2, 2, 1, 0, 0, 0, 0],
    18: [0, 2, 2, 1, 1, 0, 0, 0],
    19: [0, 3, 2, 2, 1, 0, 0, 0],
    20: [0, 3, 3, 2, 2, 0, 0, 0],
    21: [0, 3, 3, 3, 2, 1, 0, 0],
    22: [0, 3, 3, 3, 3, 2, 0, 0],
    23: [0, 4, 3, 3, 3, 2, 1, 0],
    24: [0, 4, 3, 3, 3, 3, 2, 0],
    25: [0, 4, 3, 3, 3, 3, 3, 1],
};
OSRIC.wisdomTable = {}
// Wisdom[abilityScore]={magic adj, spell bonuses, spell failure, spell imm., MAC base, PSP bonus }
OSRIC.wisdomTable["0"] = {
    0: [
        "OSRIC.abilityFields.wis.magic",
        "OSRIC.abilityFields.wis.bonus",
        "OSRIC.abilityFields.wis.failure",
        "OSRIC.abilityFields.wis.imm"
    ],
    1: [-3, "None", 0, 0, 0, 0],
    2: [-3, "None", 0, 0, 0, 0],
    3: [-3, "None", 0, 0, 0, 0],
    4: [-2, "None", 0, 0, 0, 0],
    5: [-2, "None", 0, 0, 0, 0],
    6: [-1, "None", 0, 0, 0, 0],
    7: [-1, "None", 0, 0, 0, 0],
    8: [-1, "None", 0, 0, 0, 0],
    9: [0, "None", 0, 0, 0, 0],
    10: [0, "None", 0, 0, 0, 0],
    11: [0, "None", 0, 0, 0, 0],
    12: [0, "None", 0, 0, 0, 0],
    13: [1, "None", 0, 0, 0, 0],
    14: [1, "None", 0, 0, 0, 0],
    15: [1, "None", 0, 0, 0, 0],
    16: [2, "None", 0, 0, 0, 0],
    17: [2, "None", 0, 0, 0, 0],
    18: [3, "None", 0, 0, 0, 0],
    19: [4, "None", 0, 0, 0, 0],
    20: [4, "None", 0, 0, 0, 0],
    21: [5, "None", 0, 0, 0, 0],
    22: [5, "None", 0, 0, 0, 0],
    23: [5, "None", 0, 0, 0, 0],
    24: [5, "None", 0, 0, 0, 0],
    25: [5, "None", 0, 0, 0, 0],
    //-- deal with long string bonus for tooltip
    117: [3, "Bonus Spells: 2x1st, 2x2nd, 1x3rd", 0, 0],
    118: [4, "Bonus Spells: 2x1st, 2x2nd, 1x3rd, 1x4th", 0, 0],
    119: [5, "Bonus Spells: 3x1st, 2x2nd, 1x3rd, 1x4th", 0, 0],
    120: [5, "Bonus Spells: 3x1st, 2x2nd, 1x3rd, 1x4th", 0, 0],
    121: [5, "Bonus Spells: 3x1st, 2x2nd, 1x3rd, 1x4th", 0, 0],
    122: [5, "Bonus Spells: 3x1st, 2x2nd, 1x3rd, 1x4th", 0, 0],
    123: [5, "Bonus Spells: 3x1st, 2x2nd, 1x3rd, 1x4th", 0, 0],
    124: [5, "Bonus Spells: 3x1st, 2x2nd, 1x3rd, 1x4th", 0, 0],
    125: [5, "Bonus Spells: 3x1st, 2x2nd, 1x3rd, 1x4th", 0, 0]
};
OSRIC.wisdomTable["1"] = {
    0: ["OSRIC.abilityFields.wis.magic", "OSRIC.abilityFields.wis.bonus", "OSRIC.abilityFields.wis.failure", "OSRIC.abilityFields.wis.imm"],
    1: [-3, "None", 50, "None", 0, 0],
    2: [-3, "None", 50, "None", 0, 0],
    3: [-3, "None", 50, "None", 0, 0],
    4: [-2, "None", 45, "None", 0, 0],
    5: [-1, "None", 40, "None", 0, 0],
    6: [-1, "None", 35, "None", 0, 0],
    7: [-1, "None", 30, "None", 0, 0],
    8: [0, "None", 25, "None", 0, 0],
    9: [0, "None", 20, "None", 0, 0],
    10: [0, "None", 15, "None", 0, 0],
    11: [0, "None", 10, "None", 0, 0],
    12: [0, "None", 5, "None", 0, 0],
    13: [0, "1x1st", 0, "None", 0, 0],
    14: [0, "2x1st", 0, "None", 0, 0],
    15: [1, "2x1st,1x2nd", 0, "None", 0, 0],
    16: [2, "2x1st,2x2nd", 0, "None", 0, 0],
    17: [3, "Various", 0, "None", 0, 0],
    18: [4, "Various", 0, "None", 0, 0],
    19: [4, "Various", 0, "Various", 0, 0],
    20: [4, "Various", 0, "Various", 0, 0],
    21: [4, "Various", 0, "Various", 0, 0],
    22: [4, "Various", 0, "Various", 0, 0],
    23: [4, "Various", 0, "Various", 0, 0],
    24: [4, "Various", 0, "Various", 0, 0],
    25: [4, "Various", 0, "Various", 0, 0],
    //-- deal with long string bonus for tooltip
    117: [3, "Bonus Spells: 2x1st, 2x2nd, 1x3rd", 0, "None"],
    118: [4, "Bonus Spells: 2x1st, 2x2nd, 1x3rd, 1x4th", 0, "None"],
    119: [4, "Bonus Spells: 3x1st, 2x2nd, 2x3rd, 1x4th", 0, "Spells: cause fear,charm person, command, friends, hypnotism"],
    120: [4, "Bonus Spells: 3x1st, 3x2nd, 2x3rd, 2x4th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare"],
    121: [4, "Bonus Spells: 3x1st, 3x2nd, 3x3rd, 2x4th, 5th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare, fear"],
    122: [4, "Bonus Spells: 3x1st, 3x2nd, 3x3rd, 3x4th, 2x5th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare, fear, charm monster, confusion, emotion, fumble, suggestion"],
    123: [4, "Bonus Spells: 4x1st, 3x2nd, 3x3rd, 3x4th, 2x5th, 1x6th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare, fear, charm monster, confusion, emotion, fumble, suggestion, chaos, feeblemind, hold monster,magic jar,quest"],
    124: [4, "Bonus Spells: 4x1st, 3x2nd, 3x3rd, 3x4th, 3x5th, 2x6th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare, fear, charm monster, confusion, emotion, fumble, suggestion, chaos, feeblemind, hold monster,magic jar,quest, geas, mass suggestion, rod of ruleship"],
    125: [4, "Bonus Spells: 4x1st, 3x2nd, 3x3rd, 3x4th, 3x5th, 3x6th,1x7th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare, fear, charm monster, confusion, emotion, fumble, suggestion, chaos, feeblemind, hold monster,magic jar,quest, geas, mass suggestion, rod of ruleship, antipathy/sympath, death spell,mass charm"]
};
OSRIC.wisdomTable["2"] = {
    0: [
        "OSRIC.abilityFields.wis.magic",
        "OSRIC.abilityFields.wis.bonus",
        "OSRIC.abilityFields.wis.failure",
        "OSRIC.abilityFields.wis.imm",
        //MAC base?,
        //PSP bonus?,
    ],
    1: [-6, "None", 80, "None", 10, 0],
    2: [-4, "None", 60, "None", 10, 0],
    3: [-3, "None", 50, "None", 10, 0],
    4: [-2, "None", 45, "None", 10, 0],
    5: [-1, "None", 40, "None", 10, 0],
    6: [-1, "None", 35, "None", 10, 0],
    7: [-1, "None", 30, "None", 10, 0],
    8: [0, "None", 25, "None", 10, 0],
    9: [0, "None", 20, "None", 10, 0],
    10: [0, "None", 15, "None", 10, 0],
    11: [0, "None", 10, "None", 10, 0],
    12: [0, "None", 5, "None", 10, 0],
    13: [0, "1x1st", 0, "None", 10, 0],
    14: [0, "2x1st", 0, "None", 10, 0],
    15: [1, "2x1st,1x2nd", 0, "None", 10, 0],
    16: [2, "2x1st,2x2nd", 0, "None", 9, 1],
    17: [3, "Various", 0, "None", 8, 2],
    18: [4, "Various", 0, "None", 7, 3],
    19: [4, "Various", 0, "Various", 6, 4],
    20: [4, "Various", 0, "Various", 5, 5],
    21: [4, "Various", 0, "Various", 4, 6],
    22: [4, "Various", 0, "Various", 3, 7],
    23: [4, "Various", 0, "Various", 2, 8],
    24: [4, "Various", 0, "Various", 1, 9],
    25: [4, "Various", 0, "Various", 0, 10],
    //-- deal with long string bonus for tooltip
    117: [3, "Bonus Spells: 2x1st, 2x2nd, 1x3rd", 0, "None"],
    118: [4, "Bonus Spells: 2x1st, 2x2nd, 1x3rd, 1x4th", 0, "None"],
    119: [4, "Bonus Spells: 3x1st, 2x2nd, 2x3rd, 1x4th", 0, "Spells: cause fear,charm person, command, friends, hypnotism"],
    120: [4, "Bonus Spells: 3x1st, 3x2nd, 2x3rd, 2x4th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare"],
    121: [4, "Bonus Spells: 3x1st, 3x2nd, 3x3rd, 2x4th, 5th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare, fear"],
    122: [4, "Bonus Spells: 3x1st, 3x2nd, 3x3rd, 3x4th, 2x5th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare, fear, charm monster, confusion, emotion, fumble, suggestion"],
    123: [4, "Bonus Spells: 4x1st, 3x2nd, 3x3rd, 3x4th, 2x5th, 1x6th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare, fear, charm monster, confusion, emotion, fumble, suggestion, chaos, feeblemind, hold monster,magic jar,quest"],
    124: [4, "Bonus Spells: 4x1st, 3x2nd, 3x3rd, 3x4th, 3x5th, 2x6th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare, fear, charm monster, confusion, emotion, fumble, suggestion, chaos, feeblemind, hold monster,magic jar,quest, geas, mass suggestion, rod of ruleship"],
    125: [4, "Bonus Spells: 4x1st, 3x2nd, 3x3rd, 3x4th, 3x5th, 3x6th,1x7th", 0, "Spells: cause fear,charm person, command, friends, hypnotism, forget, hold person, enfeeble, scare, fear, charm monster, confusion, emotion, fumble, suggestion, chaos, feeblemind, hold monster,magic jar,quest, geas, mass suggestion, rod of ruleship, antipathy/sympath, death spell,mass charm"]
};
OSRIC.constitutionTable = {}
// Constitution[abilityScore]={hp, system shock, resurrection survivial, poison save, regeneration, psp bonus}
OSRIC.constitutionTable["0"] = {
    0: [
        "OSRIC.abilityFields.con.hp",
        "OSRIC.abilityFields.con.shock",
        "OSRIC.abilityFields.con.survival",
        "OSRIC.abilityFields.con.poison",
        "OSRIC.abilityFields.con.regen"
    ],
    1: [[-2], 35, 40, 0, 0, 0],
    2: [[-2], 35, 40, 0, 0, 0],
    3: [[-3], 35, 30, 0, 0, 0],
    4: [[-2], 45, 45, 0, 0, 0],
    5: [[-2], 45, 45, 0, 0, 0],
    6: [[-1], 55, 60, 0, 0, 0],
    7: [[-1], 55, 60, 0, 0, 0],
    8: [[-1], 55, 60, 0, 0, 0],
    9: [[0], 70, 75, 0, 0, 0],
    10: [[0], 70, 75, 0, 0, 0],
    11: [[0], 70, 75, 0, 0, 0],
    12: [[0], 70, 75, 0, 0, 0],
    13: [[1], 85, 85, 0, 0, 0],
    14: [[1], 85, 85, 0, 0, 0],
    15: [[1], 90, 90, 0, 0, 0],
    16: [[2], 95, 95, 0, 0, 0],
    17: [[2], 97, 98, 0, 0, 0],
    18: [[3], 99, 100, 0, 0, 0],
    19: [[2, 5], 99, 100, 0, 0, 0],
    20: [[2, 5], 99, 100, 0, 0, 0],
    21: [[2, 5], 99, 100, 0, 0, 0],
    22: [[2, 5], 99, 100, 0, 0, 0],
    23: [[2, 5], 99, 100, 0, 0, 0],
    24: [[2, 5], 99, 100, 0, 0, 0],
    25: [[2, 5], 99, 100, 0, 0, 0]
};
// Constitution[abilityScore]={hp, system shock, resurrection survivial, poison save, regeneration, psp bonus}
OSRIC.constitutionTable["1"] = {
    0: ["OSRIC.abilityFields.con.hp", "OSRIC.abilityFields.con.shock", "OSRIC.abilityFields.con.survival", "OSRIC.abilityFields.con.poison", "OSRIC.abilityFields.con.regen"],
    1: [[-2], 35, 40, 0, "None", 0],
    2: [[-2], 35, 40, 0, "None", 0],
    3: [[-2], 35, 40, 0, "None", 0],
    4: [[-1], 40, 45, 0, "None", 0],
    5: [[-1], 45, 50, 0, "None", 0],
    6: [[-1], 50, 55, 0, "None", 0],
    7: [[0], 55, 60, 0, "None", 0],
    8: [[0], 60, 65, 0, "None", 0],
    9: [[0], 65, 70, 0, "None", 0],
    10: [[0], 70, 75, 0, "None", 0],
    11: [[0], 75, 80, 0, "None", 0],
    12: [[0], 80, 85, 0, "None", 0],
    13: [[0], 85, 90, 0, "None", 0],
    14: [[0], 88, 92, 0, "None", 0],
    15: [[1], 91, 94, 0, "None", 0],
    16: [[2], 95, 96, 0, "None", 0],
    17: [[2, 3], 97, 98, 0, "None", 0],
    18: [[2, 4], 99, 100, 0, "None", 0],
    19: [[2, 5], 99, 100, 1, "None", 0],
    20: [[2, 5], 99, 100, 1, "1/6 turns", 0],
    21: [[2, 6], 99, 100, 2, "1/5 turns", 0],
    22: [[2, 6], 99, 100, 2, "1/4 turns", 0],
    23: [[2, 6], 99, 100, 3, "1/3 turns", 0],
    24: [[2, 7], 99, 100, 3, "1/2", 0],
    25: [[2, 7], 100, 100, 4, "1 turn", 0]
};
// Constitution[abilityScore]={hp, system shock, resurrection survivial, poison save, regeneration, psp bonus}
OSRIC.constitutionTable["2"] = {
    0: ["OSRIC.abilityFields.con.hp", "OSRIC.abilityFields.con.shock", "OSRIC.abilityFields.con.survival", "OSRIC.abilityFields.con.poison", "OSRIC.abilityFields.con.regen"],
    1: [[-3], 25, 30, -2, "None", 0],
    2: [[-2], 30, 35, -1, "None", 0],
    3: [[-2], 35, 40, 0, "None", 0],
    4: [[-1], 40, 45, 0, "None", 0],
    5: [[-1], 45, 50, 0, "None", 0],
    6: [[-1], 50, 55, 0, "None", 0],
    7: [[0], 55, 60, 0, "None", 0],
    8: [[0], 60, 65, 0, "None", 0],
    9: [[0], 65, 70, 0, "None", 0],
    10: [[0], 70, 75, 0, "None", 0],
    11: [[0], 75, 80, 0, "None", 0],
    12: [[0], 80, 85, 0, "None", 0],
    13: [[0], 85, 90, 0, "None", 0],
    14: [[0], 88, 92, 0, "None", 0],
    15: [[1], 90, 94, 0, "None", 0],
    16: [[2], 95, 96, 0, "None", 1],
    17: [[2, 3], 97, 98, 0, "None", 2],
    18: [[2, 4], 99, 100, 0, "None", 3],
    19: [[2, 5], 99, 100, 1, "None", 4],
    20: [[2, 5], 99, 100, 1, "1/6 turns", 5],
    21: [[2, 6], 99, 100, 2, "1/5 turns", 6],
    22: [[2, 6], 99, 100, 2, "1/4 turns", 7],
    23: [[2, 6], 99, 100, 3, "1/3 turns", 8],
    24: [[2, 7], 99, 100, 3, "1/2", 9],
    25: [[2, 7], 100, 100, 4, "1 turn", 10]
};
OSRIC.charismaTable = {};
// Charisma[abilityScore]={max hench,loyalty base, reaction adj}
OSRIC.charismaTable["0"] = {
    0: [
        "OSRIC.abilityFields.cha.max",
        "OSRIC.abilityFields.cha.loyalty",
        "OSRIC.abilityFields.cha.reaction"
    ],
    1: [1, -2, -3],
    2: [1, -2, -3],
    3: [1, -2, -3],
    4: [2, -25, -20],
    5: [2, -20, -15],
    6: [3, -1, -1],
    7: [3, -1, -1],
    8: [3, -1, -1],
    9: [4, 0, 0],
    10: [4, 0, 0],
    11: [4, 0, 0],
    12: [4, 0, 0],
    13: [5, 1, 1],
    14: [5, 1, 1],
    15: [5, 1, 1],
    16: [6, 2, 2],
    17: [6, 2, 2],
    18: [8, 2, 3],
    19: [20, 5, 4],
    20: [20, 5, 4],
    21: [20, 5, 4],
    22: [20, 5, 4],
    23: [20, 5, 4],
    24: [20, 5, 4],
    25: [20, 5, 4],
};

OSRIC.charismaTable["1"] = {
    0: ["OSRIC.abilityFields.cha.max", "OSRIC.abilityFields.cha.loyalty", "OSRIC.abilityFields.cha.reaction"],
    1: [1, -30, -25],
    2: [1, -30, -25],
    3: [1, -30, -25],
    4: [1, -25, -20],
    5: [2, -20, -15],
    6: [2, -15, -10],
    7: [3, -10, -5],
    8: [3, -5, 0],
    9: [4, 0, 0],
    10: [4, 0, 0],
    11: [4, 0, 0],
    12: [5, 0, 0],
    13: [5, 0, 5],
    14: [6, 5, 10],
    15: [7, 15, 15],
    16: [8, 20, 25],
    17: [10, 30, 30],
    18: [15, 40, 35],
    19: [20, 50, 40],
    20: [20, 50, 40],
    21: [20, 50, 40],
    22: [20, 50, 40],
    23: [20, 50, 40],
    24: [20, 50, 40],
    25: [20, 50, 40],
};

OSRIC.charismaTable["2"] = {
    0: ["OSRIC.abilityFields.cha.max", "OSRIC.abilityFields.cha.loyalty", "OSRIC.abilityFields.cha.reaction"],
    1: [0, -8, -7],
    2: [1, -7, -6],
    3: [1, -6, -5],
    4: [1, -5, -4],
    5: [2, -4, -3],
    6: [2, -3, -2],
    7: [3, -2, -1],
    8: [3, -1, 0],
    9: [4, 0, 0],
    10: [4, 0, 0],
    11: [4, 0, 0],
    12: [5, 0, 0],
    13: [5, 0, 1],
    14: [6, 1, 2],
    15: [7, 3, 3],
    16: [8, 4, 5],
    17: [10, 6, 6],
    18: [15, 8, 7],
    19: [20, 10, 8],
    20: [25, 12, 9],
    21: [30, 14, 10],
    22: [35, 16, 1],
    23: [40, 18, 12],
    24: [45, 20, 13],
    25: [50, 20, 14]
};


OSRIC.intelligenceTable = {}
// Intelligence[abilityScore]={# languages, spelllevel, learn spell, max spells, illusion immunity, MAC mod, PSP Bonus,MTHACO bonus}
OSRIC.intelligenceTable["0"] = {
    0: [
        "OSRIC.abilityFields.int.languages",
        "OSRIC.abilityFields.int.level",
        "OSRIC.abilityFields.int.chance",
        "OSRIC.abilityFields.int.max",
        "OSRIC.abilityFields.int.imm"
    ],
    1: [0, 0, 0, 0, 0, 0, 0, 0],
    2: [0, 0, 0, 0, 0, 0, 0, 0],
    3: [0, 5, 20, 3, 0, 0, 0, 0],
    4: [0, 5, 20, 3, 0, 0, 0, 0],
    5: [0, 5, 30, 4, 0, 0, 0, 0],
    6: [0, 5, 30, 4, 0, 0, 0, 0],
    7: [0, 5, 30, 4, 0, 0, 0, 0],
    8: [1, 5, 40, 5, 0, 0, 0, 0],
    9: [1, 5, 40, 5, 0, 0, 0, 0],
    10: [2, 5, 50, 6, 0, 0, 0, 0],
    11: [2, 6, 55, 6, 0, 0, 0, 0],
    12: [3, 6, 60, 6, 0, 0, 0, 0],
    13: [3, 7, 65, 8, 0, 0, 0, 0],
    14: [4, 7, 70, 8, 0, 0, 0, 0],
    15: [4, 8, 75, 10, 0, 0, 0, 0],
    16: [5, 8, 80, 10, 0, 0, 0, 0],
    17: [5, 9, 85, "All", 0, 0, 0, 0],
    18: [6, 9, 95, "All", 0, 0, 0, 0],
    19: [8, 0, 90, 22, 0, 0, 0, 0],
    20: [8, 0, 90, 22, 0, 0, 0, 0],
    21: [8, 0, 90, 22, 0, 0, 0, 0],
    22: [8, 0, 90, 22, 0, 0, 0, 0],
    23: [8, 0, 90, 22, 0, 0, 0, 0],
    24: [8, 0, 90, 22, 0, 0, 0, 0],
    25: [8, 0, 90, 22, 0, 0, 0, 0],
    //-- these have such long values we stuff them into tooltips instead
    119: [0, 0, 0, "", ""],
    120: [0, 0, 0, "", ""],
    121: [0, 0, 0, "", ""],
    122: [0, 0, 0, "", ""],
    123: [0, 0, 0, "", ""],
    124: [0, 0, 0, "", ""],
    125: [0, 0, 0, "", ""],
};
// Intelligence[abilityScore]={# languages, spelllevel, learn spell, max spells, illusion immunity, MAC mod, PSP Bonus,MTHACO bonus}
OSRIC.intelligenceTable["1"] = {
    0: ["OSRIC.abilityFields.int.languages", "OSRIC.abilityFields.int.level", "OSRIC.abilityFields.int.chance", "OSRIC.abilityFields.int.max", "OSRIC.abilityFields.int.imm"],
    1: [0, 0, 0, 0, "None", 0, 0, 0],
    2: [1, 0, 0, 0, "None", 0, 0, 0],
    3: [1, 0, 0, 0, "None", 0, 0, 0],
    4: [1, 0, 0, 0, "None", 0, 0, 0],
    5: [1, 0, 0, 0, "None", 0, 0, 0],
    6: [1, 0, 0, 0, "None", 0, 0, 0],
    7: [1, 0, 0, 0, "None", 0, 0, 0],
    8: [1, 0, 0, 0, "None", 0, 0, 0],
    9: [1, 4, 35, 6, "None", 0, 0, 0],
    10: [2, 5, 40, 7, "None", 0, 0, 0],
    11: [2, 5, 45, 7, "None", 0, 0, 0],
    12: [3, 6, 50, 7, "None", 0, 0, 0],
    13: [3, 6, 55, 9, "None", 0, 0, 0],
    14: [4, 7, 60, 9, "None", 0, 0, 0],
    15: [4, 7, 65, 11, "None", 0, 0, 0],
    16: [5, 8, 70, 11, "None", 0, 0, 0],
    17: [6, 8, 75, 14, "None", 0, 0, 0],
    18: [7, 9, 85, 18, "None", 0, 0, 0],
    19: [8, 9, 95, "All", "1st", 0, 0, 0],
    20: [9, 9, 96, "All", "1,2", 0, 0, 0],
    21: [10, 9, 97, "All", "1,2,3", 0, 0, 0],
    22: [11, 9, 98, "All", "1,2,3,4", 0, 0, 0],
    23: [12, 9, 99, "All", "1,2,3,4,5", 0, 0, 0],
    24: [15, 9, 100, "All", "1,2,3,4,5,6", 0, 0, 0],
    25: [20, 9, 100, "All", "1,2,3,4,5,6,7", 0, 0, 0],
    //-- these have such long values we stuff them into tooltips instead
    119: [8, 9, 95, "All", "Level: 1st"],
    120: [9, 9, 96, "All", "Level: 1st, 2nd"],
    121: [10, 9, 97, "All", "Level: 1st, 2nd, 3rd"],
    122: [11, 9, 98, "All", "Level: 1st, 2nd, 3rd, 4th"],
    123: [12, 9, 99, "All", "Level: 1st, 2nd, 3rd, 4th, 5th"],
    124: [15, 9, 100, "All", "Level: 1st, 2nd, 3rd, 4th, 5th, 6th"],
    125: [20, 9, 100, "All", "Level: 1st, 2nd, 3rd, 4th, 5th, 6th, 7th"]
};
// Intelligence[abilityScore]={# languages, spelllevel, learn spell, max spells, illusion immunity, MAC mod, PSP Bonus,MTHACO bonus}
OSRIC.intelligenceTable["2"] = {
    0: ["OSRIC.abilityFields.int.languages", "OSRIC.abilityFields.int.level", "OSRIC.abilityFields.int.chance", "OSRIC.abilityFields.int.max", "OSRIC.abilityFields.int.imm"],
    1: [0, 0, 0, 0, "None", 0, 0, 0],
    2: [1, 0, 0, 0, "None", 0, 0, 0],
    3: [1, 0, 0, 0, "None", 0, 0, 0],
    4: [1, 0, 0, 0, "None", 0, 0, 0],
    5: [1, 0, 0, 0, "None", 0, 0, 0],
    6: [1, 0, 0, 0, "None", 0, 0, 0],
    7: [1, 0, 0, 0, "None", 0, 0, 0],
    8: [1, 0, 0, 0, "None", 0, 0, 0],
    9: [2, 4, 35, 6, "None", 0, 0, 0],
    10: [2, 5, 40, 7, "None", 0, 0, 0],
    11: [2, 5, 45, 7, "None", 0, 0, 0],
    12: [3, 6, 50, 7, "None", 0, 0, 0],
    13: [3, 6, 55, 9, "None", 0, 0, 0],
    14: [4, 7, 60, 9, "None", 0, 0, 0],
    15: [4, 7, 65, 11, "None", 0, 0, 0],
    16: [5, 8, 70, 11, "None", 1, 1, 1],
    17: [6, 8, 75, 14, "None", 1, 2, 1],
    18: [7, 9, 85, 18, "None", 2, 3, 2],
    19: [8, 9, 95, "All", "1st", 2, 4, 2],
    20: [9, 9, 96, "All", "1,2", 3, 5, 3],
    21: [10, 9, 97, "All", "1,2,3", 3, 6, 3],
    22: [11, 9, 98, "All", "1,2,3,4", 3, 7, 3],
    23: [12, 9, 99, "All", "1,2,3,4,5", 4, 8, 4],
    24: [15, 9, 100, "All", "1,2,3,4,5,6", 4, 9, 4],
    25: [20, 9, 100, "All", "1,2,3,4,5,6,7", 4, 10, 4],
    //-- these have such long values we stuff them into tooltips instead
    119: [8, 9, 95, "All", "Level: 1st"],
    120: [9, 9, 96, "All", "Level: 1st, 2nd"],
    121: [10, 9, 97, "All", "Level: 1st, 2nd, 3rd"],
    122: [11, 9, 98, "All", "Level: 1st, 2nd, 3rd, 4th"],
    123: [12, 9, 99, "All", "Level: 1st, 2nd, 3rd, 4th, 5th"],
    124: [15, 9, 100, "All", "Level: 1st, 2nd, 3rd, 4th, 5th, 6th"],
    125: [20, 9, 100, "All", "Level: 1st, 2nd, 3rd, 4th, 5th, 6th, 7th"]
};

OSRIC.saveArrayMap = {
    'paralyzation': 0,
    'poison': 0,
    'death': 0,

    'rod': 1,
    'staff': 1,
    'wand': 1,

    'petrification': 2,
    'polymorph': 2,

    'breath': 3,
    'spell': 4,
}
// para/poison/Death, Rod/staff/wand, petri/Poly, Breath, Spell
OSRIC.npcSaveTable = {
    // OSRIC 
    "0": {
        0: [16, 18, 17, 20, 19],
        1: [14, 16, 15, 17, 17],
        2: [14, 16, 15, 17, 17],
        3: [13, 15, 14, 16, 16],
        4: [13, 15, 14, 16, 16],
        5: [11, 13, 12, 13, 14],
        6: [11, 13, 12, 13, 14],
        7: [10, 12, 11, 12, 13],
        8: [10, 12, 11, 12, 13],
        9: [8, 10, 9, 9, 11],
        10: [8, 10, 9, 9, 11],
        11: [7, 9, 8, 8, 10],
        12: [7, 9, 8, 8, 10],
        13: [5, 7, 6, 5, 8],
        14: [5, 7, 6, 5, 8],
        15: [4, 6, 5, 4, 7],
        16: [4, 6, 5, 4, 7],
        17: [3, 5, 4, 4, 6],
        18: [3, 5, 4, 4, 6],
        19: [2, 4, 3, 3, 5],
        20: [2, 4, 3, 3, 5],
        21: [2, 4, 3, 3, 5]
    },
    // variant 1
    "1": {
        0: [16, 18, 17, 20, 19],
        1: [14, 16, 15, 17, 17],
        2: [14, 16, 15, 17, 17],
        3: [13, 15, 14, 16, 16],
        4: [13, 15, 14, 16, 16],
        5: [11, 13, 12, 13, 14],
        6: [11, 13, 12, 13, 14],
        7: [10, 12, 11, 12, 13],
        8: [10, 12, 11, 12, 13],
        9: [8, 10, 9, 9, 11],
        10: [8, 10, 9, 9, 11],
        11: [7, 9, 8, 8, 10],
        12: [7, 9, 8, 8, 10],
        13: [5, 7, 6, 5, 8],
        14: [5, 7, 6, 5, 8],
        15: [4, 6, 5, 4, 7],
        16: [4, 6, 5, 4, 7],
        17: [3, 5, 4, 4, 6],
        18: [3, 5, 4, 4, 6],
        19: [3, 5, 4, 4, 6],
        20: [3, 5, 4, 4, 6],
        21: [3, 5, 4, 4, 6]
    },
    // variant 2
    "2": {
        0: [16, 18, 17, 20, 19],
        1: [14, 16, 15, 17, 17],
        2: [14, 16, 15, 17, 17],
        3: [13, 15, 14, 16, 16],
        4: [13, 15, 14, 16, 16],
        5: [11, 13, 12, 13, 14],
        6: [11, 13, 12, 13, 14],
        7: [10, 12, 11, 12, 13],
        8: [10, 12, 11, 12, 13],
        9: [8, 10, 9, 9, 11],
        10: [8, 10, 9, 9, 11],
        11: [7, 9, 8, 8, 10],
        12: [7, 9, 8, 8, 10],
        13: [5, 7, 6, 5, 8],
        14: [5, 7, 6, 5, 8],
        15: [4, 6, 5, 4, 7],
        16: [4, 6, 5, 4, 7],
        17: [3, 5, 4, 4, 6],
        18: [3, 5, 4, 4, 6],
        19: [3, 5, 4, 4, 6],
        20: [3, 5, 4, 4, 6],
        21: [3, 5, 4, 4, 6]
    }
}

OSRIC.currencyType = [
    'cp', 'sp', 'ep', 'gp', 'pp'
]
// These values are a copper base value. 
// // game.osric.config.settings.osricVariant
OSRIC.currencyValue = {
    "0": {
        "cp": 1,
        "sp": 10,
        "ep": 50,
        "gp": 100,
        "pp": 500
    },
    "1": {
        "cp": 1,
        "sp": 10,
        "ep": 50,
        "gp": 100,
        "pp": 500
    },
    "2": {
        "cp": 1,
        "sp": 10,
        "ep": 50,
        "gp": 100,
        "pp": 500
    }
}

OSRIC.currencyWeight = {
    "0": 10,
    "1": 10,
    "2": 50,
}
OSRIC.itemTypes = [
    "Alchemical",
    "Ammunition",
    "Animal",
    "Art",
    "Clothing",
    "Daily Food and Lodging",
    "Equipment Packs",
    "Gear",
    "Gem",
    "Jewelry",
    "Provisions",
    "Scroll",
    "Service",
    "Herb or Spice",
    "Tack and Harness",
    "Tool",
    "Transport",
    "Other"
];

OSRIC.itemRarityTypes = [
    "Common",
    "Uncommon",
    "Rare",
    "Very Rare",
    "Unique",
    "Other"
]
// Modifiers to be applied when distance is > medium/long
OSRIC.rangeModifiers = {
    "short": 0,
    "medium": -2,
    "long": -5
}
