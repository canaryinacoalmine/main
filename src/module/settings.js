import { OSRIC } from './config.js';
import * as debug from "./debug.js"
export const registerSystemSettings = function () {


  /**
     * Register initiative dice setting
     */

  // Internal System Migration Version tracking
  game.settings.register("osric", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: false,
    type: String,
    default: ""
  });

  // Internal System Changelog Version tracking
  game.settings.register("osric", "systemChangeLogVersion", {
    name: "System Changelog Version",
    scope: "world",
    config: false,
    type: String,
    default: ""
  });


  /**
   * 
   * Settings used internally for Party-Tracker
   * 
   */
  game.settings.register("osric", "partyMembers", {
    name: "System Party Tracker - Members",
    scope: "world",
    config: false,
    type: Array,
    default: []
  });
  game.settings.register("osric", "partyAwards", {
    name: "System Party Tracker - Awards",
    scope: "world",
    config: false,
    type: Array,
    default: []
  });
  game.settings.register("osric", "partyLogs", {
    name: "System Party Tracker - Logs",
    requiresReload: false,
    scope: "world",
    config: false,
    type: Array,
    default: []
  });
  // end Party-Tracker settings

  //const osricVariant = game.settings.get("osric", "osricVariant");
  game.settings.register("osric", "osricVariant", {
    name: "SETTINGS.osricVariantLabel",
    hint: "SETTINGS.osricVariantTT",
    scope: "world",
    requiresReload: true,
    config: true,
    type: String,
    choices: {
      "0": "SETTINGS.osricVariant.0",
      "1": "SETTINGS.osricVariant.1",
      "2": "SETTINGS.osricVariant.2"
    },
    default: "0",
    onChange: osricVariant => {
      CONFIG.OSRIC.settings.osricVariant = osricVariant;
      // change initiative based on variant
      switch (osricVariant) {
        case '0':
          game.settings.set("osric", "initiativeFormula", "1d6");
          // game.settings.settings.get("osric.variant2ArmorDamage").config = false;
          break;
        case '1':
          game.settings.set("osric", "initiativeFormula", "1d6");
          // game.settings.settings.get("osric.variant2ArmorDamage").config = false;
          break;
        case '2':
          game.settings.set("osric", "initiativeFormula", "1d10");
          // game.settings.settings.get("osric.variant2ArmorDamage").config = true;
          break;
        default:
          game.settings.set("osric", "initiativeFormula", "1d6");
          // game.settings.settings.get("osric.variant2ArmorDamage").config = false;
          break;
      }

    }
  });


  //const initiativeUseSpeed = game.settings.get("osric", "initiativeUseSpeed");
  game.settings.register("osric", "initiativeUseSpeed", {
    name: "SETTINGS.OsricinitiativeUseSpeed",
    hint: "SETTINGS.OsricinitiativeUseSpeedTT",
    scope: "world",
    config: true,
    default: false,
    restricted: true,
    type: Boolean,
  });

  //const initiativeFormula = game.settings.get("osric", "initiativeFormula");
  game.settings.register("osric", "initiativeFormula", {
    name: "SETTINGS.OsricinitiativeFormula",
    hint: "SETTINGS.OsricinitiativeFormulaTT",
    scope: "world",
    config: true,
    default: "1d6",
    restricted: true,
    type: String,
    onChange: initiativeFormula => CONFIG.Combat.initiative.formula = initiativeFormula
  });

  //const ascendingInitiative = game.settings.get("osric", "InitiativeAscending");
  game.settings.register("osric", "InitiativeAscending", {
    name: "SETTINGS.OsricInitiativeAscending",
    hint: "SETTINGS.OsricInitiativeAscendingTT",
    scope: "world",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
  });

  //const panOnInitiative = game.settings.get("osric", "panOnInitiative");
  game.settings.register("osric", "panOnInitiative", {
    name: "SETTINGS.panOnInitiative",
    hint: "SETTINGS.panOnInitiativeTT",
    scope: "client",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
  });


  //const variant2ArmorDamage = game.settings.get("osric", "variant2ArmorDamage");
  game.settings.register("osric", "variant2ArmorDamage", {
    name: "SETTINGS.variant2ArmorDamage",
    hint: "SETTINGS.variant2ArmorDamageTT",
    scope: "world",
    config: false,
    default: false,
    restricted: true,
    type: Boolean,
    onChange: variant2ArmorDamage => CONFIG.OSRIC.settings.variant2ArmorDamage = variant2ArmorDamage
  });

  //const useAutoHitFailDice = game.settings.get("osric", "useAutoHitFailDice");
  game.settings.register("osric", "useAutoHitFailDice", {
    name: "SETTINGS.OsricAutoHitFailDice",
    hint: "SETTINGS.OsricAutoHitFailDiceTT",
    scope: "world",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
    onChange: useAutoHitFailDice => CONFIG.OSRIC.settings.autohitfail = useAutoHitFailDice
  });


  // *** disabling this for now.
  //const autoDamage = game.settings.get("osric", "autoDamage");
  game.settings.register("osric", "autoDamage", {
    name: "SETTINGS.OsricautoDamage",
    hint: "SETTINGS.OsricautoDamageTT",
    scope: "world",
    config: false,
    default: false,
    restricted: true,
    type: Boolean,
  });

  //const autoCheck = game.settings.get("osric", "autoCheck");
  game.settings.register("osric", "autoCheck", {
    name: "SETTINGS.OsricautoCheck",
    hint: "SETTINGS.OsricautoCheckTT",
    scope: "world",
    config: true,
    default: false,
    restricted: true,
    type: Boolean,
  });


  //const rollInitEachRound = game.settings.get("osric", "rollInitEachRound");
  game.settings.register("osric", "rollInitEachRound", {
    name: "SETTINGS.OsricrollInitEachRound",
    hint: "SETTINGS.OsricrollInitEachRoundTT",
    scope: "world",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
  });


  //const automateLighting = game.settings.get("osric", "automateLighting");
  game.settings.register("osric", "automateLighting", {
    name: "SETTINGS.automateLighting",
    hint: "SETTINGS.automateLightingTT",
    scope: "world",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
    onChange: automateLighting => {
      CONFIG.OSRIC.settings.automateLighting = automateLighting
    }
  });

  //const automateVision = game.settings.get("osric", "automateVision");
  game.settings.register("osric", "automateVision", {
    name: "SETTINGS.automateVision",
    hint: "SETTINGS.automateVisionTT",
    scope: "world",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
    onChange: automateVision => {
      CONFIG.OSRIC.settings.automateVision = automateVision
    }
  });

  //const initRoundSound = game.settings.get("osric", "initRoundSound");
  game.settings.register("osric", "initRoundSound", {
    name: "SETTINGS.initRoundSound",
    hint: "SETTINGS.initRoundSoundTT",
    scope: "world",
    config: true,
    default: OSRIC.sounds.initiative.start,
    type: String,
  });

  //const initTurnSound = game.settings.get("osric", "initTurnSound");
  game.settings.register("osric", "initTurnSound", {
    name: "SETTINGS.initTurnSound",
    hint: "SETTINGS.initTurnSoundTT",
    scope: "world",
    config: true,
    default: OSRIC.sounds.initiative.turn,
    type: String,
  });

  //const initVolumeSound = game.settings.get("osric", "initVolumeSound");
  game.settings.register("osric", "initVolumeSound", {
    name: "SETTINGS.initVolumeSound",
    hint: "SETTINGS.initVolumeSoundTT",
    scope: "client",
    config: true,
    range: {
      min: 0,
      max: 1,
      step: 0.1,
    },
    default: 0.5,
    type: Number,
  });

  //const playTurnSound = game.settings.get("osric", "playTurnSound");
  game.settings.register("osric", "playTurnSound", {
    name: "SETTINGS.initTurnSound",
    hint: "SETTINGS.initTurnSoundTT",
    scope: "client",
    config: true,
    default: true,
    type: Boolean,
  });
  //const playRoundSound = game.settings.get("osric", "playRoundSound");
  game.settings.register("osric", "playRoundSound", {
    name: "SETTINGS.initRoundSound",
    hint: "SETTINGS.initRoundSoundTT",
    scope: "client",
    config: true,
    default: true,
    type: Boolean,
  });

  //const audioPlayTriggers = game.settings.get("osric", "audioPlayTriggers");
  game.settings.register("osric", "audioPlayTriggers", {
    name: "SETTINGS.audioPlayTriggers",
    hint: "SETTINGS.audioPlayTriggersTT",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
  });

  //const audioTriggersVolume = game.settings.get("osric", "audioTriggersVolume");
  game.settings.register("osric", "audioTriggersVolume", {
    name: "SETTINGS.audioTriggersVolume",
    hint: "SETTINGS.audioTriggersVolumeTT",
    scope: "client",
    config: true,
    range: {
      min: 0,
      max: 1,
      step: 0.1,
    },
    default: 0.5,
    type: Number,
  });

  //const audioTriggerCheckSuccess = game.settings.get("osric", "audioTriggerCheckSuccess");
  game.settings.register("osric", "audioTriggerCheckSuccess", {
    name: "SETTINGS.audioTriggerCheckSuccess",
    hint: "SETTINGS.audioTriggerCheckSuccessTT",
    scope: "world",
    config: true,
    default: OSRIC.sounds.save.success,
    onChange: audioTriggerCheckSuccess => {
      CONFIG.OSRIC.sounds.save.success = audioTriggerCheckSuccess
    },
    type: String,
  });
  //const audioTriggerCheckFail = game.settings.get("osric", "audioTriggerCheckFail");
  game.settings.register("osric", "audioTriggerCheckFail", {
    name: "SETTINGS.audioTriggerCheckFail",
    hint: "SETTINGS.audioTriggerCheckFailTT",
    scope: "world",
    config: true,
    default: OSRIC.sounds.save.failure,
    onChange: audioTriggerCheckFail => {
      CONFIG.OSRIC.sounds.save.failure = audioTriggerCheckFail
    },
    type: String,
  });
  //const audioTriggerMeleeHit = game.settings.get("osric", "audioTriggerMeleeHit");
  game.settings.register("osric", "audioTriggerMeleeHit", {
    name: "SETTINGS.audioTriggerMeleeHit",
    hint: "SETTINGS.audioTriggerMeleeHitTT",
    scope: "world",
    config: true,
    default: OSRIC.sounds.combat['melee-hit'],
    onChange: audioTriggerMeleeHit => {
      CONFIG.OSRIC.sounds.combat['melee-hit'] = audioTriggerMeleeHit
    },
    type: String,
  });
  //const audioTriggerMeleeMiss = game.settings.get("osric", "audioTriggerMeleeMiss");
  game.settings.register("osric", "audioTriggerMeleeMiss", {
    name: "SETTINGS.audioTriggerMeleeMiss",
    hint: "SETTINGS.audioTriggerMeleeMissTT",
    scope: "world",
    config: true,
    default: OSRIC.sounds.combat['melee-miss'],
    onChange: audioTriggerMeleeMiss => {
      CONFIG.OSRIC.sounds.combat['melee-miss'] = audioTriggerMeleeMiss
    },
    type: String,
  });
  //const audioTriggerMeleeCrit = game.settings.get("osric", "audioTriggerMeleeCrit");
  game.settings.register("osric", "audioTriggerMeleeCrit", {
    name: "SETTINGS.audioTriggerMeleeCrit",
    hint: "SETTINGS.audioTriggerMeleeCritTT",
    scope: "world",
    config: true,
    default: OSRIC.sounds.combat['melee-hit-crit'],
    onChange: audioTriggerMeleeCrit => {
      CONFIG.OSRIC.sounds.combat['melee-hit-crit'] = audioTriggerMeleeCrit
    },
    type: String,
  });
  //const audioTriggerRangeHit = game.settings.get("osric", "audioTriggerRangeHit");
  game.settings.register("osric", "audioTriggerRangeHit", {
    name: "SETTINGS.audioTriggerRangeHit",
    hint: "SETTINGS.audioTriggerRangeHitTT",
    scope: "world",
    config: true,
    default: OSRIC.sounds.combat['missile-hit'],
    onChange: audioTriggerRangeHit => {
      CONFIG.OSRIC.sounds.combat['missile-hit'] = audioTriggerRangeHit
    },
    type: String,
  });
  //const audioTriggerRangeMiss = game.settings.get("osric", "audioTriggerRangeMiss");
  game.settings.register("osric", "audioTriggerRangeMiss", {
    name: "SETTINGS.audioTriggerRangeMiss",
    hint: "SETTINGS.audioTriggerRangeMissTT",
    scope: "world",
    config: true,
    default: OSRIC.sounds.combat['missile-miss'],
    onChange: audioTriggerRangeMiss => {
      CONFIG.OSRIC.sounds.combat['missile-miss'] = audioTriggerRangeMiss
    },
    type: String,
  });
  //const audioTriggerRangeCrit = game.settings.get("osric", "audioTriggerRangeCrit");
  game.settings.register("osric", "audioTriggerRangeCrit", {
    name: "SETTINGS.audioTriggerRangeCrit",
    hint: "SETTINGS.audioTriggerRangeCritTT",
    scope: "world",
    config: true,
    default: OSRIC.sounds.combat['missile-hit-crit'],
    onChange: audioTriggerRangeCrit => {
      CONFIG.OSRIC.sounds.combat['missile-hit-crit'] = audioTriggerRangeCrit
    },
    type: String,
  });
  //const audioTriggerDeath = game.settings.get("osric", "audioTriggerDeath");
  game.settings.register("osric", "audioTriggerDeath", {
    name: "SETTINGS.audioTriggerDeath",
    hint: "SETTINGS.audioTriggerDeathTT",
    scope: "world",
    config: true,
    default: OSRIC.sounds.combat.death,
    onChange: audioTriggerDeath => {
      CONFIG.OSRIC.sounds.combat.death = audioTriggerDeath
    },
    type: String,
  });

  //const ctShowOnlyVisible = game.settings.get("osric", "ctShowOnlyVisible");
  game.settings.register("osric", "ctShowOnlyVisible", {
    name: "SETTINGS.ctShowOnlyVisible",
    hint: "SETTINGS.ctShowOnlyVisibleTT",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    onChange: ctShowOnlyVisible => {
      CONFIG.OSRIC.settings.ctShowOnlyVisible = ctShowOnlyVisible
      ui.combat.render();
    }
  });

  //const encumbranceIncludeCoin = game.settings.get("osric", "encumbranceIncludeCoin");
  game.settings.register("osric", "encumbranceIncludeCoin", {
    name: "SETTINGS.encumbranceIncludeCoin",
    hint: "SETTINGS.encumbranceIncludeCoinTT",
    scope: "world",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
    onChange: encumbranceIncludeCoin => {
      CONFIG.OSRIC.settings.encumbranceIncludeCoin = encumbranceIncludeCoin
    }
  });


  //const identificationActor = game.settings.get("osric", "identificationActor");
  game.settings.register("osric", "identificationActor", {
    name: "SETTINGS.identificationActor",
    hint: "SETTINGS.identificationActorTT",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    onChange: identificationActor => {
      CONFIG.OSRIC.settings.identificationActor = identificationActor
    }
  });

  //const identificationItem = game.settings.get("osric", "identificationItem");
  game.settings.register("osric", "identificationItem", {
    name: "SETTINGS.identificationItem",
    hint: "SETTINGS.identificationItemTT",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    onChange: identificationItem => {
      CONFIG.OSRIC.settings.identificationItem = identificationItem
    }
  });

  //const npcNumberedNames = game.settings.get("osric", "npcNumberedNames");
  game.settings.register("osric", "npcNumberedNames", {
    name: "SETTINGS.OsricNPCNumberedName",
    hint: "SETTINGS.OsricNPCNumberedNameTT",
    scope: "world",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
  });

  //const npcLootable = game.settings.get("osric", "npcLootable");
  game.settings.register("osric", "npcLootable", {
    name: "SETTINGS.npcLootable",
    hint: "SETTINGS.npcLootableTT",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    onChange: npcLootable => CONFIG.OSRIC.settings.npcLootable = npcLootable
  });


  //const debugMode = game.settings.get("osric", "debugMode");
  game.settings.register("osric", "debugMode", {
    name: "SETTINGS.OsricdebugMode",
    hint: "SETTINGS.OsricdebugModeTT",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: debugMode => CONFIG.OSRIC.settings.debugMode = debugMode
  });

  //const itemBrowserMargin = game.settings.get("osric", "itemBrowserMargin");
  game.settings.register("osric", "itemBrowserMargin", {
    name: "SETTINGS.itemBrowserMargin",
    hint: "SETTINGS.itemBrowserMarginTT",
    scope: "world",
    config: true,
    restricted: true,
    range: {
      min: -100,
      max: 100,
      step: 1,
    },
    default: 0,
    type: Number,
    onChange: itemBrowserMargin => {
      if (game.osric.ui?.itembrowser)
        game.osric.ui.itembrowser.render(true);
    }
  });

}