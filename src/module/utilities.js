
import { OSRIC } from './config.js';
import * as effectManager from "./effect/effects.js";
import * as actionManager from "./apps/action.js";
import * as initLibrary from "./library.js"
import * as dialogManager from "./dialog.js";
import * as debug from "./debug.js";

/**
 * 
 * Capitalize string
 * 
 * @param {*} s 
 * @returns 
 */
export function capitalize(s) {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

/**
 * 
 * Request a action to be performed by the connected GM
 * 
 *           utilitiesManager.runAsGM({
 *               operation: 'applyActionEffect',
 *               user: game.user.id,
 *               sourceActorId: source.id,
 *               sourceTokenId: sourceToken.id
 *               targetActorId: target.id,
 *               targetTokenId: token.id,
 *               targetItemId: item.id,
 *               itemUpdate: {'some.path': value}
 *               sourceAction: sourceAction
 *           });
 * 
 * @param {Object} data Requested command data
 */
export async function runAsGM(data = {}) {
    // if GM we skip to the command
    if (game.user.isGM) {
        await runGMCommand(data);
    } else {
        // send data to socket and look for GM to run the command for user
        const dataPacket = {
            requestId: randomID(16),
            type: 'runAsGM',
            ...data
        }
        // Emit a socket event
        console.trace("runAsGM", { data, dataPacket });
        await game.socket.emit('system.osric', dataPacket);
    }
}

/**
 * 
 * Process the requested GM Command
 * 
 * @param {*} data Requested command data
 */
export async function processGMCommand(data = {}) {
    // const findGM = game.user === game.users.filter((user) => user.isGM && user.active).sort((a, b) => a.id - b.id)[0];
    const activeGMs = game.users.filter((user) => user.isGM && user.active);
    const findGM = activeGMs.length ? activeGMs[0] : null;
    console.trace("processGMCommand", { data, activeGMs, findGM });
    if (!findGM) {
        ui.notifications.error(`No GM connected to process requested command.`);
        console.trace("processGMCommand No GM connected to process requested command.");
    } else
        // check to see if the GM we found is the person we've emit'd to and if so run command.
        if (findGM.id === game.user.id) {
            if (!game.osric.runAsGMRequestIds[data.requestId]) {

                // console.log("utilities.js processGMCommand", { data });

                // We do this to make sure the command is only run once if more than 
                // one GM is on the server
                game.osric.runAsGMRequestIds[data.requestId] = data.requestId;

                /**
                 * "data" is serialized and deserialized so the type is lost.
                 * Because of that we just exchange IDs when we need protos/etc 
                 * and load them where needed
                 * 
                 */
                // let source = data.sourceId ? canvas.tokens.get(data.sourceId) : undefined;
                // let target = data.targetId ? canvas.tokens.get(data.targetId) : undefined;

                // const sourceActor = data.sourceActorId ? game.actors.get(data.sourceActorId) : undefined;
                // const targetActor = data.targetActorId ? game.actors.get(data.targetActorId) : undefined;
                // const targetToken = data.targetTokenId ? canvas.tokens.get(data.targetTokenId) : undefined;

                // console.log("utilities.js processGMCommand", { data });

                await runGMCommand(data);
            } else {
                // requestId already processed
                console.log("utilities.js", "processGMCommand", "Unknown asGM request DUPLICATE ", { data });
                ui.notifications.error(`Duplicate asGM request command.`, { permanent: true });
            }
        }
}

/**
 * 
 * Run command as GM, final leg (after sending it to socket or directly executed by GM)
 * 
 * @param {Object} data 
 */
async function runGMCommand(data) {

    console.log("utilitis.js runGMCommand", { data })

    const sourceActor = data.sourceActorId ? game.actors.get(data.sourceActorId) : undefined;
    const sourceToken = data.sourceTokenId ? canvas.tokens.get(data.sourceTokenId) : undefined;
    const targetActor = data.targetActorId ? game.actors.get(data.targetActorId) : undefined;
    const targetToken = data.targetTokenId ? canvas.tokens.get(data.targetTokenId) : undefined;
    const targetItemId = data.targetItemId ? data.targetItemId : undefined;

    // console.log("utilitis.js runGMCommand", { sourceActor, sourceToken, targetActor, targetToken, targetItemId })
    switch (data.operation) {

        case 'game.party.updateMember':
            if (data.sourceActorId) {
                game.party.updateMember(data.sourceActorId);
            }
            break;

        case 'deleteEmbeddedDocuments':
            if (targetActor && targetItemId) {
                targetActor.deleteEmbeddedDocuments("Item", [targetItemId], { hideChanges: true });
            } else if (targetToken && targetItemId) {
                targetToken.actor.deleteEmbeddedDocuments("Item", [targetItemId], { hideChanges: true });
            }
            break;

        case 'createEmbeddedDocuments':
            if (targetActor && data.itemData) {
                await targetActor.createEmbeddedDocuments("Item", [data.itemData], { hideChanges: true });
            }
            break;

        case "deleteActiveEffect":
            await targetToken.actor.deleteEmbeddedDocuments("ActiveEffect", data.effectIds);
            break;

        case "applyActionEffect":
            await effectManager.applyActionEffect(sourceToken ? sourceToken.actor : sourceActor, targetToken, data.sourceAction, data.user);
            break;

        case "adjustTargetHealth":
            await setActorHealth(targetToken.actor, data.targetHPresult);
            if (targetToken.hasActiveHUD) canvas.tokens.hud.render();
            break;

        case "unsetFlag":
            await targetToken.document.unsetFlag("osric", data.flag.tag);
            break;

        case "setFlag":
            await targetToken.document.setFlag("osric", data.flag.tag, data.flag.data);
            break;

        case 'itemUpdate':
            let itemToUpdate;
            if (targetActor && targetItemId && data.update) {
                itemToUpdate = await targetActor.getEmbeddedDocument("Item", targetItemId);
                if (itemToUpdate) itemToUpdate.update(data.update);
            } else if (targetToken && targetItemId && data.update) {
                itemToUpdate = await targetToken.actor.getEmbeddedDocument("Item", targetItemId);
                if (itemToUpdate) itemToUpdate.update(data.update);
            }
            break;

        case 'actorUpdate':
            if (targetActor && data.update) {
                await targetActor.update(data.update);
            } else if (targetToken && data.update) {
                await targetToken.actor.update(data.update);
            }
            break;

        case 'partyAddLogEntry':
            if (data.text && game.party?.addLogEntry) {
                await game.party.addLogEntry(data.text);
            }
            break;

        case 'partyShareLootedCoins':
            if (sourceToken && game.party?.shareLootedCoins) {
                game.party.shareLootedCoins(sourceToken);
            }
            break;

        default:
            console.log("utilities.js processGMCommand Unknown asGM/runGMCommand request ", data.operation, { data });
            break;
    };
}

/**
 * 
 * Adjust actor heath accounting for min/max
 * 
 * @param {*} actor 
 * @param {*} adjustment  12 or -12
 */
export function adjustActorHealth(actor, adjustment) {
    console.log("utilitis.js adjustActorHealth ", { actor, adjustment });

    const nCurrent = parseInt(actor.system.attributes.hp.value);
    const nMax = parseInt(actor.system.attributes.hp.max);
    const nMin = parseInt(actor.system.attributes.hp.min);

    console.log("utilitis.js adjustActorHealth ", { actor, adjustment, nCurrent, nMax, nMinax });

    let nNew = nCurrent + parseInt(adjustment);
    if (nNew > nMax) nNew = nMax;
    if (nNew < nMin) nNew = nMin;
    setActorHealth(actor, nNew);
}
/**
 * 
 * Set new health value on token.
 * 
 * @param {*} targetActor  targetActor's health to adjust
 * @param {*} value  The new value of target tokens health
 */
export async function setActorHealth(targetActor, value) {
    console.log("utilities.js setActorHealth", { targetActor, value });
    await targetActor.update({ "system.attributes.hp.value": value });
    // set status markers for health values
    setHealthStatusMarkers(targetActor);
}

/**
 * 
 * If the actor hp is at min or lower, mark them defeated
 * 
 * @param {*} actorData 
 */
async function setHealthStatusMarkers(targetActor) {
    console.log("utilities.js setDefeatedStatus", { targetActor });
    // need token object, not token document
    const token = targetActor.getToken().object;
    if (token) {
        console.log("utilities.js setDefeatedStatus", { token });
        const defeated = targetActor.system.attributes.hp.value <= targetActor.system.attributes.hp.min;
        const alreadyDown = (token.actor?.effects.find(e => e.getFlag("core", "statusId") === 'dead'));
        // find defeated status
        const status = CONFIG.statusEffects.find(e => e.id === 'dead');
        // const defeatedIcon = OSRIC.icons.general.combat.effects.defeated;
        const effect = token.actor && status ? status : CONFIG.controlIcons.defeated;
        // check to see if they need defeated mark or remove defeated mark
        // if ((!defeated && alreadyDown) || (defeated && !alreadyDown)) {
        await token.toggleEffect(effect, { overlay: defeated, active: defeated });
        // }
    }
}


/**
 * 
 * @param {Number} slotIndex 
 * @param {String} slotType arcaneSlots or divineSlots
 * @param {Boolean} bValue  Set true or false
 */
export async function memslotSetUse(actor, slotType, slotLevel, slotIndex, bValue) {
    let memSlots = foundry.utils.deepClone(actor.system.spellInfo.memorization);
    memSlots[slotType][slotLevel][slotIndex].cast = bValue;
    await actor.update({ "system.spellInfo.memorization": memSlots })
}

/**
 * 
 * @param {String} slotType  arcaneSlots or divineSlots
 * @param {Number} slotIndex 
 * 
 */
export function isMemslotUsed(actor, slotType, slotLevel, slotIndex) {
    // console.log("utilitis.js  isMemslotUsed", { actor, slotType, slotLevel, slotIndex });
    const spellUsed = actor.system.spellInfo.memorization[slotType][slotLevel][slotIndex]?.cast || false;
    return spellUsed;
}


/**
 * 
 * Use ammo for weapon (range/thrown)
 * 
 * @param {*} actor 
 * @param {*} weapon 
 * @returns 
 */
export async function useWeaponAmmo(actor, weapon) {
    // console.log("utilities.js useWeaponAmmo", { actor, weapon })
    let itemId = weapon.system.resource.itemId;
    if (!itemId) {
        itemId = await dialogManager.dialogGetInventoryItem(actor, `Ammo for ${weapon.name}`, `Select Ammo`);
        await weapon.update({ 'system.resource.itemId': itemId });
    }
    if (itemId) {
        let item = actor.items.get(itemId);
        if (item) {
            let itemCount = item.system.quantity;
            if (itemCount - 1 >= 0) {
                await item.update({ 'system.quantity': (itemCount - 1) });
                return true;
            }
        }
    }
    return false;
}

/**
 * 
 * Use a charge for an action
 * 
 * @param {*} actor 
 * @param {*} action 
 * @returns 
 */
export async function useActionCharge(actor, itemSource, action) {
    // console.log("utilities.js useActionCharge", { actor, action })
    let actionBundle = itemSource ? foundry.utils.deepClone(itemSource.system.actions) : foundry.utils.deepClone(actor.system.actions);

    const type = action.resource.type;
    const cost = action.resource.count.cost;
    const max = action.resource.count.max;
    let used = action.resource.count.value;
    let itemId = action.resource.itemId;

    console.log("utilities.js useActionCharge", { type, cost, max, used, itemId })

    switch (type) {
        case 'charged':
            if ((used + cost) <= max) {
                // console.log("utilities.js useActionCharge itemCount - (used + cost)", (used + cost))
                // console.log("utilities.js useActionCharge itemCount - max", max)
                actionBundle[action.index].resource.count.value = (used + cost);
                itemSource ? await itemSource.update({ "system.actions": actionBundle }) :
                    await actor.update({ "system.actions": actionBundle })
                // console.log("utilities.js useActionCharge TRUE")
                return true;
            } else {
                // no charges left
                return false;
            }
            break;

        case 'item':
            if (!itemId) {
                itemId = await dialogManager.dialogGetInventoryItem(actor, `Resource for ${action.name}`, `Select Resource`);
                actionBundle[action.index].resource.itemId = itemId;
                if (itemSource) {
                    await itemSource.update({ "system.actions": actionBundle })
                } else {
                    await actor.update({ "system.actions": actionBundle });
                }
            }
            // let item = actor.items.get(itemId);
            let item = await actor.getEmbeddedDocument("Item", itemId);
            // console.log("utilities.js useActionCharge item", item)
            if (item) {
                let itemCount = item.system.quantity;
                // console.log("utilities.js useActionCharge itemCount", itemCount)
                if ((itemCount - cost) >= 0) {
                    // console.log("utilities.js useActionCharge itemCount - cost", (itemCount - cost))
                    await item.update({ 'system.quantity': (itemCount - cost) });
                    return true;
                } else {
                    return false;
                }
            }
            break;
    }
    // console.log("utilities.js useActionCharge FALSE")
    return false;
}

/**
 * Evaluate a formula to it's total number value, async
 * @param {String} formula 
 * @param {*} data 
 * @returns 
 */
export async function evaluateFormulaValue(formula, data = {}) {

    // console.log("utilities.js evaluateFormulaValue 1", { formula, data });
    if (formula) {
        let fRoll;
        try {
            // fRoll = new Roll(String(formula), data).roll({ async: false });
            fRoll = await new Roll(String(formula), data).roll({ async: true });
            // console.log("utilities.js evaluateFormulaValue 2", { formula, data, fRoll });
        } catch (err) {
            ui.notifications.error(`utilities.js evaluateFormulaValue() formula process error: ${err}`)
            console.log("utilities.js evaluateFormulaValue formula process error", { err });
            return 0
        }
        return fRoll.total; // round it off
    } else {
        // console.log("utilities.js evaluateFormulaValue NOT FORMULA PASSED");
        return 0;
    }
}

/**
 * 
 * Eval a formula w/o async
 * 
 * @param {*} formula 
 * @param {*} data 
 * @returns 
 */
export function evaluateFormulaValueNoAsync(formula, data = {}) {

    // console.log("utilities.js evaluateFormulaValue 1", { formula, data });
    if (formula) {
        let fRoll;
        try {
            fRoll = new Roll(String(formula), data).roll({ async: false });
            // console.log("utilities.js evaluateFormulaValueNoAsync 2", { formula, data, fRoll });
        } catch (err) {
            ui.notifications.error(`utilities.js evaluateFormulaValueNoAsync() formula process error: ${err}`)
            console.log("utilities.js evaluateFormulaValueNoAsync formula process error", { err });
            return 0
        }
        return fRoll.total;
    } else {
        // console.log("utilities.js evaluateFormulaValueNoAsync NOT FORMULA PASSED");
        return 0;
    }
}


/**
 * Do somethings when NPC token first placed.
 *  
 * Roll hp using static hp, hp calculation or hitdice
 * 
 * @param {Object} token NPC Token instance
 */
export async function postNPCTokenCreate(token) {
    // wont run this if the npc token already has a .max value
    // if (!token.actor.system.attributes.hp.max) {
    const hitdice = String(token.actor.system.hitdice);
    const hpCalc = String(token.actor.system.hpCalculation);
    const hpStatic = parseInt(token.actor.system.attributes.hp.value);
    let formula = "";

    let hp = 0;
    if (hpStatic > 0) {
        hp = hpStatic;
    } else if (hpCalc.length > 0) {
        formula = hpCalc;
    } else if (hitdice.length > 0) {
        const aHitDice = hitdice.match(/^(\d+)(.*)/);
        const sHitDice = aHitDice[1]
        const sRemaining = aHitDice[2];
        formula = sHitDice + "d8" + sRemaining;
    }

    if (!hpStatic) {
        const roll = new Roll(formula).roll({ async: false });
        hp = roll.total;
    }
    // at least 1 hp
    hp = Math.max(hp, 1);

    console.log("_postNPCTokenCreate:createToken:hp", { hp });

    const npcNumberedNames = game.settings.get("osric", "npcNumberedNames");
    let newName = ""
    if (npcNumberedNames) {
        // set NPC name # with random value so they can be called out
        const MaxRandom = 25;
        const npcTokens = token.collection ? token.collection.filter(function (mapObject) { return mapObject?.actor?.type === 'npc' }) : [];
        let randomRange = MaxRandom + npcTokens.length;
        let loops = 0;

        do {
            loops += 1;
            const nNumber = Math.floor(Math.random() * (randomRange + loops)) + 1;
            // if the name contains #XX remove it, happens when you copy/paste a existing npc token
            let sName = token.name.replace(/#\d+/, '');
            sName = `${sName} #${nNumber}`;
            const matchedNPCs = npcTokens.filter(function (npcToken) { return npcToken.name.toLowerCase() === sName.toLowerCase() });
            if (matchedNPCs.length < 1) {
                newName = sName;
            }
            if (loops > 100) {
                newName = `${token.name} #xXxXxX`;
            }
        } while (newName == "");
    } else {
        newName = token.name;
    }

    // if we have a list of damage entries and nothing in actions or no weapons, create some
    const damageList = token.actor.system.damage.match(/(\d+[\-dD]\d+([\-\+]\d+)?)/g);
    // token.actor.system.weapons
    if (damageList?.length && token.actor.system?.weapons?.length < 1) {
        if (!token.actor.system.actions || token.actor.system.actions?.length < 1) {
            actionManager.createActionForNPCToken(token, damageList);
        }
    }

    // calculate size when dropped.
    let sizeSetting = 1;
    switch (token.actor.system.attributes.size) {
        case 'tiny':
            sizeSetting = 0.25;
            break;
        case 'small':
            sizeSetting = 0.5;
            break;
        case 'medium':
            sizeSetting = 1;
            break;
        case 'large':
            sizeSetting = 1;
            break;
        case 'huge':
            sizeSetting = 2;
            break;
        case 'gargantuan':
            sizeSetting = 3;
            break;
    }

    /**
     * do this so we can calculate xp at drop time with current hp for older systems 
     * 
     * 15+(@system.attributes.hp.max*100) would be 15+maxHP*100 and when killed granted to party xp
     * 
     */
    const rollData = mergeObject(token.actor.getRollData(),
        {
            system: {
                attributes: {
                    hp: { max: hp }
                }
            }
        }) || 0;

    const xpTotal = await evaluateFormulaValue(token.actor.system.xp.value, rollData);

    console.log("utilities.js postNPCTokenCreate", { rollData, xpTotal })

    // set values for updates
    const actorUpdates = {
        "system.attributes.hp.value": hp,
        "system.attributes.hp.max": hp,
        "system.attributes.hp.min": 0,
        "system.xp.value": xpTotal,
        "name": newName,
    };
    const tokenUpdates = {
        'name': newName,
        "width": sizeSetting,
        "height": sizeSetting,
        "xp.value": xpTotal,
    };

    // generate coin formulas when spawning npc
    for (const coin in token.actor.system.currency) {
        const formula = token.actor.system.currency[coin];
        if (formula) {
            const coinValue = parseInt(await evaluateFormulaValue(formula, null)) || 0;
            actorUpdates[`system.currency.${coin}`] = coinValue;
        }

    }
    // console.log("utilities.js postNPCTokenCreate", { actorUpdates, tokenUpdates })
    // use this for token update instead?
    Hooks.call("osricUpdateToken", this, token, tokenUpdates, 300);
    // const _timeout1 = setTimeout(async () => await token.update(tokenUpdates), 300);

    Hooks.call("osricUpdateActor", token.actor, actorUpdates);
    // without this I sometimes will see (50%) errors with hud.*
    // const _timeout2 = setTimeout(async () => await token.actor.update(actorUpdates), 300);
}

/**
 * 
 * Sort callback function to sort by record.name
 * 
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
export function sortByRecordName(a, b) {
    if (a?.name && b?.name) {
        const nameA = a.name.toLowerCase();
        const nameB = b.name.toLowerCase();
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
    }
    return 0;
}

/**
 * 
 * Sort by the item.data.sort value
 * 
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
export function sortBySort(a, b) {
    const nameA = a.sort;
    const nameB = b.sort;
    if (nameA < nameB) {
        return -1;
    }
    if (nameA > nameB) {
        return 1;
    }
    return 0;
}

/**
 * 
 * Sort callback function to sort by record.level
 * 
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
export function sortByLevel(a, b) {
    if (a.level < b.level) {
        return -1;
    }
    if (a.level > b.level) {
        return 1;
    }
    return 0;
}

/**
 * 
 * Wrapper function to get item from world or from packs
 * 
 * @param {*} itemId 
 * @returns 
 */
export async function getItem(itemId) {
    let item = getWorldItem(itemId);
    // console.log("utilitiesManager.js getItem 1", { itemId, item });
    if (!item) item = await getPackItem(itemId);
    // console.log("utilitiesManager.js getItem 2", { itemId, item });
    return item;
}

/**
 * 
 * Get item by name in world or pack file, first match
 * 
 * @param {*} name 
 * @returns 
 */
export async function getItemByName(name) {
    let item = await getWorldItemByName(name);
    // if (!item) item = await getPackItemByName(name);
    return item;
}


/**
 * 
 * Get an item from any pack file
 * 
 * @param {*} itemId 
 * @returns 
 */
export async function getPackItem(itemId) {
    let item;
    const allItemPacks = game.packs.filter(i => i.metadata.type === 'Item');
    // console.log("utilities.js getPackItem", { allItemPacks });
    for (const pack of allItemPacks) {
        const foundItem = await game.packs.get(pack.collection).getDocument(itemId);
        if (foundItem) {
            if (game.user.isGM || !pack.private) {
                item = foundItem;
            }
            break;
        }
    };
    return item;
}

/**
 * 
 * Get an item in the Campaign local
 * 
 * @param {*} itemId 
 * @returns 
 */
export function getWorldItem(itemId) {
    const item = game.items.get(itemId);
    // console.log("utilitiesManager.js getWorldItem 1", { itemId, item });
    return item;
}

// /**
//  * 
//  * Get a item by name from pack files
//  * 
//  * @param {*} name 
//  * @returns 
//  */
// export async function getPackItemByName(name) {
//     const _getPackItemEntry = async (name) => {
//         let matchedItem;
//         for (const foundItem of game.osric.library.allItems) {
//             if (foundItem.name === name) {
//                 matchedItem = await foundItem.pack.getDocument(foundItem.id);
//                 break;
//             }
//         }
//         return matchedItem;
//     }
//     const item = await _getPackItemEntry(name);
//     return item;
// }

/**
 * 
 * Get a item from world files by name
 * 
 * @param {*} name 
 * @returns 
 */
export async function getWorldItemByName(name) {
    const items = await game.items.filter(a => { return a.name === name });
    const item = items[0] ? item[0] : undefined
    return item;
}

/**
 * Function to allow refresh of library data, used by modules
 */
export async function initializeLibrary() {
    await initLibrary.default();
}

/**
 * 
 * Get folder record or create one to match name/type.
 * 
 * @param {String} folderName "Dropped NPCs"
 * @param {String} type "Actor" or "Item" .etc
 * @param {String} sort "a" for auto, "m" for manual
 * @param {Number} sortNumber "0"
 * @returns Folder
 */
export async function getFolder(folderName, type, sort = 'a', sortNumber = 0) {
    let folder;
    // const folderList = game.folders.filter(a => { a.name === folderName && a.type == type });
    const foundFolder = game.folders.getName(folderName);
    // console.log("utilitiesManager.js getFolder 1", { foundFolder })
    if (foundFolder) {
        // console.log("utilitiesManager.js getFolder 2", { foundFolder })
        folder = foundFolder;
    } else {
        let folderData = {
            name: folderName,
            type: type,
            sorting: "a",
            sort: 0,
        };
        const rootFolder = await Folder.implementation.create(folderData);
        // console.log("utilitiesManager.js getFolder 3", { rootFolder })
        folder = rootFolder;
    }

    // console.log("utilitiesManager.js getFolder", { folder })
    return folder;
}
/**
 * 
 *  Clear save Cache
 * 
 * @param {Actor} sourceActor 
 * @param {Token} targetToken 
 */
export async function deleteSaveCache(targetToken) {
    await runAsGM({
        operation: "unsetFlag",
        user: game.user.id,
        targetTokenId: targetToken.id,
        targetActorId: targetToken.actor.id,
        // sourceActorId: sourceActor.id,
        flag: {
            tag: 'saveCache',
        }
    });
}

/**
 * 
 * Set the saveCache for follow up spell damage reductions
 * 
 * @param {Actor} sourceActor 
 * @param {Token} targetToken 
 */
export async function setSaveCache(sourceActor, targetToken) {
    await runAsGM({
        operation: "setFlag",
        user: game.user.id,
        targetTokenId: targetToken.id,
        targetActorId: targetToken.actor.id,
        sourceActorId: sourceActor.id,
        flag: {
            tag: 'saveCache',
            data: { save: 'halve', sourceId: sourceActor.id }
        }
    });
}

/**
 * 
 * Send generic chat message with minimal requirements
 * 
 * @param {*} speaker  ChatMessage.getSpeaker({ actor: this.actor })
 * @param {*} title 
 * @param {*} message 
 */
export function chatMessage(speaker = ChatMessage.getSpeaker(), title = `Message`, message = `Forgot something...`, img = '', rollMode = game.settings.get("core", "rollMode")) {
    let imageHTML = '';
    if (img) {
        imageHTML = `<div class="a25-image"><img src="${img}" height="64"/></div>`;
    }
    let chatData = {
        title: title,
        content: `<div><h2>${title}</h2></div>` +
            `${imageHTML ? imageHTML : ''}` +
            `<div>${message}</div>`,
        user: game.user.id,
        speaker: speaker,
        type: game.osric.const.CHAT_MESSAGE_TYPES.OTHER,
    };
    //use user current setting? game.settings.get("core", "rollMode") 
    if (rollMode) ChatMessage.applyRollMode(chatData, rollMode);
    ChatMessage.create(chatData);
}

/**
 * 
 * Return a itemList record from a item
 * 
 * @param {*} itm Item Record
 * @returns { id: itm.id, uuid: itm.uuid, img: itm.img, name: itm.name, type: itm.type, quantity: itm.system.quantity }
 */
export function makeItemListRecord(itm) {
    if (itm) {
        return { id: itm.id, uuid: itm.uuid, img: itm.img, name: itm.name, type: itm.type, quantity: itm.system.quantity };
    }
}

/**
 * 
 * Take a time type (round|turn|hour|day) and convert the
 * count to seconds for duration management.
 * 
 * @param {*} type 
 * @param {*} count 
 * @returns Integer (seconds)
 */
export function convertTimeToSeconds(count = 1, type = 'round') {
    // console.log("utilities.js convertTimeToSeconds", { count, type })
    let timeMultiplier = 60;
    switch (type) {
        // turn is 10 rounds
        case 'turn':
            timeMultiplier = 600;
            break;
        // hour is 6 turns
        case 'hour':
            timeMultiplier = 3600;
            break;
        // day is 24 hours
        case 'day':
            timeMultiplier = 86400;
            break;

        // default is round, 60 seconds
        default:
            timeMultiplier = 60;
            break;
    }

    // console.log("utilities.js convertTimeToSeconds count * timeMultiplier", count * timeMultiplier)
    return count * timeMultiplier;
}

/**
 * 
 * @param {String} objectType Default 'Item'
 * @param {Boolean} gmOnly  Default false
 * @returns 
 */
export async function getPackItems(objectType = 'Item', isGM = false) {
    // console.log("utilities.js getPackItems", { objectType, isGM })
    const allItemPacks = game.packs.filter(i => i.metadata.type === objectType);
    let gmPackItems = [];
    let packItems = [];
    for (const pack of allItemPacks) {
        // console.log("utilities.js getPackItems", { pack })
        for (const entry of pack.index) {
            const item = await game.packs.get(pack.collection).getDocument(entry._id);
            if (item) {
                if (item.name.toLowerCase() !== '#[cf_tempentity]') {
                    gmPackItems.push(item);
                    if (!pack.private) {
                        packItems.push(item);
                    }
                }
            }
            // console.log("utilities.js getPackItems", { entry })
        };
    };

    if (isGM) return gmPackItems;

    return packItems;
}

/**
 * 
 * Check for sale npc/lootable npc actor locks
 * If the player is no longer active, remove it.
 * 
 * @param {*} sheet 
 */
export async function cleanStaleSheetLocks(sheet) {
    const lockId = sheet.object.system.opened;
    if (lockId) {
        // console.log("utilities.js cleanStaleSheetLocks", { lockId });
        const lockedBy = game.users.get(lockId);
        // console.log("utilities.js cleanStaleSheetLocks", { lockedBy });
        if (!lockedBy || !lockedBy?.active) {
            await runAsGM({
                sourceFunction: 'cleanSheetLocks',
                operation: 'actorUpdate',
                user: game.user.id,
                targetTokenId: sheet.object.token.id,
                update: { 'system.opened': null },
            });
        }
    }
    console.log("utilities.js cleanStaleSheetLocks end");

}

/**
 * Roll initiative. Check turn, check values, check for situation dialog() then roll
 * @param {*} combatant 
 * @param {*} combat 
 * @param {*} ctrlKey Boolean
 * @param {*} inlineMod Integer
 * @returns 
 */
export async function rollCombatantInitiative(combatant, combat, ctrlKey = false,
    initOptions = { inlineMod: 0, casting: false, question: 'Modifier', title: 'Initiative', flavor: 'Rolling Initiative' }) {
    console.log("utilities.js rollCombatantInitiative", { combatant, combat, ctrlKey, initOptions });
    // if (!game.user.isGM && (combatant.initiative !== null || combat.current.turn !== 0)) {
    if (!game.user.isGM && combatant.initiative !== null) {
        // if (combat.current.turn !== 0)
        //     ui.notifications.warn(`The combat round has already advanced, you will go last.`);
        // else
        ui.notifications.warn(`You cannot roll initiative more than once.`);
        return;
    }
    const init = ctrlKey ? { mod: 0 } : await dialogManager.getInitiative(initOptions.question, initOptions.title, initOptions.flavor, initOptions.casting)

    const currentInitiative = combat._getCurrentTurnInitiative();
    const startingTurnIndex = combat.current.turn;

    if (isNaN(init.mod)) return null;
    if (init.casting) {
        combatant.setFlag("osric", "initCasting", true);
    }
    const initFormula = combatant._getInitiativeFormula();
    // let formula = init.mod != 0 ? `${initFormula} +${init.mod} ` : `${initFormula}`;
    let formula = `${initFormula}`
    if (initOptions.inlineMod) formula += ` + ${initOptions.inlineMod}`;
    if (init.mod) formula += ` + ${init.mod}`
    if (combat.current.turn && currentInitiative) {
        formula += ` +${currentInitiative + 1}`;
    }
    // console.log("utilities.js rollCombatantInitiative", { formula, combat, combatant }, "COMBAT:", duplicate(combat));
    await combat.rollInitiative([combatant.id], { formula: formula, updateTurn: !startingTurnIndex, messageOptions: { rollMode: init.rollMode } });
    //dont update turn if combat is already progressed past first person
    // console.log("utilities.js rollCombatantInitiative", { formula, combat, combatant }, "TURN:", startingTurnIndex, "ROLLEDTURN:", combatant.index, "INIT:", combatant.initiative);
    if (startingTurnIndex) {
        if (combatant.index < startingTurnIndex) {
            console.log("utilities.js rollCombatantInitiative", "SET TO :", combatant.index);
            await combat.update({ turn: combatant.index });
        }
    } else {
        console.log("utilities.js rollCombatantInitiative", "SET TO DEFAULT:", 0);
        await combat.update({ turn: 0 })
    }
    // return combat.current.turn ? combat : await combat.update({ turn: 0 });
    return combat;
}

// /**
//  * 
//  * //TODO: Not using right now, might come back to this as results are more easily understood by players
//  * 
//  * Return currency updates of for purchase of amount/type
//  * 
//  * @param {*} currency actor.system.currency
//  * @param {*} amount  Integer
//  * @param {*} type  coin type, cp or sp or gp etc... 
//  * @returns the actor.update('coinType': remaining) value you'll need if amount avaliable or undefined
//  */
// export function exactChangePlease(currency, amount, type = "gp") {
//     console.log("utilities.js exactChangePlease", { currency, amount, type });

//     const variant = game.osric.config.settings.osricVariant;

//     // get the cp value of this coinage type
//     const cpConversion = OSRIC.currencyValue[variant][type];
//     // convert the amount to cp value
//     const targetCPValue = cpConversion * amount;
//     // let copperBaseNeeded = targetCPValue;

//     //return currency copper base value * amount
//     function cpConversionHelper(cAmount, cType) {
//         return (OSRIC.currencyValue[variant][cType] * parseInt(cAmount));
//     }

//     //return cost in ctype and remaining copper
//     function coinAmountHelper(ctype) {
//         // total cost in this coin type 
//         const costInCoin = targetCPValue / OSRIC.currencyValue[variant][ctype];
//         // round up the coin cost for floats
//         const costInCoinRound = Math.ceil(costInCoin);
//         // remainder converted to copper 
//         // console.log("utilities.js exactChangePlease coinAmountHelper", targetCPValue % OSRIC.currencyValue[variant][ctype]);
//         const remainingCopper = targetCPValue % OSRIC.currencyValue[variant][ctype] ? (OSRIC.currencyValue[variant][ctype] - targetCPValue % OSRIC.currencyValue[variant][ctype]) : 0;

//         // console.log("utilities.js exactChangePlease coinAmountHelper", { targetCPValue, costInCoin, costInCoinRound, remainingCopper }, "currencyValue:", OSRIC.currencyValue[variant][ctype]);

//         return ([costInCoinRound, remainingCopper]);
//     }

//     /**
//      * convert remaining copper to other (larger value) coins where possible
//      * 
//      * @param {*} remainingCopperCoins 
//      * @returns 
//      */
//     function consolidateCoinHelper(remainingCopperCoins) {
//         // console.log("utilities.js exactChangePlease consolidateCoinHelper", { remainingCopperCoins });
//         const leftOverChange = [];
//         if (remainingCopper) {
//             let copperChange = parseInt(remainingCopper);
//             // if I dont nail the order of coin types it will sometimes go backwards?!?! not sure why
//             // and we want to start with largest currency first.
//             for (const ctype of ['pp', 'ep', 'gp', 'sp', 'cp']) {
//                 const conversionValue = parseInt(OSRIC.currencyValue[variant][ctype]);
//                 const copperToCTypeCount = (copperChange / conversionValue);
//                 const convertedRemaining = (copperChange % conversionValue);
//                 // console.log("utilities.js exactChangePlease consolidateCoinHelper", { copperChange, amount, ctype, conversionValue, copperToCTypeCount, convertedRemaining });
//                 if (copperToCTypeCount >= 1 || convertedRemaining == 0) {
//                     const amount =
//                         ((copperChange - (convertedRemaining)) / conversionValue)
//                     const leftOver = convertedRemaining;
//                     copperChange = leftOver;
//                     leftOverChange.push({ amount: amount, type: ctype })

//                     // console.log("utilities.js exactChangePlease consolidateCoinHelper", { leftOver, copperChange, leftOverChange });
//                 }
//                 if (!copperChange)
//                     break;
//             }
//         }
//         console.log("utilities.js exactChangePlease consolidateCoinHelper DONE", { leftOverChange });
//         return leftOverChange;
//     }

//     let validCoinType = '';
//     for (const ctype of OSRIC.currencyType) {
//         const coinCPValue = cpConversionHelper(currency[ctype], ctype);
//         if (coinCPValue >= targetCPValue) {
//             validCoinType = ctype;
//             break;
//         }
//     }


//     // console.log("utilities.js exactChangePlease", { validCoinType });
//     const [validCoinAmount, remainingCopper] = coinAmountHelper(validCoinType);
//     let diffString = `spent ${validCoinAmount} ${validCoinType}`;

//     let update = {
//         [`system.currency.${validCoinType}`]: (parseInt(currency[validCoinType]) - validCoinAmount),
//     };
//     const change = consolidateCoinHelper(remainingCopper);
//     if (change.length) {
//         // update['system.currency.cp'] = (parseInt(currency['cp']) + remainingCopper);
//         diffString += ` and received`;
//         for (let i = 0; i < change.length; i++) {
//             update[`system.currency.${change[i].type}`] = (parseInt(currency[change[i].type]) + change[i].amount);
//             diffString += ((i > 0 ? ', ' : ' ') + `${change[i].amount} ${change[i].type}`);
//         }
//         diffString += ` in change`;
//     }

//     if (!validCoinType) {
//         update = undefined;
//         diffString = game.i18n.localize("OSRIC.currency.insufficient");
//     }

//     console.log("utilities.js exactChangePlease", { update, diffString });
//     return [update, diffString];
// }


/**
 * 
 * Debug code to see what total CP value before/after resulted in to check math on calculateCoins()
 * 
 * @param {*} availableCurrency 
 * @returns 
 */
function getCurrentCPTotal(availableCurrency) {
    const variant = game.osric.config.settings.osricVariant;
    const currencyBaseExchange = OSRIC.currencyValue[variant];
    let totalAvailable = 0;
    for (let currency in availableCurrency) {
        totalAvailable += availableCurrency[currency] * currencyBaseExchange[currency];
    }

    return totalAvailable;
}
/**
 * 
 * //TODO: This might generate confusing results but it does the right 
 * math and uses lowest currencies first
 * 
 * Calculate coins used when buying amount of currency type
 * We will try and use smaller coins first, or convert largest
 * currency to get to amount.
 * 
 * @param {*} availableCurrency actor.system.currency
 * @param {*} costAmount Integer
 * @param {*} costCurrency cp/sp/ep/gp/pp
 * @returns { available: newAvailableCurrency, spent: spent }
 * 
 */
export function calculateCoins(availableCurrency, costAmount, costCurrency) {
    const variant = game.osric.config.settings.osricVariant;
    const currencyBaseExchange = OSRIC.currencyValue[variant];
    // Step 1: convert the cost amount and currency type to base value
    let costInBaseValue = costAmount * currencyBaseExchange[costCurrency];

    console.log("utilities.js calculateCoins availableCurrency totalAvailable:", getCurrentCPTotal(availableCurrency), "Cost in CP", { costInBaseValue });

    // Step 2: verify availableCurrency contains enough currency for the total cost
    let totalAvailable = 0;
    for (let currency in availableCurrency) {
        totalAvailable += availableCurrency[currency] * currencyBaseExchange[currency];
    }
    if (totalAvailable < costInBaseValue) {
        // return "Not enough currency available";
        return undefined;
    }

    // Step 3: Exchange the lowest value currencies for the complete total amount needed
    let newAvailableCurrency = { ...availableCurrency };
    let spent = {};
    // let currencies = Object.keys(currencyBaseExchange).reverse();
    let currencies = Object.keys(currencyBaseExchange);
    for (let i = 0; i < currencies.length; i++) {
        let currency = currencies[i];

        // console.log("utilities.js calculateCoins 1", { costInBaseValue, currency });

        spent[currency] = 0;
        while (costInBaseValue >= currencyBaseExchange[currency] && newAvailableCurrency[currency] > 0) {
            costInBaseValue -= currencyBaseExchange[currency];
            newAvailableCurrency[currency]--;
            spent[currency]++;
        }
    }

    // console.log("utilities.js calculateCoins", { costInBaseValue });

    // // If costInBaseValue is not 0, exchange larger currency to compensate
    for (let i = 0; i < currencies.length && costInBaseValue > 0; i++) {
        let currency = currencies[i];

        // console.log("utilities.js calculateCoins", { currency, costInBaseValue });
        if (newAvailableCurrency[currency] > 0) {
            let needed = Math.ceil(costInBaseValue / currencyBaseExchange[currency]);
            if (needed <= newAvailableCurrency[currency]) {
                spent[currency] += needed;
                newAvailableCurrency[currency] -= needed;
                costInBaseValue -= currencyBaseExchange[currency];
                break;
            }
        }
    }

    // console.log("utilities.js calculateCoins END 1", { costInBaseValue });
    // if we have negative costInBaseValue, convert it to highest currency we can then default to CP
    let change = {};
    if (costInBaseValue < 0) {
        costInBaseValue = Math.abs(costInBaseValue);
        for (let i = currencies.length - 1; i >= 0 && costInBaseValue > 0; i--) {
            let currency = currencies[i];
            change[currency] = 0;
            // console.log("utilities.js calculateCoins Math.abs(costInBaseValue)", { currency, costInBaseValue }, currencyBaseExchange[currency]);
            while (costInBaseValue >= currencyBaseExchange[currency]) {
                newAvailableCurrency[currency]++;
                change[currency]++;
                costInBaseValue -= currencyBaseExchange[currency];
            }
        }
    }
    console.log("utilities.js calculateCoins END", { costInBaseValue, newAvailableCurrency, spent, change });

    console.log("utilities.js calculateCoins newAvailableCurrency totalAvailable:", getCurrentCPTotal(newAvailableCurrency));

    // Step 4: Return the new availableCurrency totals and the amount of each currency spent
    return { available: newAvailableCurrency, spent: spent, change: change };
}




/**
 * 
 * Find a item name/type match in current inventory of actor if it exists
 * 
 * finds itemToMatch.name split up by space or comma to see if its in the item.name 
 * of the actors inventory of items
 * 
 * @param {*} targetActor actor these items exist on
 * @param {*} itemToMatch item object to find similar
 * @param {*} itemTypes  Filter itemToMatch types out (optional)
 * @param {*} matchToType Only compare itemToMatch against items of this type (optional) i.e. look for a name of weapon itemToMatch in proficiency item names
 * @returns 
 */
export function findSimilarItem(targetActor, itemToMatch, itemTypes = OSRIC.inventoryTypes, matchToType = null) {

    console.log("utilities.js findSimilarItem", { targetActor, itemToMatch, itemTypes, matchToType });

    //if this isnt a item type valid to look for, return not found
    if (itemTypes.length &&
        !itemTypes.includes(itemToMatch.type))
        return undefined;

    if (itemToMatch?.system?.attributes?.magic &&
        !itemToMatch?.system?.attributes?.identified)
        return undefined;

    // const targetNameWords = itemToMatch.name.split(/[\s,]+/);
    // const targetNameRegex = new RegExp(targetNameWords
    //     .filter(word => word.length >= 2)
    //     .join(".*?"), "i");

    // console.log("utilities.js findSimilarItem", { targetNameWords, targetNameRegex });

    // const foundItem = targetActor.items.find(item => {
    //     const found = targetNameRegex.test(item.name);
    //     const matchType = matchToType ? item.type === matchToType : itemToMatch.type === item.type;

    //     console.log("utilities.js findSimilarItem", { item, found, matchType });

    //     return (matchType && found);
    // });


    function searchObjectsByName(item, objectList) {
        const escapedQuery = item.name.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
        const filteredList = objectList.filter((obj) => {
            const name = obj.name.toLowerCase();
            const regex = new RegExp('\\b' + escapedQuery.toLowerCase() + '\\b', 'g');
            const found = regex.test(name) && escapedQuery.length > 2;
            const matchType = matchToType ? obj.type === matchToType : itemToMatch.type === obj.type;

            console.log("utilities.js findSimilarItem", { name, regex, found, matchType });
            return matchType && found;
        });
        return filteredList;
    }


    /**search for every piece of item.name in every part of the object name in the list of objects     */
    function searchObjectsByName(item, objectList) {
        const escapedQuery = item.name.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
        const filteredList = objectList.filter((obj) => {
            const name = obj.name.toLowerCase();
            const nameWords = name.split(/[,\s]+/);
            const queryWords = escapedQuery.toLowerCase().split(/\s+/);

            const found = queryWords.some((word) => { return nameWords.some((nameWord) => { return nameWord.includes(word) }) })
            const matchType = matchToType ? obj.type === matchToType : itemToMatch.type === obj.type;
            const longEnough = escapedQuery.length > 2;

            return longEnough && matchType && found;
        });
        return filteredList;
    }

    const foundItems = searchObjectsByName(itemToMatch, targetActor.items);

    //for now we only need the first item if it's found.
    return foundItems[0] ?? null;
}